
====================
 ||  Amber Crux  ||
====================

by Andrew Apted


INTRODUCTION

Amber Crux is a graphical adventure game, set in a randomly generated
fantasy world.  The player will travel through a variety of different
environments, such as forests, towns and dungeons, fighting against
numerous vicious monsters and collecting intriguing artifacts in order
to complete their quest.


STATUS

This is currently in a very "alpha" state, undergoing heavy development.
The user interface is working, also the Lua scripts, and there is some
ability to generate maps.  But it is still a fair way away from being
a playable game.


GAMEPLAY DESIGN

+  turned based game (not real-time).

+  aiming for a game to take a few hours to complete.

+  permadeath, game only saves when quit.

+  the world is randomly generated, but very little (ideally nothing)
   during the gameplay will be random, the player will have access to
   things like monster stats and hence can predict the ability to win
   battles.

+  player can only see areas of the map which they have visited.  So
   the world slowly expands as they play.

+  battles never "just happen", but occur because the player either
   wants to travel to the next area (and the monsters are blocking),
   or the monsters are guarding an item which the player wants.  The
   player initiates the battle, but then must see it through (either
   being victorious or dying).

+  there will be simple puzzles, mainly to find item X in order to do Y,
   like finding a key to open a door.  There might be some NPCs too,
   sometimes part of the quest structure, but generally just part of the
   world.  Player will not be able to attack NPCs.


COPYRIGHT and LICENSE
 
|  Amber Crux : a graphical adventure game
|
|  Copyright (C) 2014 Andrew Apted, et al
|
|  Amber Crux is free software; you can redistribute it and/or modify
|  it under the terms of the GNU General Public License as published
|  by the Free Software Foundation; either version 3 of the License,
|  or (at your option) any later version.
|
|  Amber Crux is distributed in the hope that it will be useful, but
|  WITHOUT ANY WARRANTY; without even the implied warranty of
|  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
|  GNU General Public License for more details.
|
|  You should have received a copy of the GNU General Public License
|  along with this software.  If not, please visit the following
|  web page: http://www.gnu.org/licenses/gpl.html
 
See the file 'doc/LICENSE_GPL3.txt' in a source or binary package
for the complete text of the GNU GPLv3.  This license covers the
executable, the Lua scripts, and the source code.

The media files (i.e. the artwork) are covered by other licenses.
These licenses, such as CC-BY, CC-BY-SA, and CC0, all guarantee the
right to distribute the media (with or without modifications).
Please see AUTHORS.txt for a detailed list.

