//----------------------------------------------------------------------
//  Argument library
//----------------------------------------------------------------------
//
//  Copyright (C) 2010-2014 Andrew Apted
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this library; if not, see http://www.gnu.org/licenses
//
//----------------------------------------------------------------------

#ifndef __APLIB_ARGV_H__
#define __APLIB_ARGV_H__

extern const char **arg_list;
extern int arg_count;

void ArgvInit(int argc, const char **argv);
void ArgvClose(void);

int ArgvFind(char short_name, const char *long_name, int *num_params = NULL);
bool ArgvIsOption(int index);

#endif /* __APLIB_ARGV_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
