//----------------------------------------------------------------------
//  IMAGE MANAGEMENT
//----------------------------------------------------------------------
// 
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this library; if not, see http://www.gnu.org/licenses
//
//----------------------------------------------------------------------

#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "aplib/types.h"
#include "aplib/macros.h"
#include "aplib/util.h"

#include "aplib/image.h"


image_c::image_c(int _w, int _h) :
	width(_w),
	height(_h),
	opacity(OPAC_Complex),
	flags(0),
	name(NULL)
{
	pixels = new byte[width * height * 4];
}


image_c::~image_c()
{
	delete[] pixels;

	ClearName();
}


// set all pixels to transparent (black)
void image_c::Clear()
{
	memset(pixels, 0, width * height * 4);
}


void image_c::SetName(const char *new_name)
{
	StringFree(name);

	name = StringDup(new_name);
}

void image_c::ClearName()
{
	StringFree(name);
	
	name = NULL;
}


void image_c::SwapRows(int y1, int y2)
{
	if (y1 == y2)
		return;
	
	byte * p1 = PixelAt(0, y1);
	byte * p2 = PixelAt(0, y2);

	byte * p1_end = p1 + width * 4;

	for ( ; p1 < p1_end ; p1++, p2++)
	{
		byte tmp = *p1;  *p1 = *p2;  *p2 = tmp;
	}
}


// invert the image (turn it up-side-down)
void image_c::Invert()
{
	int y;

	for (y = 0 ; y < height/2 ; y++)
	{
		SwapRows(y, height - 1 - y);
	}
}


void image_c::DetermineOpacity()
{
	const byte *p = pixels;
	const byte *p_end = p + width * height * 4;

	bool is_masked = false;

	for ( ; p < p_end ; p += 4)
	{
		if (p[3] == 0)
		{
			is_masked = true;
		}
		else if (p[3] != 255)
		{
			opacity = OPAC_Complex;
			return;
		}
	}

	opacity = is_masked ? OPAC_Masked : OPAC_Solid;
}


//----------------------------------------------------------------------

#define MAX_IMG_ERROR  4096

static char img_error_buf[MAX_IMG_ERROR];


const char * IMG_Error(void)
{
	return img_error_buf;
}


void IMG_SetError(const char *str, ...)
{
	va_list args;

	va_start(args, str);
	vsnprintf(img_error_buf, MAX_IMG_ERROR, str, args);
	va_end(args);

	// ensure NUL termination
	img_error_buf[MAX_IMG_ERROR-1] = 0;
}

void IMG_ClearError(void)
{
	img_error_buf[0] = 0;
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
