//----------------------------------------------------------------------
//  Type definitions
//----------------------------------------------------------------------
//
//  Copyright (C) 2010-2014 Andrew Apted
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this library; if not, see http://www.gnu.org/licenses
//
//----------------------------------------------------------------------

#ifndef __APLIB_TYPE_H__
#define __APLIB_TYPE_H__

// basic types

typedef char  s8_t;
typedef short s16_t;
typedef int   s32_t;
 
typedef unsigned char  u8_t;
typedef unsigned short u16_t;
typedef unsigned int   u32_t;

typedef u8_t byte;

#endif  /* __APLIB_TYPE_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
