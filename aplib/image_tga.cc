//----------------------------------------------------------------------
//  TGA (Targa) IMAGE LOADER
//----------------------------------------------------------------------
// 
//  Copyright (C) 1999-2005  Id Software, Inc.
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this library; if not, see http://www.gnu.org/licenses
//
//----------------------------------------------------------------------
//
//  Based on code from Quake III Arena.
//
//----------------------------------------------------------------------

#include <stdio.h>
#include <string.h>

#include "aplib/types.h"
#include "aplib/macros.h"
#include "aplib/endian.h"
#include "aplib/image.h"
#include "aplib/file.h"


typedef struct _TargaHeader
{
	u8_t 	id_length;
	u8_t 	colormap_type;
	u8_t 	image_type;
	s16_t	colormap_start;
	s16_t	colormap_length;
	u8_t	colormap_bits;
	s16_t	x_origin;
	s16_t	y_origin;
	s16_t	width;
	s16_t	height;
	u8_t	pixel_bits;
	u8_t	attributes;

} TargaHeader;


typedef enum
{
	TGA_INDEXED = 1,
	TGA_INDEXED_RLE = 9,

	TGA_RGB = 2,
	TGA_RGB_RLE = 10,

	TGA_BW = 3,
	TGA_BW_RLE = 11

} TargaType_e;



image_c * IMG_LoadTGA(const char *filename)
{
	//
	// read the file into memory
	//
	byte	* buffer;
	int		length;

	buffer = FileLoad(filename, &length);


	if (! buffer)
	{
		IMG_SetError("No such file.");
		return NULL;
	}

	if (length < 18)
	{
		IMG_SetError("Bad TGA, header is too short.");
		FileFree(buffer);
		return NULL;
	}

	//
	// parse the TGA header
	//
	TargaHeader	targa_header;

	const byte *buf_p   = buffer;
	const byte *buf_end = buffer + length;

	targa_header.id_length = buf_p[0];
	targa_header.colormap_type = buf_p[1];
	targa_header.image_type = buf_p[2];

	memcpy(&targa_header.colormap_start, &buf_p[3], 2);
	memcpy(&targa_header.colormap_length, &buf_p[5], 2);
	targa_header.colormap_bits = buf_p[7];

	memcpy(&targa_header.x_origin, &buf_p[8], 2);
	memcpy(&targa_header.y_origin, &buf_p[10], 2);
	memcpy(&targa_header.width, &buf_p[12], 2);
	memcpy(&targa_header.height, &buf_p[14], 2);
	targa_header.pixel_bits = buf_p[16];
	targa_header.attributes = buf_p[17];

	// fix endianness of 16-bit words
	targa_header.colormap_start = LE_S16(targa_header.colormap_start);
	targa_header.colormap_length = LE_S16(targa_header.colormap_length);
	targa_header.x_origin = LE_S16(targa_header.x_origin);
	targa_header.y_origin = LE_S16(targa_header.y_origin);
	targa_header.width = LE_S16(targa_header.width);
	targa_header.height = LE_S16(targa_header.height);

	buf_p += 18;

	//
	// validate the header
	//
	if (targa_header.image_type != TGA_RGB &&
		targa_header.image_type != TGA_RGB_RLE &&
		targa_header.image_type != TGA_INDEXED &&
	    targa_header.image_type != TGA_INDEXED_RLE &&
		targa_header.image_type != TGA_BW)
	{
		IMG_SetError("Unsupported TGA image (type %d).", targa_header.image_type);
		FileFree(buffer);
		return NULL;
	}

	if ((targa_header.image_type == TGA_RGB || targa_header.image_type == TGA_RGB_RLE) &&
		! (targa_header.pixel_bits == 32 || targa_header.pixel_bits == 24))
	{
		IMG_SetError("Unsupported TGA image (odd pixel size: %d).", targa_header.pixel_bits);
		FileFree(buffer);
		return NULL;
	}

	int width    = targa_header.width;
	int height   = targa_header.height;
	int pix_bits = targa_header.pixel_bits;

	if (width <= 0 || height  <= 0)
	{
		IMG_SetError("Bad TGA, invalid image size (%d x %d).\n", width, height);
		FileFree(buffer);
		return NULL;
	}

	if (targa_header.id_length != 0)
	{
		if (buf_p + targa_header.id_length > buf_end)
		{
			IMG_SetError("Bad TGA, invalid comment size.\n");
			FileFree(buffer);
			return NULL;
		}

		buf_p += targa_header.id_length;  // skip TARGA image comment
	}

	//
	// create the image object, read the pixels
	//
	image_c * img = NULL;

	img = new image_c(width, height);

	byte * pixbuf;


	//
	// decode the palette, if any
	//
	byte palette[256][4];

	if (targa_header.image_type == TGA_INDEXED ||
		targa_header.image_type == TGA_INDEXED_RLE)
	{
		if (targa_header.colormap_type != 1)
		{
			IMG_SetError("Bad TGA, colormap type != 1\n");
			FileFree(buffer);
			return NULL;
		}

		if (targa_header.pixel_bits != 8 || targa_header.colormap_bits < 24)
		{
			IMG_SetError("Unsupported TGA image (bad colormap size)\n");
			FileFree(buffer);
			return NULL;
		}

		if (targa_header.colormap_length > 256)
		{
			IMG_SetError("Bad TGA, too many colors (over 256)\n");
			FileFree(buffer);
			return NULL;
		}

		memset(palette, 255, sizeof(palette));

		int cm_start = targa_header.colormap_start;
		int cm_end   = cm_start + targa_header.colormap_length;

		for (int n = cm_start ; n < cm_end ; n++)
		{
			// colors in the file are BGR(A) format!
			palette[n][2] = *buf_p++;
			palette[n][1] = *buf_p++;
			palette[n][0] = *buf_p++;

			if (targa_header.colormap_bits == 32)
				palette[n][3] = *buf_p++;
		}
	}


	if (targa_header.image_type == TGA_RGB || targa_header.image_type == TGA_BW)
	{
		if (buf_p + width * height * pix_bits / 8 > buf_end)
		{
			IMG_SetError("Bad TGA, file truncated.\n");
			delete img;
			FileFree(buffer);
			return NULL;
		}

		// Uncompressed RGB or gray scale image
		for (int y = height-1 ; y >= 0 ; y--)
		{
			pixbuf = img->PixelAt(0, y);

			for (int x = 0 ; x < width ; x++)
			{
				byte r, g, b, a;

				switch (pix_bits)
				{
					case 8:
						b = *buf_p++;
						g = b;
						r = b;
						*pixbuf++ = r;
						*pixbuf++ = g;
						*pixbuf++ = b;
						*pixbuf++ = 255;
						break;

					case 24:
						b = *buf_p++;
						g = *buf_p++;
						r = *buf_p++;
						*pixbuf++ = r;
						*pixbuf++ = g;
						*pixbuf++ = b;
						*pixbuf++ = 255;
						break;

					case 32:
						b = *buf_p++;
						g = *buf_p++;
						r = *buf_p++;
						a = *buf_p++;
						*pixbuf++ = r;
						*pixbuf++ = g;
						*pixbuf++ = b;
						*pixbuf++ = a;
						break;
				}
			}
		}
	}
	else if (targa_header.image_type == TGA_RGB_RLE)   // Run-length encoded RGB images
	{
		byte packetHeader, packetSize, j;

		byte r = 0;
		byte g = 0;
		byte b = 0;
		byte a = 255;

		for (int y = height-1 ; y >= 0 ; y--)
		{
			pixbuf = img->PixelAt(0, y);

			for (int x = 0 ; x < width ; )
			{
				if (buf_p + 1 > buf_end)
				{
					IMG_SetError("Bad TGA, file truncated\n");
					delete img;
					FileFree(buffer);
					return NULL;
				}

				packetHeader= *buf_p++;
				packetSize = 1 + (packetHeader & 0x7f);

				if (packetHeader & 0x80)    // run-length packet
				{
					if (buf_p + pix_bits/8 > buf_end)
					{
						IMG_SetError("Bad TGA, file truncated\n");
						delete img;
						FileFree(buffer);
						return NULL;
					}

					switch (pix_bits)
					{
						case 24:
							b = *buf_p++;
							g = *buf_p++;
							r = *buf_p++;
							a = 255;
							break;

						case 32:
							b = *buf_p++;
							g = *buf_p++;
							r = *buf_p++;
							a = *buf_p++;
							break;
					}

					for (j = 0 ; j < packetSize ; j++)
					{
						*pixbuf++ = r;
						*pixbuf++ = g;
						*pixbuf++ = b;
						*pixbuf++ = a;

						x++;

						if (x == width)  // run spans across rows
						{
							x = 0;

							if (y > 0)
								y--;
							else
								goto breakOut;

							pixbuf = img->PixelAt(0, y);
						}
					}
				}
				else     // non run-length packet
				{
					if (buf_p + pix_bits / 8 * packetSize > buf_end)
					{
						IMG_SetError("Bad TGA, file truncated\n");
						delete img;
						FileFree(buffer);
						return NULL;
					}

					for (j = 0 ; j < packetSize ; j++)
					{
						switch (pix_bits)
						{
							case 24:
								b = *buf_p++;
								g = *buf_p++;
								r = *buf_p++;
								*pixbuf++ = r;
								*pixbuf++ = g;
								*pixbuf++ = b;
								*pixbuf++ = 255;
								break;

							case 32:
								b = *buf_p++;
								g = *buf_p++;
								r = *buf_p++;
								a = *buf_p++;
								*pixbuf++ = r;
								*pixbuf++ = g;
								*pixbuf++ = b;
								*pixbuf++ = a;
								break;
						}

						x++;

						if (x == width)  // pixel packet run spans across rows
						{
							x = 0;

							if (y > 0)
								y--;
							else
								goto breakOut;

							pixbuf = img->PixelAt(0, y);
						}
					}
				}
			}
			breakOut: ;
		}
	}
	else if (targa_header.image_type == TGA_INDEXED)   // Uncompressed, colormapped images
	{
		if (buf_p + width * height * pix_bits / 8 > buf_end)
		{
			IMG_SetError("Bad TGA, file truncated.\n");
			delete img;
			FileFree(buffer);
			return NULL;
		}

		for (int y = height-1 ; y >= 0 ; y--)
		{
			pixbuf = img->PixelAt(0, y);

			for (int x = 0 ; x < width ; x++)
			{
				byte col = *buf_p++;

				*pixbuf++ = palette[col][0];
				*pixbuf++ = palette[col][1];
				*pixbuf++ = palette[col][2];
				*pixbuf++ = palette[col][3];
			}
		}
	}
	else if (targa_header.image_type == TGA_INDEXED_RLE)   // Runlength encoded colormapped image
	{
		byte packet_header, packet_size;

		for (int y = height-1 ; y >= 0 ; y--)
		{
			pixbuf = img->PixelAt(0, y);

			for (int x = 0 ; x < width ; )
			{
				packet_header = *buf_p++;
				packet_size = 1 + (packet_header & 0x7f);

				if (packet_header & 0x80)    // run-length packet
				{
					byte col = *buf_p++;

					for (int j = 0 ; j < packet_size ; j++)
					{
						*pixbuf++ = palette[col][0];
						*pixbuf++ = palette[col][1];
						*pixbuf++ = palette[col][2];
						*pixbuf++ = palette[col][3];

						x++;

						if (x == width)  // run spans across edge
						{
							x = 0;
							if (y > 0)
								y--;
							else
								goto breakOut2;

							pixbuf = img->PixelAt(0, y);
						}
					}
				}
				else   // not a run-length packet
				{
					for (int j = 0 ; j < packet_size; j++)
					{
						byte col = *buf_p++;

						*pixbuf++ = palette[col][0];
						*pixbuf++ = palette[col][1];
						*pixbuf++ = palette[col][2];
						*pixbuf++ = palette[col][3];

						x++;

						if (x == width)  // pixel packet run spans across edge
						{
							x = 0;
							if (y > 0)
								y--;
							else
								goto breakOut2;

							pixbuf = img->PixelAt(0, y);
						}
					}
				}
			}
			breakOut2: ;
		}
	}

	FileFree(buffer);

#if 1
	// 
	// bit 5 set => top-down mode
	//
	if (targa_header.attributes & 0x20) 
	{
		img->Invert();
	}
#endif

	img->DetermineOpacity();

	return img;
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
