//----------------------------------------------------------------------
//  IMAGE MANAGEMENT
//----------------------------------------------------------------------
//
//  Copyright (C) 2014 Andrew Apted
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 2
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this library; if not, see http://www.gnu.org/licenses
//
//----------------------------------------------------------------------

#ifndef __APLIB_IMAGE_H__
#define __APLIB_IMAGE_H__


typedef enum
{
	OPAC_Complex = 0,	// uses full range of alpha values
	OPAC_Masked,		// only uses alpha 255 and 0
	OPAC_Solid 			// utterly solid (alpha = 255 everywhere)
}
opacity_e;


class image_c
{
public:
	int		width;
	int		height;

	byte	* pixels;	// each pixel is four bytes: R,G,B,A

	opacity_e  opacity;

	//
	// application data, not used here
	//
	int		flags;
	const char * name;

public:
	// constructor creates a new, uninitialized image
	image_c(int _w, int _h);

	virtual ~image_c();

	// set all pixels to transparent (black)
	void Clear();

	void   SetName(const char *new_name);
	void ClearName();

	// invert the image (turn it up-side-down)
	void Invert();

	// scan image and test alpha values
	void DetermineOpacity();

	// test if coordinate lies inside the image
	inline bool isValid(int x, int y) const
	{
		return	(x >= 0 && x < width) &&
				(y >= 0 && y < height);
	}

	// get pointer to a specific pixel
	// Note: COORDINATE IS NOT CHECKED TO BE VALID
	inline byte * PixelAt(int x, int y) const
	{
		return pixels + (y * width + x) * 4;
	}

private:
	void SwapRows(int y1, int y2);
};


//
// IMAGE lOADERS
//

// if the loader fails, use this to get an error message
const char * IMG_Error(void);

void IMG_SetError(const char *str, ...);
void IMG_ClearError(void);


// load a TGA (Targe) image.
// returns NULL if something went wrong
image_c * IMG_LoadTGA(const char *filename);


#endif /* __APLIB_IMAGE_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
