//----------------------------------------------------------------------
//  TEXT MESSAGES
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __UI_TEXTAREA_H__
#define __UI_TEXTAREA_H__


class TextLine_c;
class TextChunk_c;


class UI_TextArea : public Fl_Widget
{
private:
	// number of lines we show (determines height of this widget)
	int view_lines;

	// number of lines to keep (in addition to view_lines)
	int keep_lines;

	std::vector< TextLine_c * > lines;

	// when zero, the last line is drawn at bottom of window.
	// +1.0 moves everything DOWN a single line.
	// -1.0 moves everything UP a single line.
	double scroll_dy;

	int font_h;
	int line_h;

public:
	UI_TextArea(int _maxlines, int X, int Y, int W, int H);
	virtual ~UI_TextArea();

	/* FLTK methods */
	void resize(int X, int Y, int W, int H);

	void draw();

public:
	void ResetEverything();

	void setFontSize(int new_font_h);

	int CalcHeight() const;

	void AddMessage(const char *msg);

	// delta is in same units / direction as 'scroll_dy' above
	void Scroll(double delta);

private:
	void Clear();

	void ScrollToTop();
	void NewLine();

	void AddChunk(const char *str, int len, bool hyphen = false);

	int LastLineWidth() const;

	void DrawArrows();
};


#endif  /* __UI_TEXTAREA_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
