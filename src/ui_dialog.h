//----------------------------------------------------------------------
//  ERROR DIALOG
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2014 Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __DIALOG_HEADER_H__
#define __DIALOG_HEADER_H__

void DLG_ShowError(const char *msg, ...);
void DLG_Notify(const char *msg, ...);
int  DLG_Confirm(const char *buttons, const char *msg, ...);

#endif /* __DIALOG_HEADER_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
