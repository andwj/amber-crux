//----------------------------------------------------------------------
//  Debugging support
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2011-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __SYS_DEBUG_H__
#define __SYS_DEBUG_H__

void LogInit(const char *filename);  // NULL for none
void LogClose(void);

void LogEnableDebug(bool enable);
void LogEnableTerminal(bool enable);

void BigPrintf(const char *str, ...);
void LogPrintf(const char *str, ...);
void DebugPrintf(const char *str, ...);

void LogGameMessage(const char *str);

#endif /* __SYS_DEBUG_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
