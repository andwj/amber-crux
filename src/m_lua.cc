//----------------------------------------------------------------------
//  LUA interface
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "hdr_lua.h"
#include "hdr_ui.h"

#include <algorithm>

#include "aplib/file.h"
#include "aplib/util.h"
#include "aplib/twister.h"

#include "main.h"
#include "m_cookie.h"
#include "m_lua.h"

#include "r_render.h"
#include "r_sprite.h"


static lua_State * LUA_ST;

static bool has_loaded = false;

static std::vector<std::string> * conf_line_buffer;


// random number generator (Mersenne Twister)
static MT_rand_c GUI_RNG(0);

// file handle for savegame API (gui.sv_*)
static FILE * save_fp;

static const int SV_ENCRYPT_BYTE = 0xAC;  /* think about it... */

typedef enum
{
	ATOM_Nil = 0,
	ATOM_True,
	ATOM_False,

	ATOM_String,     // followed by characters (NUL terminated),
	ATOM_Double,     // followed by raw 8 bytes

	ATOM_Table,
	ATOM_EndTable,
	ATOM_Reference,  // followed by a three byte integer

	ATOM_IntBase     // values >= IntBase is a small integer
	                 // (a _signed_ 8-bit value)

} sv_atom_kind_e;


//
// Utility function to parse a "color" value.
// We accept two formats:
//   - a string : "RGB" or "RRGGBB"     -- like in HTML or CSS
//   - a table  : { red, green, blue }  -- values are 0..255
//
static Fl_Color Grab_Color(lua_State *L, int stack_idx)
{
	if (lua_isstring(L, stack_idx))
	{
		const char *name = lua_tostring(L, stack_idx);

		int raw_hex = strtol(name, NULL, 16);
		int r, g, b;

		if (strlen(name) == 3)
		{
			r = ((raw_hex >> 8) & 0x0f) * 17;
			g = ((raw_hex >> 4) & 0x0f) * 17;
			b = ((raw_hex     ) & 0x0f) * 17;
		}
		else if (strlen(name) == 6)
		{
			r = (raw_hex >> 16) & 0xff;
			g = (raw_hex >>  8) & 0xff;
			b = (raw_hex      ) & 0xff;
		}
		else
			luaL_error(L, "bad color string");

		return fl_rgb_color(r, g, b);
	}

	if (lua_istable(L, stack_idx))
	{
		// format is a list, for example: { 255, 127, 0 }

		int r, g, b;

		lua_pushinteger(L, 1);
		lua_gettable(L, stack_idx);

		lua_pushinteger(L, 2);
		lua_gettable(L, stack_idx);

		lua_pushinteger(L, 3);
		lua_gettable(L, stack_idx);

		if (! lua_isnumber(L, -3) ||
			! lua_isnumber(L, -2) ||
			! lua_isnumber(L, -1))
		{
			luaL_error(L, "bad color table");
		}

		r = lua_tointeger(L, -3);
		g = lua_tointeger(L, -2);
		b = lua_tointeger(L, -1);

		lua_pop(L, 3);

		return fl_rgb_color(r, g, b);
	}

	luaL_error(L, "bad color value (not a string or table)");
	return FL_BLACK; /* NOT REACHED */
}

//
// Utility function to parse an alignment specifier.
// This is a string may contain the following letters:
//
//    'L' : left    |   'T' : top
//    'R' : right   |   'B' : bottom
//
static Fl_Align Grab_Align(lua_State *L, int stack_idx)
{
	if (! lua_isstring(L, stack_idx))
		luaL_error(L, "bad align value (not a string)");

	const char *name = lua_tostring(L, stack_idx);

	Fl_Align result = 0;

	if (strchr(name, 'L'))
		result = result | FL_ALIGN_LEFT;
	else if (strchr(name, 'R'))
		result = result | FL_ALIGN_RIGHT;

	if (strchr(name, 'T'))
		result = result | FL_ALIGN_TOP;
	else if (strchr(name, 'B'))
		result = result | FL_ALIGN_BOTTOM;

	return result;
}


//
// Utility function to parse the 'drag_to' field.
// This is a string which can contain the following letters:
//
//    'w' : weapon slot
//    'h' : hold slot
//    's' : spell slot
//
//    'g' : gettable item
//    'd' : droppable item
//    'a' : armor (i.e. wearable item)
//
static int Grab_DragTo(lua_State *L, int stack_idx)
{
	if (! lua_isstring(L, stack_idx))
		luaL_error(L, "bad align value (not a string)");

	const char *name = lua_tostring(L, stack_idx);

	int result = 0;

	if (strchr(name, 'w')) result = result | DRAGTO_Weapon;
	if (strchr(name, 'h')) result = result | DRAGTO_Hold;
	if (strchr(name, 's')) result = result | DRAGTO_Spell;

	if (strchr(name, 'g')) result = result | DRAGTO_Get;
	if (strchr(name, 'd')) result = result | DRAGTO_Drop;
	if (strchr(name, 'a')) result = result | DRAGTO_Armor;

	return result;
}


//
// LUA: raw_print(str)
//
int gui_raw_print(lua_State *L)
{
	int nargs = lua_gettop(L);

	if (nargs >= 1)
	{
		const char *res = luaL_checkstring(L, 1);
		SYS_ASSERT(res);

		BigPrintf("%s", res);
	}

	return 0;
}


//
// LUA: raw_debug_print(str)
//
int gui_raw_debug_print(lua_State *L)
{
	int nargs = lua_gettop(L);

	if (nargs >= 1)
	{
		const char *res = luaL_checkstring(L, 1);
		SYS_ASSERT(res);

		DebugPrintf("%s", res);
	}

	return 0;
}


//
// LUA: raw_message(str)
//
// Special characters:
//    '\n' = subsequent text appears on a new line
//    '\f' = subsequent text appears at top of window (on a new line)
//
// (all other control characters are ignored)
//
int gui_raw_message(lua_State *L)
{
	int nargs = lua_gettop(L);

	if (nargs >= 1)
	{
		const char *msg = luaL_checkstring(L, 1);
		SYS_ASSERT(msg);

		LogGameMessage(msg);

		main_win->text_box->AddMessage(msg);
	}

	return 0;
}


//
// LUA: version() --> string
//
int gui_version(lua_State *L)
{
	lua_pushstring(L, AMBER_VERSION);
	return 1;
}


//
// LUA: config_line(str)
//
int gui_config_line(lua_State *L)
{
	const char *res = luaL_checkstring(L, 1);

	SYS_ASSERT(conf_line_buffer);

	conf_line_buffer->push_back(res);

	return 0;
}


//
// LUA: change_preference(name, value)
//
int gui_change_preference(lua_State *L)
{
	const char *name  = luaL_checkstring(L, 1);
	const char *value = luaL_checkstring(L, 2);

	Cookie_ChangePreference(name, value);

	return 0;
}


//
// LUA: set_char_info(info)
//
int gui_set_char_info(lua_State *L)
{
	const char *res = luaL_checkstring(L, 1);

	next_char_info = StringDup(res);

	return 0;
}


//
// LUA: force_quit()
//
int gui_force_quit(lua_State *L)
{
	want_quit = QUIT_Immediately;

	return 0;
}


//
// LUA: rand_seed(seed)
//
int gui_rand_seed(lua_State *L)
{
	int the_seed = luaL_checkint(L, 1) & 0x7FFFFFFF;

	GUI_RNG.Seed(the_seed);

	return 0;
}


//
// LUA: random() --> number
//
int gui_random(lua_State *L)
{
	lua_Number value = GUI_RNG.Rand_fp();

	lua_pushnumber(L, value);
	return 1;
}


//
// LUA: sv_exists(filename) --> boolean
//
int gui_sv_exists(lua_State *L)
{
	const char *base = luaL_checkstring(L, 1);
	const char *filename = StringPrintf("%s/%s", home_dir, base);

	lua_pushboolean(L, FileExists(filename) ? 1 : 0);

	StringFree(filename);

	return 1;
}


//
// LUA: sv_delete(filename)
//
int gui_sv_delete(lua_State *L)
{
	const char *base = luaL_checkstring(L, 1);
	const char *filename = StringPrintf("%s/%s", home_dir, base);

	FileDelete(filename);

	StringFree(filename);

	return 0;
}


//
// LUA: sv_open(filename, mode)
//
// Opens the savegame file.
// The 'mode' can be "r" for reading, "w" for writing.
//
// NOTE: only one savegame file can be open at any one time.
//
int gui_sv_open(lua_State *L)
{
	if (save_fp)
		return luaL_error(L, "gui.sv_open: a savegame file is already open");

	const char *base = luaL_checkstring(L, 1);
	const char *mode = luaL_checkstring(L, 2);

	if (mode[0] != 'r' && mode[0] != 'w')
		return luaL_argerror(L, 2, "bad mode string");

	const char *filename = StringPrintf("%s/%s", home_dir, base);

	mode = (mode[0] == 'r') ? "rb" : "wb";

	save_fp = fopen(filename, mode);

	if (! save_fp)
	{
		const char *err_msg = strerror(errno);

		return luaL_error(L, "failed to open savegame file:\n%s", err_msg);
	}

	StringFree(filename);

	return 0;
}


//
// LUA: sv_close()
//
int gui_sv_close(lua_State *L)
{
	if (! save_fp)
		return luaL_error(L, "gui.sv_close: no savegame file is open");

	fclose(save_fp);
	save_fp = NULL;

	return 0;
}


static void sv_put_byte(int value)
{
	// trivial encryption
	fputc(value ^ SV_ENCRYPT_BYTE, save_fp);
}

static void sv_put_integer24(int value)
{
	sv_put_byte(value & 0xff);  value >>= 8;
	sv_put_byte(value & 0xff);  value >>= 8;
	sv_put_byte(value & 0xff);
}

static void sv_put_string(const char *str)
{
	sv_put_byte(ATOM_String);

	for ( ; *str ; str++)
		sv_put_byte(*str);

	sv_put_byte(0);
}

static void sv_put_number(double val)
{
	// special handling for small integers
	int iv = (int)val;

	if (val == (double)iv && (iv >= -64) && (iv <= 126))
	{
		iv += 0x80;
		sv_put_byte(iv);
		return;
	}

	sv_put_byte(ATOM_Double);

	// -AJA- the following sucks, but should work OK

	union
	{
		double d;
		byte raw[8];

	} uv;

	uv.d = val;

	for (int k = 0 ; k < 8 ; k++)
		sv_put_byte(uv.raw[k]);
}


//
// LUA: sv_write_atom(kind [, val])
//
// The following table shows the possible kind values.
// Some kinds of atoms need a value, and some don't.
//
//    nil     : the Lua 'nil' value
//    true    : the Lua 'true' boolean value
//    false   : the Lua 'false' boolean value
//
//    number  : a numeric value (in 'val')
//    string  : a string value (in 'val')
//
//    table   : a new (unseen) table has begun.
//              the integer in 'val' is a reference (id) number.
//              the following atoms are key/value pairs.
//
//    end_table : marks the end of a table.
//
//    reference : an old (already seen) table.
//                the integer in 'val' is the reference number
//                (matching a previous "table" atom).
//
int gui_sv_write_atom(lua_State *L)
{
	if (! save_fp)
		return luaL_error(L, "gui.sv_write_atom: no savegame file is open");

	const char *kind = luaL_checkstring(L, 1);

	if (strcmp(kind, "nil") == 0)
		sv_put_byte(ATOM_Nil);

	else if (strcmp(kind, "boolean") == 0)
	{
		int val = lua_toboolean(L, 2);

		sv_put_byte(val ? ATOM_True : ATOM_False);
	}
	else if (strcmp(kind, "string") == 0)
	{
		const char *val = luaL_checkstring(L, 2);

		if (strlen(val) >= MSG_BUF_LEN - 4)
			luaL_error(L, "gui.sv_write_atom: string too long");

		sv_put_string(val);
	}
	else if (strcmp(kind, "number") == 0)
	{
		double val = luaL_checknumber(L, 2);

		sv_put_number(val);
	}
	else if (strcmp(kind, "table") == 0)
		sv_put_byte(ATOM_Table);

	else if (strcmp(kind, "end_table") == 0)
		sv_put_byte(ATOM_EndTable);

	else if (strcmp(kind, "reference") == 0)
	{
		int ref_id = luaL_checkint(L, 2);

		sv_put_byte(ATOM_Reference);
		sv_put_integer24(ref_id);
	}

	else
		return luaL_argerror(L, 1, "invalid atom kind");

	return 0;
}


class sv_read_ERROR
{
public:
	 sv_read_ERROR() {}
	~sv_read_ERROR() {}
};


static int sv_get_byte() throw(sv_read_ERROR)
{
	if (  feof(save_fp)) throw sv_read_ERROR();
	if (ferror(save_fp)) throw sv_read_ERROR();

	int ch = fgetc(save_fp);

	// trivial encryption
	return (ch ^ SV_ENCRYPT_BYTE) & 0xff;
}

static int sv_get_integer24() throw(sv_read_ERROR)
{
	return	 sv_get_byte() |
			(sv_get_byte() << 8) |
			(sv_get_byte() << 16);
}

static const char * sv_get_string() throw(sv_read_ERROR)
{
	static char buffer[MSG_BUF_LEN];

	for (int len = 0 ; len < MSG_BUF_LEN-2 ; len++)
	{
		buffer[len] = sv_get_byte();

		if (buffer[len] == 0)
			return buffer;
	}

	LogPrintf("Very long string while reading savegame file!\n");

	// string too long!
	throw sv_read_ERROR();
}

static double sv_get_double() throw(sv_read_ERROR)
{
	// -AJA- the following sucks, but should work OK

	union
	{
		double d;
		byte raw[8];

	} uv;

	for (int k = 0 ; k < 8 ; k++)
		uv.raw[k] = sv_get_byte();

	return uv.d;
}


//
// LUA: sv_read_atom() --> kind [, val]
//
// The result "ERROR" is special, and indicates that an error
// occurred (e.g. file reached the end).  The 'val' in this case
// is a string containing a more detailed message.
//
// See sv_write_atom() above for a description of atom kinds.
//
int gui_sv_read_atom(lua_State *L)
{
	if (! save_fp)
		return luaL_error(L, "gui.sv_read_atom: no savegame file is open");

	try
	{
		int kind = sv_get_byte();

		switch (kind)
		{
			case ATOM_Nil:
				lua_pushstring(L, "nil");
				return 1;

			case ATOM_True:
				lua_pushstring (L, "boolean");
				lua_pushboolean(L, 1);
				return 2;

			case ATOM_False:
				lua_pushstring (L, "boolean");
				lua_pushboolean(L, 0);
				return 2;

			case ATOM_String:
			{
				const char *str = sv_get_string();
				lua_pushstring(L, "string");
				lua_pushstring(L, str);
				return 2;
			}

			case ATOM_Double:
			{
				double num = sv_get_double();
				lua_pushstring(L, "number");
				lua_pushnumber(L, num);
				return 2;
			}

			case ATOM_Table:
				lua_pushstring(L, "table");
				return 1;

			case ATOM_EndTable:
				lua_pushstring(L, "end_table");
				return 1;

			case ATOM_Reference:
			{
				int ref_id = sv_get_integer24();

				lua_pushstring(L, "reference");
				lua_pushinteger(L, ref_id);
				return 2;
			}

			// everything >= ATOM_IntBase is a small integer
			default:
				lua_pushstring (L, "number");
				lua_pushinteger(L, kind - 0x80);
				return 2;
		}
	}
	catch (sv_read_ERROR)
	{
		lua_pushstring(L, "ERROR");
		lua_pushstring(L, "file read error");
		return 2;
	}

	/* NOT REACHED */
	return 0;
}


//
// LUA: game_state() --> keyword
//  OR: game_state(keyword)
//
int gui_game_state(lua_State *L)
{
	const char *keyword = luaL_optstring(L, 1, NULL);

	if (! keyword)  // query?
	{
		switch (game_state)
		{
			case GAME_NotStarted:
				lua_pushstring(L, "not_started");
				return 1;

			case GAME_Active:
				lua_pushstring(L, "active");
				return 1;

			case GAME_Finished:
				lua_pushstring(L, "finished");
				return 1;

			default:
				Main_FatalError("Internal Error: bad game state\n");
				return 0;  /* NOT REACHED */
		}
	}

	if (strcmp(keyword, "not_started") == 0)
		game_state = GAME_NotStarted;
	else if (strcmp(keyword, "active") == 0)
		game_state = GAME_Active;
	else if (strcmp(keyword, "finished") == 0)
		game_state = GAME_Finished;
	else
		luaL_argerror(L, 1, "unknown game_state keyword");

	return 0;
}


//
// LUA: screen_off()
//
int gui_screen_off(lua_State *L)
{
	main_win->ShowScreen(0);

	return 0;
}


//
// LUA: screen_begin(props)
//
int gui_screen_begin(lua_State *L)
{
	if (lua_type(L, 1) != LUA_TTABLE)
		return luaL_argerror(L, 1, "missing props table");

	lua_getfield(L, 1, "width");
	lua_getfield(L, 1, "height");
	lua_getfield(L, 1, "char_w");
	lua_getfield(L, 1, "char_h");

	int width  = luaL_optint(L, -4, 0);
	int height = luaL_optint(L, -3, 0);
	int char_w = luaL_optint(L, -2, 35);
	int char_h = luaL_optint(L, -1, (char_w * 4 + 4) / 7);

	lua_pop(L, 4);

	if (width <= 0 || height <= 0)
		return luaL_error(L, "bad/missing size for screen");

	if (char_w <= 0 || char_h <= 0)
		return luaL_error(L, "bad char_w or char_h value");

	main_win->screen->Begin(width, height, char_w, char_h);


	lua_getfield(L, 1, "bg_color");

	if (! lua_isnil(L, -1))
	{
		Fl_Color col = Grab_Color(L, lua_gettop(L));

		main_win->screen->color(col);
	}
	else
	{
		main_win->screen->color(FL_BLACK);
	}

	lua_pop(L, 1);

	main_win->ShowScreen(1);

	return 0;
}


//
// LUA: create_element() --> id number
//
int gui_create_element(lua_State *L)
{
	int id = main_win->screen->CreateElement();

	lua_pushinteger(L, id);
	return 1;
}


//
// LUA: set_element(id, element)
//
int gui_set_element(lua_State *L)
{
	int id = luaL_checkint(L, 1);

	Screen_element_c * elem = main_win->screen->FindElement(id);

	if (! elem)
		return luaL_argerror(L, 1, "invalid element id");

	if (lua_type(L, 2) != LUA_TTABLE)
		return luaL_argerror(L, 2, "missing element table");

	// first parse various common properties, all optional

	lua_getfield(L, 2, "x");
	lua_getfield(L, 2, "y");
	lua_getfield(L, 2, "w");
	lua_getfield(L, 2, "h");
	lua_getfield(L, 2, "scale");
	{
		if (! lua_isnil(L, -5))
			elem->x = luaL_checknumber(L, -5);

		if (! lua_isnil(L, -4))
			elem->y = luaL_checknumber(L, -4);

		if (! lua_isnil(L, -3))
			elem->w = luaL_checknumber(L, -3);

		if (! lua_isnil(L, -2))
			elem->h = luaL_checknumber(L, -2);

		if (! lua_isnil(L, -1))
			elem->scale = luaL_checknumber(L, -1);
	}
	lua_pop(L, 5);


	lua_getfield(L, 2, "color");
	if (! lua_isnil(L, -1))
		elem->color = Grab_Color(L, lua_gettop(L));
	lua_pop(L, 1);

	lua_getfield(L, 2, "align");
	if (! lua_isnil(L, -1))
		elem->align = Grab_Align(L, lua_gettop(L));
	lua_pop(L, 1);


	lua_getfield(L, 2, "visible");
	lua_getfield(L, 2, "mirror");
	{
		if (! lua_isnil(L, -2))
			elem->visible = lua_toboolean(L, -2);

		if (! lua_isnil(L, -1))
			elem->mirror = lua_toboolean(L, -1);
	}
	lua_pop(L, 2);


	lua_getfield(L, 2, "val");
	lua_getfield(L, 2, "maxval");
	lua_getfield(L, 2, "opt_name");
	lua_getfield(L, 2, "words");
	{
		if (! lua_isnil(L, -4))
			elem->val = luaL_checknumber(L, -4);

		if (! lua_isnil(L, -3))
			elem->maxval = luaL_checknumber(L, -3);

		if (! lua_isnil(L, -2))
			elem->setOptName(lua_tostring(L, -2));

		if (! lua_isnil(L, -1))
		{
			elem->setText(lua_tostring(L, -1));

			// Fixme? this is hackish...
			elem->maxval = elem->countKeys() - 1;
		}
	}
	lua_pop(L, 4);


	// see what kind of element we have

	lua_getfield(L, 2, "text");
	lua_getfield(L, 2, "image");
	lua_getfield(L, 2, "control");

	if (! lua_isnil(L, -3))
	{
		elem->kind = ELEM_Text;
		elem->setText(lua_tostring(L, -3));
	}
	else if (! lua_isnil(L, -2))
	{
		if (elem->w <= 1 && elem->h <= 1)
			luaL_error(L, "gui.set_element: image without a width or height");

		const char *name = lua_tostring(L, -2);

		elem->kind = ELEM_Image;
		elem->image = R_CacheImage(name);
	}
	else if (! lua_isnil(L, -1))
	{
		if (strlen(elem->opt_name) == 0)
			luaL_error(L, "gui.screen_add: control is missing 'opt_name'");

		const char *kind = lua_tostring(L, -1);

		if (strcmp(kind, "check_button") == 0)
		{
			elem->kind = ELEM_CheckButton;
		}
		else if (strcmp(kind, "word_sel") == 0)
		{
			if (! elem->text)
				luaL_error(L, "gui.set_element: word_sel is missing 'words'");

			elem->kind = ELEM_WordSel;
		}
		else if (strcmp(kind, "slider") == 0)
		{
			if (elem->maxval <= 0)
				luaL_error(L, "gui.set_element: bad maxval for slider");

			elem->kind = ELEM_Slider;
		}
		else
			luaL_error(L, "gui.set_element: unknown control kind '%s'", kind);
	}

	lua_pop(L, 3);

	if (elem->kind == ELEM_INVALID)
		luaL_error(L, "gui.set_element: invalid element (no kind set)");

	elem->val = CLAMP(0, elem->val, elem->maxval);

	// ensure updates get shown
	main_win->screen->redraw();

	return 0;
}


//
// LUA: set_menu(title, entries)
//
// The 'entries' parameter is a list, each entry is a table.
// Example:
//
// {
//    { label="New Game",  act="new"   }
//    { label="Load Game", act="load"  }
// }
//
// When the user clicks a menu item, the 'amb_user_action()' script
// function is called, passing "menu" as the source, the ACT string as
// the action, and the PARAM string as the parameter.
//
// There are some special ACT strings which begin with an exclamation
// mark.  These are handled by the engine and never sent to the script.
//
//    "!quit"     : causes the game to quit
//                  (with a confirmation dialog if necessary).
//
static int Grab_MenuEntry(lua_State *L, int stack_pos)
{
	if (lua_type(L, stack_pos) != LUA_TTABLE)
		return luaL_error(L, "gui.set_menu: invalid menu entry");

	lua_getfield(L, stack_pos, "act");
	lua_getfield(L, stack_pos, "label");
	lua_getfield(L, stack_pos, "param1");
	lua_getfield(L, stack_pos, "param2");

	const char *act    = luaL_checkstring(L, -4);
	const char *label  = luaL_checkstring(L, -3);
	const char *param1 = luaL_optstring(L, -2, NULL);
	const char *param2 = luaL_optstring(L, -1, NULL);

	main_win->panel->AddMenu(act, label, param1, param2);

	lua_pop(L, 4);

	return 0;
}

int gui_set_menu(lua_State *L)
{
	const char *title = luaL_checkstring(L, 1);

	if (lua_type(L, 2) != LUA_TTABLE)
		return luaL_argerror(L, 3, "missing menu entries");

	main_win->panel->BeginMenu(title);

	int index = 1;

	for (;;)
	{
		lua_pushinteger(L, index);
		lua_gettable(L, 2);

		if (lua_isnil(L, -1))
		{
			lua_pop(L, 1);
			break;
		}

		Grab_MenuEntry(L, 3);

		lua_pop(L, 1);

		index++;
	}

	main_win->panel->FinishMenu();

	return 0;
}


//
// LUA: set_stats(table)
//
int gui_set_stats(lua_State *L)
{
	if (lua_type(L, 1) != LUA_TTABLE)
		return luaL_argerror(L, 1, "missing stats table");

	lua_getfield(L, 1, "health");
	lua_getfield(L, 1, "mana");
	lua_getfield(L, 1, "armor");
	lua_getfield(L, 1, "gold");

	int health = luaL_optint(L, -4, -1);
	int mana   = luaL_optint(L, -3, -1);
	int armor  = luaL_optint(L, -2, -1);
	int gold   = luaL_optint(L, -1, -1);

	lua_pop(L, 4);

	main_win->panel->stat_box->setHealth(health);
	main_win->panel->stat_box->setMana  (mana);
	main_win->panel->stat_box->setArmor (armor);
	main_win->panel->stat_box->setGold  (gold);

	lua_getfield(L, 1, "weapon_icon");
	lua_getfield(L, 1, "spell_icon");
	lua_getfield(L, 1, "hold_icon");

	const char *weapon_icon = luaL_optstring(L, -3, NULL);
	const char *spell_icon  = luaL_optstring(L, -2, NULL);
	const char *hold_icon   = luaL_optstring(L, -1, NULL);

	lua_pop(L, 3);

	main_win->panel->stat_box->setWeaponIcon(weapon_icon);
	main_win->panel->stat_box->setSpellIcon(spell_icon);
	main_win->panel->stat_box->setHoldIcon(hold_icon);

	return 0;
}


//
// LUA: cache_image(name)
//
int gui_cache_image(lua_State *L)
{
	const char *name = luaL_checkstring(L, 1);

	R_CacheImage(name);

	return 0;
}


//
// LUA: create_world(width, height)
//
int gui_create_world(lua_State *L)
{
	int width  = luaL_checkint(L, 1);
	int height = luaL_checkint(L, 2);

	if (width  < 1 || width  > MAX_WORLD_W)
		return luaL_argerror(L, 1, "bad world width");

	if (height < 1 || height > MAX_WORLD_H)
		return luaL_argerror(L, 2, "bad world height");

	world_w = width;
	world_h = height;

	home_tile_x = 1;
	home_tile_y = 1;

	main_win->ShowText(1);

	main_win->map->Populate();

	return 0;
}


//
// LUA: move_home(tx, ty, [zoom])
//
int gui_move_home(lua_State *L)
{
	int tx = luaL_checkint(L, 1);
	int ty = luaL_checkint(L, 2);

	if (tx < 1 || tx > world_w)
		return luaL_argerror(L, 1, "bad home X coord");

	if (ty < 1 || ty > world_h)
		return luaL_argerror(L, 2, "bad home Y coord");

	ty = world_h + 1 - ty;

	main_win->map->ChangeHome(tx, ty);

	int zoom = lua_toboolean(L, 3);

	if (zoom)
		main_win->map->GoHome();

	return 0;
}


//
// LUA: set_tile(tx, ty, props)
//
int gui_set_tile(lua_State *L)
{
	int tx = luaL_checkint(L, 1);
	int ty = luaL_checkint(L, 2);

	if (tx < 1 || tx > world_w)
		return luaL_argerror(L, 1, "bad tile X coordinate");

	if (ty < 1 || ty > world_h)
		return luaL_argerror(L, 2, "bad tile Y coordinate");
	
	ty = world_h + 1 - ty;

	if (lua_type(L, 3) != LUA_TTABLE)
		return luaL_argerror(L, 3, "missing tile properties");

	Tile_c *T = main_win->map->getTile(tx, ty);

	SYS_ASSERT(T);


	/* tile properties */

	lua_getfield(L, 3, "visible");

	if (! lua_isnil(L, -1))
		T->visible = lua_toboolean(L, -1);

	lua_pop(L, 1);


	lua_getfield(L, 3, "background");

	if (! lua_isnil(L, -1))
	{
		const char *name = lua_tostring(L, -1);
		T->setBackground(TILE_LAYER_BG, name);
	}

	lua_pop(L, 1);


	lua_getfield(L, 3, "overlay");

	if (lua_istable(L, -1))
	{
		for (int layer = 1 ; layer < 10 ; layer++)
		{
			lua_pushinteger(L, layer);
			lua_gettable(L, -2);

			if (! lua_isnil(L, -1))
			{
				const char *name = lua_tostring(L, -1);
				T->setBackground(layer, name);
			}

			lua_pop(L, 1);
		}
	}

	lua_pop(L, 1);


	/* tile borders */

	lua_getfield(L, 3, "border8");
	lua_getfield(L, 3, "border4");
	lua_getfield(L, 3, "border6");
	lua_getfield(L, 3, "border2");

	for (int b = 0 ; b < 4 ; b++)
	{
		int idx = lua_gettop(L) - 3 + b;

		if (! lua_isnil(L, idx))
		{
			Fl_Color col = Grab_Color(L, idx);

			T->setBorder((b+1) * 2, col);
		}
	}

	lua_pop(L, 4);

	return 0;
}


//
// LUA: create_sprite() --> id number
//
int gui_create_sprite(lua_State *L)
{
	RE_Sprite * spr = R_NewSprite();

	lua_pushinteger(L, spr->id);
	return 1;
}


//
// LUA: set_sprite(id, props)
//
int gui_set_sprite(lua_State *L)
{
	int id = luaL_checkint(L, 1);

	RE_Sprite * spr = R_FindSprite(id);

	if (! spr)
		return luaL_argerror(L, 1, "invalid sprite id");

	if (lua_type(L, 2) != LUA_TTABLE)
		return luaL_argerror(L, 2, "missing sprite properties");


	/* appearance properties */

	lua_getfield(L, 2, "image");
	if (! lua_isnil(L, -1))
	{
		const char *img_name = lua_tostring(L, -1);
		spr->SetImage(img_name);
	}
	lua_pop(L, 1);

	lua_getfield(L, 2, "visible");
	if (! lua_isnil(L, -1))
		spr->visible = lua_toboolean(L, -1);
	lua_pop(L,1);

	lua_getfield(L, 2, "mirror");
	if (! lua_isnil(L, -1))
		spr->mirror = lua_toboolean(L, -1);
	lua_pop(L,1);

	lua_getfield(L, 2, "height");
	if (! lua_isnil(L, -1))
		spr->height = luaL_checknumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 2, "origin_x");
	if (! lua_isnil(L, -1))
		spr->origin_x = luaL_checknumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 2, "origin_y");
	if (! lua_isnil(L, -1))
		spr->origin_y = luaL_checknumber(L, -1);
	lua_pop(L, 1);


	/* current position */

	lua_getfield(L, 2, "tile_x");
	if (! lua_isnil(L, -1))
		spr->tile_x = luaL_checknumber(L, -1);
	lua_pop(L,1);

	lua_getfield(L, 2, "tile_y");
	if (! lua_isnil(L, -1))
		spr->tile_y = luaL_checknumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 2, "min_scale");
	if (! lua_isnil(L, -1))
		spr->min_scale = luaL_checknumber(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 2, "parent_id");
	if (! lua_isnil(L, -1))
	{
		spr->parent_id = luaL_checkint(L, -1);

		if (spr->parent_id >= 0 && ! R_FindSprite(spr->parent_id))
			return luaL_error(L, "gui.set_sprite: invalid parent_id");
	}
	lua_pop(L, 1);

	lua_getfield(L, 2, "child_height");
	if (! lua_isnil(L, -1))
		spr->child_height = luaL_checknumber(L, -1);
	lua_pop(L, 1);


	/* miscellaneous properties */

	lua_getfield(L, 2, "name");
	if (! lua_isnil(L, -1))
	{
		const char *name = lua_tostring(L, -1);
		spr->SetName(name);
	}
	lua_pop(L, 1);

	lua_getfield(L, 2, "drag_to");
	if (! lua_isnil(L, -1))
		spr->drag_to = Grab_DragTo(L, lua_gettop(L));
	lua_pop(L, 1);

	return 0;
}


static const luaL_Reg gui_functions[] =
{
	{ "raw_print",			gui_raw_print },
	{ "raw_debug_print",	gui_raw_debug_print },
	{ "raw_message",		gui_raw_message },

	{ "version",			gui_version },
	{ "config_line",		gui_config_line },
	{ "change_preference",	gui_change_preference },
	{ "set_char_info",		gui_set_char_info },
	{ "force_quit",			gui_force_quit },

	{ "rand_seed",			gui_rand_seed },
	{ "random",				gui_random },

	{ "sv_exists",			gui_sv_exists },
	{ "sv_delete",			gui_sv_delete },
	{ "sv_open",			gui_sv_open },
	{ "sv_close",			gui_sv_close },
	{ "sv_read_atom",		gui_sv_read_atom },
	{ "sv_write_atom",		gui_sv_write_atom },

	{ "cache_image",		gui_cache_image },

	{ "screen_off",			gui_screen_off },
	{ "screen_begin",		gui_screen_begin },
	{ "create_element",		gui_create_element },
	{ "set_element",		gui_set_element },

	{ "game_state",			gui_game_state },
	{ "set_menu",			gui_set_menu },
	{ "set_stats",			gui_set_stats },

	{ "create_world",		gui_create_world },
	{ "move_home",			gui_move_home },
	{ "set_tile",			gui_set_tile },

	{ "create_sprite",		gui_create_sprite },
	{ "set_sprite",			gui_set_sprite },

	{ NULL, NULL }  /* end of list */
};


//------------------------------------------------------------------------

//
// LUA: bit_and(A, B) --> number
//
int gui_bit_and(lua_State *L)
{
	int A = luaL_checkint(L, 1);
	int B = luaL_checkint(L, 2);

	lua_pushinteger(L, A & B);
	return 1;
}

// LUA: bit_test(val) --> boolean
//
int gui_bit_test(lua_State *L)
{
	int A = luaL_checkint(L, 1);
	int B = luaL_checkint(L, 2);

	lua_pushboolean(L, (A & B) != 0);
	return 1;
}

// LUA: bit_or(A, B) --> number
//
int gui_bit_or(lua_State *L)
{
	int A = luaL_checkint(L, 1);
	int B = luaL_checkint(L, 2);

	lua_pushinteger(L, A | B);
	return 1;
}

// LUA: bit_xor(A, B) --> number
//
int gui_bit_xor(lua_State *L)
{
	int A = luaL_checkint(L, 1);
	int B = luaL_checkint(L, 2);

	lua_pushinteger(L, A ^ B);
	return 1;
}

// LUA: bit_not(val) --> number
//
int gui_bit_not(lua_State *L)
{
	int A = luaL_checkint(L, 1);

	// do not make the result negative
	lua_pushinteger(L, (~A) & 0x7FFFFFFF);
	return 1;
}


static const luaL_Reg bit_functions[] =
{
	{ "band",    gui_bit_and },
	{ "btest",   gui_bit_test },
	{ "bor",     gui_bit_or  },
	{ "bxor",    gui_bit_xor },
	{ "bnot",    gui_bit_not },

	{ NULL, NULL }  /* end of list */
};


//------------------------------------------------------------------------


int Script_RegisterLib(const char *name, const luaL_Reg *reg)
{
	SYS_NULL_CHECK(LUA_ST);

	luaL_register(LUA_ST, name, reg);

	// remove the table which luaL_register created
	lua_pop(LUA_ST, 1);

	return 0;
}


static int p_init_lua(lua_State *L)
{
	/* stop collector during initialization */
	lua_gc(L, LUA_GCSTOP, 0);
	{
		luaL_openlibs(L);  /* open libraries */

		Script_RegisterLib("gui", gui_functions);
		Script_RegisterLib("bit", bit_functions);
	}
	lua_gc(L, LUA_GCRESTART, 0);

	return 0;
}


static void Script_SetScriptPath(lua_State *L, const char *path)
{
	LogPrintf("script_path: [%s]\n", path);

	lua_getglobal(L, "package");

	if (lua_type(L, -1) == LUA_TNIL)
		Main_FatalError("Script problem: no 'package' module!");

	lua_pushstring(L, path);

	lua_setfield(L, -2, "path");

	lua_pop(L, 1);
}


static void Script_CallFunc(const char *func_name, int nresult = 0, const char **params = NULL)
{
	// Note: the results of the function will be on the Lua stack

	lua_getglobal(LUA_ST, "amb_traceback");

	if (lua_type(LUA_ST, -1) == LUA_TNIL)
		Main_FatalError("Script problem: missing function '%s'", "amb_traceback");

	lua_getglobal(LUA_ST, func_name);

	if (lua_type(LUA_ST, -1) == LUA_TNIL)
		Main_FatalError("Script problem: missing function '%s'", func_name);

	int nargs = 0;
	if (params)
	{
		for (; *params; params++, nargs++)
			lua_pushstring(LUA_ST, *params);
	}

	int status = lua_pcall(LUA_ST, nargs, nresult, -2-nargs);
	if (status != 0)
	{
		const char *msg = lua_tolstring(LUA_ST, -1, NULL);

		// skip the filename
		const char *err_msg = strstr(msg, ": ");
		if (err_msg)
			err_msg += 2;
		else
			err_msg = msg;

		Main_FatalError("Script Error: %s", err_msg);
		return; /* NOT REACHED */
	}

	// remove the traceback function
	lua_remove(LUA_ST, -1-nresult);
}


static void Script_Require(const char *name)
{
	char require_text[128];
	sprintf(require_text, "require '%s'", name);

	int status = luaL_loadstring(LUA_ST, require_text);

	if (status == 0)
		status = lua_pcall(LUA_ST, 0, 0, 0);

	if (status != 0)
	{
		const char *msg = lua_tolstring(LUA_ST, -1, NULL);

		Main_FatalError("Unable to load script '%s.lua'\n%s", name, msg);
	}
}


void Script_Open()
{
	DebugPrintf("\n--- OPENING LUA VM ---\n\n");

	// create Lua state

	LUA_ST = luaL_newstate();
	if (! LUA_ST)
		Main_FatalError("LUA Init failed: cannot create new state");

	int status = lua_cpcall(LUA_ST, &p_init_lua, NULL);
	if (status != 0)
		Main_FatalError("LUA Init failed: cannot load standard libs (%d)", status);

	// load main scripts

	LogPrintf("Loading main script: amber.lua\n");

	const char *script_path = StringPrintf("%s/game/?.lua", install_dir);

	Script_SetScriptPath(LUA_ST, script_path);

	Script_Require("amber");

	has_loaded = true;

	LogPrintf("DONE.\n\n");
}


void Script_Close()
{
	if (LUA_ST)
		lua_close(LUA_ST);

	LUA_ST = NULL;

	DebugPrintf("\n--- CLOSED LUA VM ---\n\n");
}


//------------------------------------------------------------------------
// WRAPPERS TO LUA FUNCTIONS
//------------------------------------------------------------------------


void amb_init(const char *mode, int seed, const char *char_info)
{
	char seed_buf[64];

	snprintf(seed_buf, sizeof(seed_buf), "%d", seed);

	const char *params[4];

	params[0] = mode;
	params[1] = seed_buf;
	params[2] = char_info;
	params[3] = NULL;  // end of list

	Script_CallFunc("amb_init", 0, params);

	LogPrintf("\n");
}


void amb_user_action(const char *source, const char *act, const char *param1)
{
	const char *params[4];

	params[0] = source;
	params[1] = act;
	params[2] = param1;
	params[3] = NULL;  // end of list

	Script_CallFunc("amb_user_action", 0, params);
}


void amb_set_preference(const char *key, const char *value)
{
	SYS_NULL_CHECK(key);
	SYS_NULL_CHECK(value);

	if (! has_loaded)
	{
		DebugPrintf("amb_set_preference(%s) called before loaded!\n", key);
		return;
	}

	const char *params[3];

	params[0] = key;
	params[1] = value;
	params[2] = NULL; // end of list

	Script_CallFunc("amb_set_preference", 0, params);
}


void amb_read_preferences(std::vector<std::string> * lines)
{
	if (! has_loaded)
	{
		DebugPrintf("amb_read_preferences() called before loaded!\n");
		return;
	}

	conf_line_buffer = lines;

	Script_CallFunc("amb_read_preferences", 0);

	conf_line_buffer = NULL;
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
