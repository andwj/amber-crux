//----------------------------------------------------------------------
//  MAIN WINDOW
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"

#include "main.h"
#include "m_input.h"
#include "m_cookie.h"

#include "r_sprite.h"


#ifndef WIN32
#include <unistd.h>
#endif

#if (FL_MAJOR_VERSION != 1 ||  \
	 FL_MINOR_VERSION != 3 ||  \
	 FL_PATCH_VERSION < 0)
#error "Require FLTK version 1.3.0 or later"
#endif


#define MIN_WINDOW_W  624
#define MIN_WINDOW_H  380


UI_Window *main_win;

int screen_w;
int screen_h;


int FontSizeForArea(int W, int H, int char_w, int char_h, double scale)
{
	// we assume that characters have the following aspect ratio
	const double char_aspect = 0.8;

	// also assume the line difference is font_h * this
	const double line_factor = 1.5;

	double width  = W / (double)char_w;
	double height = H / (double)char_h;

	double size = MIN(height / line_factor, width / char_aspect);

	int result = (int)(size * scale);

#if 0
	// make the result an even number (10, 12, 14, etc)
	result += (result & 1);
#endif

	// ensure the result is fairly sane
	return CLAMP(6, result, 240);
}


static void main_win_close_CB(Fl_Widget *w, void *data)
{
	if (game_state == GAME_Active)
		want_quit = QUIT_Save;
	else
		want_quit = QUIT_Immediately;
}


//
// Window Constructor
//
UI_Window::UI_Window(int W, int H, const char *title) :
	Fl_Double_Window(W, H, title),
	panel(NULL), text_box(NULL),
	map(NULL), screen(NULL),
	text_active(false), screen_active(false),
	action(ACT_NONE),
	act_mouse_x(0), act_mouse_y(0),
	act_sprite(NULL), act_target(0)
{
	size_range(MIN_WINDOW_W, MIN_WINDOW_H);

	callback((Fl_Callback *) main_win_close_CB);

	// the PANEL and MAP widgets cover the whole window, and always
	// draw their own background.
	box(FL_NO_BOX);


	int PAN_W = 100;

	int TEXT_H = 50;


	panel = new UI_Panel(0, 0, PAN_W, H);

	text_box = new UI_TextArea(4 /* max_lines */, PAN_W, 0, W - PAN_W, TEXT_H);

	map = new UI_Map(PAN_W, TEXT_H, W - PAN_W, H - TEXT_H);

	screen = new UI_Screen(PAN_W, 0, W - PAN_W, H);


	end();

	resizable(NULL);

	HideChildren();
}


//
// Window Destructor
//
UI_Window::~UI_Window()
{ }


void UI_Window::InitialSize(int *W, int *H)
{
	// int diff_x = screen_w - MIN_WINDOW_W;
	// int diff_y = screen_h - MIN_WINDOW_H;

	*W = *H = 1;

//!!!!! FIXME : temporary while developing
#if 1
	*W = 924;
	*H = 680;
#endif

	if (*W < MIN_WINDOW_W) *W = MIN_WINDOW_W;
	if (*H < MIN_WINDOW_H) *H = MIN_WINDOW_H;
}


int UI_Window::CalcPanelWidth(int W, int H)
{
	// want panel to have aspect of 1:3
	int w1 = H / 3;

	// but map should occupy most of the available width
	int w2 = W / 5;

	return MIN(w1, w2);
}


int UI_Window::CalcTextHeight(int W, int H)
{
	int font_h = FontSizeForArea(W, H, 54, 25);

	text_box->setFontSize(font_h);

	return text_box->CalcHeight();
}


void UI_Window::drawDragObject()
{
	fl_push_clip(0, 0, w(), h());

	SYS_ASSERT(act_sprite);

	R_RenderDragSprite(act_sprite, act_mouse_x, act_mouse_y);

	fl_pop_clip();
}


void UI_Window::draw()
{
	Fl_Double_Window::draw();

	// FIXME : draw separators here

	if (action == ACT_DragObject)
		drawDragObject();
}


void UI_Window::resize(int X, int Y, int W, int H)
{
	remove(panel);
	remove(text_box);
	remove(map);
	remove(screen);

	Fl_Group::resize(X, Y, W, H);

	add(panel);
	add(text_box);
	add(map);
	add(screen);

	PositionChildren();
}


int UI_Window::handle(int event)
{
	// treat mouse motion specially, as Fl_Group::handle always eats it
	if (event == FL_MOVE || event == FL_DRAG)
	{
		Input_RawMouse(event);
	}

	// try the normal handling...
	if (Fl_Double_Window::handle(event))
		return 1;

	// otherwise we will process a keyboard or mouse event
	switch (event)
	{
		case FL_KEYDOWN:
		case FL_KEYUP:
		case FL_SHORTCUT:
			return Input_RawKey(event);

		case FL_PUSH:
		case FL_RELEASE:
			return Input_RawButton(event);

		case FL_MOUSEWHEEL:
			return Input_RawWheel(event);

		default:
			return 0;
	}
}


void UI_Window::ShowText(int enable)
{
	text_active = (enable > 0);

	HideChildren();
}


void UI_Window::ShowScreen(int enable)
{
	bool new_active = (enable > 0);

	if (new_active == screen_active)
		return;

	screen_active = new_active;

	HideChildren();
}


void UI_Window::HideChildren()
{
	if (screen_active)
	{
		screen->show();
		text_box->hide();
		map->hide();
	}
	else
	{
		map->show();
		screen->hide();

		if (text_active)
			text_box->show();
		else
			text_box->hide();

		PositionChildren();
	}
}


void UI_Window::PositionChildren()
{
	int W = w();
	int H = h();

	int PAN_W = CalcPanelWidth(W, H);

	int TEXT_H = text_active ? CalcTextHeight(W, H) : 0;


	// support panel being on the right (normally on left)
	int px = 0;
	int mx = PAN_W;

	if (pref_panel_right)
	{
		mx = 0;
		px = W - PAN_W;
	}


	// support textbox being at top of screen (normally under the map)
	int my = 0;
	int ty = H - TEXT_H;

	if (pref_text_top)
	{
		ty = 0;
		my = TEXT_H;
	}


	panel   ->resize(px,  0, PAN_W, H);
	text_box->resize(mx, ty, W - PAN_W, TEXT_H);
	map     ->resize(mx, my, W - PAN_W, H - TEXT_H);
	screen  ->resize(mx,  0, W - PAN_W, H);
}


void UI_Window::ResetEverything()
{
	Action_Clear();

	  text_active = false;
	screen_active = false;

	HideChildren();

	 panel->ResetEverything();
	   map->ResetEverything();
	screen->ResetEverything();
	text_box->ResetEverything();
}


void UI_Window::UpdateArrangement()
{
	resize(x(), y(), w(), h());
	redraw();
}


void UI_Window::Action_Set(user_action_e act, RE_Sprite *spr)
{
	if (action == act && act_sprite == spr)
		return;

	Action_Clear();

	action     = act;
	act_sprite = spr;

	// grab mouse location
	act_mouse_x = Fl::event_x();
	act_mouse_y = Fl::event_y();

	switch (action)
	{
		// TODO : more stuff....

		case ACT_ScrollMap:
			fl_cursor(FL_CURSOR_MOVE);
			break;

		case ACT_ScrollText:
			fl_cursor(FL_CURSOR_MOVE);
			break;

		case ACT_ZoomMap:
			act_pos = map->getScale();
			fl_cursor(FL_CURSOR_NS);
			break;

		case ACT_Highlight:
			SYS_ASSERT(spr);
			if (spr->isDraggable())
			    fl_cursor(FL_CURSOR_HAND);
			break;

		case ACT_DragObject:
			SYS_ASSERT(spr);
			fl_cursor(FL_CURSOR_HAND);
			break;

		default: break;
	}

	// TODO : be a bit smarter about redrawing whole screen
	redraw();
}


void UI_Window::Action_Clear()
{
	switch (action)
	{
		case ACT_NONE:
			return;

		// do stuff....

		default: break;
	}

	action = ACT_NONE;
	act_sprite = NULL;
	act_target = 0;

	fl_cursor(FL_CURSOR_DEFAULT);

	// TODO : be a bit smarter about redrawing whole screen
	redraw();
}


void UI_Window::Action_DropObject()
{
	// TODO !

	Action_Clear();
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
