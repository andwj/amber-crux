//----------------------------------------------------------------------
//  THE PANEL (STATS / MENU / ETC)
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __UI_PANEL_H__
#define __UI_PANEL_H__


class UI_MenuButton;


#define MAX_MENU_ITEMS  10


class UI_Panel : public Fl_Group
{
private:
	int font_h;
	int title_font_h;

	Fl_Box *title_box;

public:
	UI_Stats *stat_box;

private:
	UI_MenuButton * items[MAX_MENU_ITEMS];

	int num_items;

public:
	UI_Panel(int X, int Y, int W, int H);
	virtual ~UI_Panel();

	/* FLTK methods */
	void resize(int X, int Y, int W, int H);

	void draw();

public:
	void ResetEverything();

	// API for creating a menu
	void BeginMenu(const char *title);
	void AddMenu(const char *act, const char *label,
				 const char *param1 = NULL, const char *param2 = NULL);
	void FinishMenu();

private:
	void ClearItems();

	void CalcFontHeight(int W, int H);

	void RepositionMenu();

};


#endif  /* __UI_PANEL_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
