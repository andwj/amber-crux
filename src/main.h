//----------------------------------------------------------------------
//  MAIN DEFINITIONS
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __AMBER_MAIN_H__
#define __AMBER_MAIN_H__

#define AMBER_TITLE  "Amber Crux"

#define AMBER_VERSION  "0.45"
#define AMBER_HEX_VER  0x045


#define PREFS_FILENAME  "prefs.txt"
#define LOG_FILENAME    "log.txt"


extern const char *home_dir;
extern const char *install_dir;


typedef enum
{
	GAME_NotStarted = 0,
	GAME_Active,
	GAME_Finished   // player won or died

} game_state_type_e;

extern game_state_type_e  game_state;


typedef enum
{
	QUIT_NO = 0,

	CONFIRM_Quit,		// ask user to confirm quitting
	CONFIRM_Discard,	// ask to confirm discarding active game

	QUIT_Immediately,	// quit now (e.g. after user confirmed it)
	QUIT_Save,			// save game and quit

	RESTART_Menu,		// as if the user quit then restarted
	RESTART_Fresh,		// start a new game (after user died)
	RESTART_Replay,		// replay the same game (after user died)

} quit_type_e;

extern quit_type_e want_quit;


// Misc Options
extern bool debug_messages;


// this needed for replaying a game
extern const char *next_char_info;

extern int next_rand_seed;


void Main_FatalError(const char *msg, ...);
bool Main_BackupFile(const char *filename, const char *ext);
void Main_RemoveLockFile();
void Main_Ticker();

void Main_SpecialAction(const char *act, const char *param);


#endif /* __AMBER_MAIN_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
