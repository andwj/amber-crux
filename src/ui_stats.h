//----------------------------------------------------------------------
//  PLAYER STATS
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __UI_STATS_H__
#define __UI_STATS_H__


class UI_Stats : public Fl_Group
{
private:
	int health;
	int mana;
	int armor;
	int gold;

	image_c * icon_bg;
	image_c * icon_bg_dest;

	image_c * weapon_icon;
	image_c * hold_icon;
	image_c * spell_icon;

	int font_h;

public:
	UI_Stats(int X, int Y, int W, int H);
	virtual ~UI_Stats();

	// get ready for a game
	void Prepare();

	/* FLTK methods */
	void resize(int X, int Y, int W, int H);

	void draw();

public:
	void ResetEverything();

	void setHealth(int new_health)  { health = new_health; Update(); }
	void setMana  (int new_mana)    { mana   = new_mana;   Update(); }
	void setArmor (int new_armor)   { armor  = new_armor;  Update(); }
	void setGold  (int new_gold)    { gold   = new_gold;   Update(); }

	void setWeaponIcon(const char *name);
	void setHoldIcon  (const char *name);
	void setSpellIcon (const char *name);

	// returns a DRAGTO_xxx value, or zero if mouse does not hover over a slot
	int MouseOverSlot(int mouse_x, int mouse_y);

private:
	void CalcFontHeight(int W, int H);

	void DrawStat(const char *name, int cx, int cy,
	              int value, Fl_Color color,
				  const char *prefix = NULL);

	void Update();
};


#endif  /* __UI_STATS_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
