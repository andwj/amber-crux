//----------------------------------------------------------------------
//  ERROR DIALOG
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2014 Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "main.h"


static int dialog_result;

static void dialog_close_callback(Fl_Widget *w, void *data)
{
	dialog_result = 0;
}

static void dialog_button_callback(Fl_Widget *w, void *data)
{
	dialog_result = (long)data;
}


static int DialogShowAndRun(char icon_type, const char *message,
		const char *title, std::vector<std::string> *labels = NULL)
{
	dialog_result = -1;

	// dialog window and font sizes will be based on the main
	// window size.
	int main_width  = screen_w;
	int main_height = screen_h;

	if (! main_width)
	{
		main_width  = 800;
		main_height = 500;
	}

	if (main_win)
	{
		main_width  = main_win->w();
		main_height = main_win->h();
	}

	int font_h = 4 + FontSizeForArea(main_width, main_height, 75, 40);

	// determine required size
	int mesg_W = 140 + main_width / 3;
	int mesg_H = 0;

	fl_font(FL_HELVETICA, font_h);
	fl_measure(message, mesg_W, mesg_H);

	if (mesg_W < 150 + font_h * 10)
		mesg_W = 150 + font_h * 10;

	if (mesg_H < 8 + font_h * 3)
		mesg_H = 8 + font_h * 3;

	// add a little wiggle room
	mesg_W += font_h * 2;
	mesg_H += font_h;

	int ICON_SIZE = font_h * 2 + font_h / 2;

	int BUT_H = font_h * 2 - 4;

	int total_W = 10 + ICON_SIZE + 10 + mesg_W + 10;
	int total_H = 10 + mesg_H + 10;

	int GROUP_H = BUT_H * 2 + 8;

	total_H += GROUP_H;


	// create window...
	Fl_Double_Window *dialog = new Fl_Double_Window(total_W, total_H, title);

	dialog->size_range(total_W, total_H, total_W, total_H);
	dialog->callback((Fl_Callback *) dialog_close_callback);


	// create the error icon...
	Fl_Box *icon = new Fl_Box(10, 10 + (mesg_H - ICON_SIZE) / 2, ICON_SIZE, ICON_SIZE, "");

	icon->box(FL_OVAL_BOX);
	icon->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
	icon->labelfont(FL_HELVETICA_BOLD);
	icon->labelsize(font_h * 2);

	if (icon_type == '!')
	{
		icon->label("!");
		icon->color(FL_RED, FL_RED);
		icon->labelcolor(FL_WHITE);
	}
	else if (icon_type == '?')
	{
		icon->label("?");
		icon->color(FL_GREEN, FL_GREEN);
		icon->labelcolor(FL_BLACK);
	}
	else
	{
		icon->label("i");
		icon->color(FL_BLUE, FL_BLUE);
		icon->labelcolor(FL_WHITE);
	}


	// create the message area...
	Fl_Box *box = new Fl_Box(ICON_SIZE + 20, 10, mesg_W, mesg_H, message);

	box->align(FL_ALIGN_LEFT | FL_ALIGN_INSIDE | FL_ALIGN_WRAP);
	box->labelfont(FL_HELVETICA);
	box->labelsize(font_h);


	// create buttons...

	Fl_Button *focus_button = NULL;

	Fl_Group *b_group = new Fl_Group(0, total_H - GROUP_H, total_W, GROUP_H);
	b_group->box(FL_FLAT_BOX);
	b_group->color(FL_DARK3, FL_DARK3);
	b_group->end();

	int but_count = labels ? (int)labels->size() : 1;

	int but_x = total_W - font_h * 3;
	int but_y = b_group->y() + (GROUP_H - BUT_H) / 2;

	for (int b = but_count - 1 ; b >= 0 ; b--)
	{
		const char *text = labels ? (*labels)[b].c_str() :
		                   (icon_type == '?') ? "OK" : "Close";

		int b_width = fl_width(text) + font_h * 2;

		Fl_Button *button = new Fl_Button(but_x - b_width, but_y, b_width, BUT_H, text);

		button->box(FL_ROUND_UP_BOX);
		button->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
		button->callback((Fl_Callback *) dialog_button_callback, (void *)(long)b);
		button->labelsize(font_h);
		button->visible_focus(0);

		b_group->insert(*button, 0);

		but_x = but_x - b_width - font_h * 3;

		// left-most button should get the focus
		focus_button = button;
	}

	dialog->end();


	// show time!
	if (focus_button)
		dialog->hotspot(focus_button);

	dialog->set_modal();
	dialog->show();

	if (icon_type == '!')
		fl_beep();

///??	if (focus_button)
///??		Fl::focus(focus_button);


	// run the GUI and let user make their choice
	while (dialog_result < 0)
	{
		Fl::wait();
	}

	// delete window (automatically deletes child widgets)
	delete dialog;

	return dialog_result;
}


static void ParseButtons(const char *buttons,
                         std::vector<std::string>& labels)
{
	for (;;)
	{
		const char *p = strchr(buttons, '|');

		if (! p)
		{
			labels.push_back(buttons);
			return;
		}

		int len = (int)(p - buttons);
		SYS_ASSERT(len > 0);

		labels.push_back(std::string(buttons, len));

		buttons = p + 1;
	}
}


//------------------------------------------------------------------------

static char dialog_buffer[MSG_BUF_LEN];


void DLG_ShowError(const char *msg, ...)
{
	va_list arg_pt;

	va_start (arg_pt, msg);
	vsnprintf (dialog_buffer, MSG_BUF_LEN, msg, arg_pt);
	va_end (arg_pt);

	dialog_buffer[MSG_BUF_LEN-1] = 0;

	DialogShowAndRun('!', dialog_buffer, AMBER_TITLE " : Fatal Error");
}


void DLG_Notify(const char *msg, ...)
{
	va_list arg_pt;

	va_start (arg_pt, msg);
	vsnprintf (dialog_buffer, MSG_BUF_LEN, msg, arg_pt);
	va_end (arg_pt);

	dialog_buffer[MSG_BUF_LEN-1] = 0;

	DialogShowAndRun('i', dialog_buffer, AMBER_TITLE " : Important Notice");
}


int DLG_Confirm(const char *buttons, const char *msg, ...)
{
	va_list arg_pt;

	va_start (arg_pt, msg);
	vsnprintf (dialog_buffer, MSG_BUF_LEN, msg, arg_pt);
	va_end (arg_pt);

	dialog_buffer[MSG_BUF_LEN-1] = 0;

	std::vector<std::string> labels;

	ParseButtons(buttons, labels);

	return DialogShowAndRun('?', dialog_buffer, AMBER_TITLE " : Confirmation",
							&labels);
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
