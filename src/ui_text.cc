//----------------------------------------------------------------------
//  TEXT MESSAGES
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "main.h"

#include "aplib/util.h"


// FIXME: move into a common header?
#define PANEL_BG  fl_rgb_color(48, 46, 44)
#define CONSOLE_BG  fl_rgb_color(0,54,72)

#define TEXT_BG  CONSOLE_BG


#define ARROW_COLOR  fl_rgb_color(0, 128, 255)

#define SIDE_MARGIN    4
#define BOTTOM_MARGIN  4



class TextChunk_c
{
public:
	const char *text;

	bool hyphen;

	Fl_Color color;

	int gap;

public:
	TextChunk_c(const char *_text, int len, bool _hyphen) :
		hyphen(_hyphen),
		color(FL_WHITE),
		gap(2)
	{
		text = StringDup(_text, len);
	}

	~TextChunk_c()
	{
		StringFree(text);
	}

	void draw(int& cur_x, int cur_y)
	{
		fl_color(color);

		int text_W = fl_width(text);

		fl_draw(text, cur_x, cur_y);

		cur_x += text_W;

		if (hyphen)
		{
			fl_draw("-", cur_x, cur_y);
			cur_x += fl_width("-");
		}

		if (gap)
			cur_x += fl_width(" ") * gap;
	}
};


class TextLine_c
{
public:
	std::vector< TextChunk_c* > chunks;

public:
	TextLine_c() : chunks()
	{ }

	~TextLine_c()
	{
		for (unsigned int k = 0 ; k < chunks.size() ; k++)
			delete chunks[k];
	}

	void add(TextChunk_c *T)
	{
		chunks.push_back(T);
	}

	void draw(int cur_x, int cur_y)
	{
		for (unsigned int k = 0 ; k < chunks.size() ; k++)
		{
			chunks[k]->draw(cur_x, cur_y);
		}
	}

	int CalcWidth() const
	{
		int width = 0;

		for (unsigned int k = 0 ; k < chunks.size() ; k++)
		{
			width += fl_width(chunks[k]->text);
			width += fl_width("  ");
		}

		return width;
	}
};


//----------------------------------------------------------------------

UI_TextArea::UI_TextArea(int _view, int X, int Y, int W, int H) :
    Fl_Widget(X, Y, W, H, NULL),
	view_lines(_view),
	keep_lines(100),
	lines(),
	font_h(10),
	line_h(15)
{
	box(FL_NO_BOX);

	NewLine();

	ScrollToTop();
}


UI_TextArea::~UI_TextArea()
{
	Clear();
}


void UI_TextArea::ResetEverything()
{
	Clear();
	NewLine();
	ScrollToTop();
}


void UI_TextArea::resize(int X, int Y, int W, int H)
{
	Fl_Widget::resize(X, Y, W, H);

// TODO
}


void UI_TextArea::draw()
{
	fl_push_clip(x(), y(), w(), h());

	fl_color(TEXT_BG);
	fl_rectf(x(), y(), w(), h());

	// when scrolled back, draw arrows at bottom of window
	if (scroll_dy > 0)
	{
		DrawArrows();
	}

	fl_font(FL_HELVETICA, font_h);

	int bottom_y = y() + h() - BOTTOM_MARGIN - font_h/4 + scroll_dy * line_h;

	int num_lines = (int)lines.size();

	// if last line is blank, pretend it did not exist
	// [ draw previous line where last line would have been ]
	if (LastLineWidth() == 0)
		bottom_y += line_h;

	int min_y = y() - line_h;
	int max_y = y() + h();

	for (int k = 0 ; k < num_lines ; k++)
	{
		int ky = bottom_y - (num_lines - 1 - k) * line_h;

		if (ky < min_y || ky > max_y)
			continue;

		lines[k]->draw(x() + SIDE_MARGIN, ky);
	}

	fl_pop_clip();
}


void UI_TextArea::DrawArrows()
{
	fl_color(ARROW_COLOR);

	int step_w = line_h * 4 / 3;
	int tri_h  = line_h / 3;

	int by = y() + h();

	for (int tx = x() + step_w / 2 ; tx < x() + w() - tri_h ; tx += step_w)
	{
		fl_polygon(tx + tri_h, by, tx - tri_h, by, tx, by - tri_h);
	}
}


void UI_TextArea::setFontSize(int new_font_h)
{
	font_h = new_font_h;
	line_h = font_h * 3 / 2;
}


int UI_TextArea::CalcHeight() const
{
	return view_lines * line_h + BOTTOM_MARGIN;
}


void UI_TextArea::Clear()
{
	for (unsigned int k = 0 ; k < lines.size() ; k++)
		delete lines[k];

	lines.clear();

	scroll_dy = 0;
}


void UI_TextArea::ScrollToTop()
{
	scroll_dy = 1.0 - view_lines;
}


void UI_TextArea::NewLine()
{
	// throw away excess lines
	while ((int)lines.size() >= (keep_lines + view_lines))
	{
		delete lines[0];

		lines.erase(lines.begin());
	}

	lines.push_back(new TextLine_c);

	if (scroll_dy < 0)
		scroll_dy = MIN(0, scroll_dy + 1.0);
}


void UI_TextArea::AddChunk(const char *str, int len, bool hyphen)
{
	SYS_ASSERT(! lines.empty());

	TextChunk_c *T = new TextChunk_c(str, len, hyphen);

	lines.back()->add(T);
}


int UI_TextArea::LastLineWidth() const
{
	if (lines.empty())
		return 0;
	
	return lines.back()->CalcWidth();
}


void UI_TextArea::AddMessage(const char *msg)
{
	redraw();

	// if scrolled up, come back down
	if (scroll_dy > 0)
		scroll_dy = 0;

	if (*msg == '\f')
	{
		msg++;

		if (LastLineWidth() > 0)
			NewLine();

		NewLine();
		ScrollToTop();
	}

	fl_font(FL_HELVETICA, font_h);

	while (*msg)
	{
		// can force a new-line
		if (*msg == '\n')
		{
			NewLine();

			msg++; continue;
		}

		// skip spaces at beginning of a line / chunk
		if (isspace(*msg))
		{
			msg++; continue;
		}

		// find longest string which will fit on current line
		int space = w() - 8 - LastLineWidth();

		int len = 0;

		while (msg[len] && fl_width(msg, len + 1) < space)
			len++;

		if (! msg[len])
		{
			// no wrapping needed
			AddChunk(msg, len);
			return;
		}

		// if very little room left, begin a new line
		if (len < 5)
		{
			NewLine();
			continue;
		}

		// try to end chunk at a word boundary
		int min_len = MAX(len - 9, 3);

		for (int i = len ; i >= min_len ; i--)
		{
			if (isspace(msg[i]))
			{
				len = i;
				break;
			}
		}

		// add a hyphen if a word was split across two lines
		bool hyphen = false;

		if (isalpha(msg[len-1]) && isalpha(msg[len]))
			hyphen = true;

		AddChunk(msg, len, hyphen);

		NewLine();

		msg += len;
	}
}


void UI_TextArea::Scroll(double delta)
{
	scroll_dy = scroll_dy + delta;
	
	double min_dy = 1.0 - view_lines;
	double max_dy = (int)lines.size() - view_lines;

	scroll_dy = CLAMP(min_dy, scroll_dy, max_dy);

	redraw();
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
