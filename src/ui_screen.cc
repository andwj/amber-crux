//----------------------------------------------------------------------
//  SCREEN WIDGET (for Help, Options, etc)
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "main.h"
#include "m_lua.h"

#include "aplib/util.h"

#include "r_render.h"
#include "r_sprite.h"


#define KEY_DELIM  '|'


Screen_element_c::Screen_element_c() :
	kind(ELEM_INVALID),
	x(0), y(0), w(-1), h(-1),
	scale(1.0),
	text(NULL),
	image(NULL),
	align(FL_ALIGN_LEFT | FL_ALIGN_TOP),
	color(FL_WHITE),
	visible(true),
	mirror(false),
	val(0.0), maxval(1.0)
{
	opt_name[0] = 0;
}

Screen_element_c::~Screen_element_c()
{
	clearText();
}


void Screen_element_c::clearText()
{
	if (text)
	{
		StringFree(text);
		text = NULL;
	}
}

void Screen_element_c::setText(const char *new_text)
{
	clearText();

	text = StringDup(new_text);
}

void Screen_element_c::setOptName(const char *new_name)
{
	strncpy(opt_name, new_name, sizeof(opt_name));
	opt_name[sizeof(opt_name) - 1] = 0;
}

void Screen_element_c::setImage(const char *filename)
{
	image = R_CacheImage(filename);
}


//------------------------------------------------------------------------

UI_Screen::UI_Screen(int X, int Y, int W, int H) :
		Fl_Widget(X, Y, W, H, NULL),
		virt_W(1), virt_H(1),
		char_W(1), char_H(1),
		elements(),
		drag_elem(NULL)
{ }


UI_Screen::~UI_Screen()
{
	Clear();
}


void UI_Screen::Clear()
{
	// free all the elements
	for (unsigned int k = 0 ; k < elements.size() ; k++)
		delete elements[k];

	elements.clear();

	drag_elem = NULL;
}


void UI_Screen::ResetEverything()
{
	Clear();
}


void UI_Screen::CalcTransform(int sx, int sy, int sw, int sh)
{
	double aspect1 = virt_W / (double)virt_H;
	double aspect2 =     sw / (double)sh;

	rx = sx;  rw = sw;
	ry = sy;  rh = sh;

	if (aspect1 < aspect2)
	{
		rw = (int)(rh * aspect1);
		rx += (sw - rw) / 2;

		r_scale = sh / (double)virt_H;
	}
	else
	{
		rh = (int)(rw / aspect1);
		ry += (sh - rh) / 2;

		r_scale = sw / (double)virt_W;
	}
}


int Screen_element_c::countKeys() const
{
	SYS_ASSERT(text);

	int count = 1;

	const char *pos = text;

	for ( ; *pos ; pos++)
		if (*pos == KEY_DELIM)
			count++;

	return count;
}


const char * Screen_element_c::currentKeyText() const
{
	static char buffer[256];

	SYS_ASSERT(text);

	int which = (int)val;

	const char *pos = text;

	for ( ; which > 0 ; which--)
	{
		while (*pos && *pos != KEY_DELIM)
			pos++;

		if (*pos == 0)
			break;

		pos++;
	}

	if (*pos == 0)
		return "????";

	char *dest = buffer;

	while (*pos && *pos != KEY_DELIM && dest < &buffer[sizeof(buffer)-2])
		*dest++ = *pos++;

	*dest = 0;

	return buffer;
}


void UI_Screen::drawText(Screen_element_c * E)
{
	int font_h = FontSizeForArea(rw, rh, char_W, char_H, E->scale);

	fl_color(E->color);
	fl_font(FL_HELVETICA, font_h);

	int text_W = 0;
	int text_H = 0;
	fl_measure(E->text, text_W, text_H);

	int sx = WINDOW_X(E->x);
	int sy = WINDOW_Y(E->y);

	/* alignment */

	if (E->align & FL_ALIGN_LEFT)
	{ /* no change */ }
	else if (E->align & FL_ALIGN_RIGHT)
		sx -= text_W;
	else
		sx -= text_W / 2;

	if (E->align & FL_ALIGN_TOP)
		sy += font_h / 8;
	else if (E->align & FL_ALIGN_BOTTOM)
		sy -= text_H + font_h / 8;
	else
		sy -= text_H / 2;

	// increase box size a bit
	sx -= 2 ; text_W += 4;
	sy -= 2 ; text_H += 4;

	/* DEBUG : fl_rect(sx, sy, text_W, text_H); */

	fl_draw(E->text, sx, sy, text_W, text_H, E->align, NULL, 0 /* draw symbols */);
}


void UI_Screen::drawImage(Screen_element_c * E)
{
	if (! E->image)
		return;

	int sx = WINDOW_X(E->x);
	int sy = WINDOW_Y(E->y);

	int w = (int)(E->w * r_scale);
	int h = (int)(E->h * r_scale);

	// either the width or height can be absent, in which case
	// we calculate it (preserving the aspect of the image).
	if (E->w < 0)
		w = E->image->width * h / E->image->height;
	else if (E->h < 0)
		h = E->image->height * w / E->image->width;

	if (w < 3 || h < 3)  // too small
		return;

	/* alignment */

	if (E->align & FL_ALIGN_LEFT)
	{ /* no change */ }
	else if (E->align & FL_ALIGN_RIGHT)
		sx -= w;
	else
		sx -= w / 2;

	if (E->align & FL_ALIGN_TOP)
	{ /* no change */ }
	else if (E->align & FL_ALIGN_BOTTOM)
		sy -= h;
	else
		sy -= h / 2;

	R_DrawImage(E->image, sx, sy, w, h, E->mirror ? DIF_MIRROR : 0);
}


void UI_Screen::drawCheckButton(Screen_element_c * E)
{
	int w1 = FontSizeForArea(rw, rh, char_W, char_H, E->scale);

	int sx = WINDOW_X(E->x);
	int sy = WINDOW_Y(E->y);

	w1 = MAX(w1, 6);

	int w2 = w1 * 7 / 10;

	fl_draw_box(FL_OFLAT_BOX, sx - w1/2, sy - w1/2, w1, w1, fl_rgb_color(160,160,160));

	if (E->val > 0.5)
		fl_draw_box(FL_OFLAT_BOX, sx - w2/2, sy - w2/2, w2, w2, FL_BLUE);
}


void UI_Screen::drawSlider(Screen_element_c * E)
{
	int font_h = FontSizeForArea(rw, rh, char_W, char_H, E->scale);

	int sx = WINDOW_X(E->x);
	int sy = WINDOW_Y(E->y);

	int w1 = WINDOW_X(E->x + E->w) - sx;
	int h1 = font_h * 3 / 2;

	if (w1 < 10) w1 = 10;
	if (h1 < 6)  h1 = 6;

	int bw = h1 / 6;
	int bh = h1 / 6;

	fl_color(fl_rgb_color(160,160,160));

	fl_rectf(sx, sy, w1, h1);

	int bar_w = (int)((w1 - bw * 2) * E->val / E->maxval);

	if (bar_w > 0)
	{
		fl_color(FL_BLUE);

		fl_rectf(sx + bw, sy + bh, bar_w, h1 - bh * 2);
	}


	/* draw the numeric value */

	static char buffer[256];

	if (E->maxval > 99)
		sprintf(buffer, "%1.0f", E->val);
	else if (E->maxval > 9.9)
		sprintf(buffer, "%1.1f", E->val);
	else
		sprintf(buffer, "%1.2f", E->val);

	int number_h = FontSizeForArea(rw, rh, char_W, char_H, E->scale * 0.7);

	fl_color(FL_WHITE);
	fl_font(FL_COURIER, number_h);

	int text_W = 0;
	int text_H = 0;
	fl_measure(buffer, text_W, text_H);

	fl_draw(buffer, sx + w1 + 8 * r_scale, sy + text_H);
}


void UI_Screen::drawWordSel(Screen_element_c * E)
{
	int font_h = FontSizeForArea(rw, rh, char_W, char_H, E->scale);

	int sx = WINDOW_X(E->x);
	int sy = WINDOW_Y(E->y);

	int sw = WINDOW_X(E->x + E->w) - sx;
	int sh = font_h * 3 / 2;

	if (sh < 6) sh = 6;

	fl_color(fl_rgb_color(160,160,160));

	fl_rectf(sx, sy, sw, sh);


	/* draw the current choice */

	const char *buffer = E->currentKeyText();

	fl_color(FL_BLACK);
	fl_font(FL_COURIER, font_h);

	int text_W = 0;
	int text_H = 0;
	fl_measure(buffer, text_W, text_H);

	fl_draw(buffer, sx, sy, sw, sh, FL_ALIGN_CENTER, NULL, 0 /* symbols */);


	/* draw the arrows */

	int aw = sh;

	fl_color(FL_WHITE);
	fl_draw("@<", sx, sy, aw, sh, FL_ALIGN_CENTER, NULL, 1 /* symbols */);

	fl_color(FL_WHITE);
	fl_draw("@>", sx + sw - aw, sy, aw, sh, FL_ALIGN_CENTER, NULL, 1);
}


void UI_Screen::drawElement(Screen_element_c * E)
{
	if (! E->visible)
		return;

	switch (E->kind)
	{
		case ELEM_Text:
			drawText(E);
			break;

		case ELEM_Image:
			drawImage(E);
			break;

		case ELEM_CheckButton:
			drawCheckButton(E);
			break;

		case ELEM_WordSel:
			drawWordSel(E);
			break;

		case ELEM_Slider:
			drawSlider(E);
			break;

		default:
			break;
	}
}


void UI_Screen::draw()
{
	int sx = x();
	int sy = y();
	int sw = w();
	int sh = h();

	CalcTransform(sx, sy, sw, sh);

	fl_push_clip(sx, sy, sw, sh);

	// fill the background
	fl_color(color());
	fl_rectf(sx, sy, sw, sh);

	// draw all elements
	for (unsigned int k = 0 ; k < elements.size() ; k++)
	{
		drawElement(elements[k]);
	}

	fl_pop_clip();
}


bool UI_Screen::touchesElement(Screen_element_c *E, int mx, int my)
{
	switch (E->kind)
	{
		case ELEM_CheckButton:
			{
				int sx = WINDOW_X(E->x);
				int sy = WINDOW_Y(E->y);

				int sw = 4 + FontSizeForArea(rw, rh, char_W, char_H, E->scale);

				sw = MAX(sw, 6);

				sx -= sw / 2;
				sy -= sw / 2;

				return (mx >= sx && mx <= sx+sw && my >= sy && my <= sy+sw);
			}

		case ELEM_Slider:
			{
				int font_h = FontSizeForArea(rw, rh, char_W, char_H, E->scale);

				int sx = WINDOW_X(E->x) - 4;
				int sy = WINDOW_Y(E->y) - 2;
				int sw = WINDOW_X(E->x + E->w) - sx + 8;
				int sh = font_h * 3 / 2 + 4;

				return (mx >= sx && mx <= sx+sw && my >= sy && my <= sy+sh);
			}

		case ELEM_WordSel:
			{
				int font_h = FontSizeForArea(rw, rh, char_W, char_H, E->scale);

				int sx = WINDOW_X(E->x) - 4;
				int sy = WINDOW_Y(E->y) - 2;
				int sw = WINDOW_X(E->x + E->w) - sx + 8;
				int sh = font_h + font_h / 2 + 4;
#if 0
				int mid_x = sx + sw /2;

				// middle area is not clickable (ambiguous)
				if (mx >= mid_x - sh && mx <= mid_x + sh)
					return false;
#endif
				return (mx >= sx && mx <= sx+sw && my >= sy && my <= sy+sh);
			}

		default: return false;
	}
}


void UI_Screen::toggleCheckButton(Screen_element_c *E)
{
	E->val = (E->val < 0.5) ? 1 : 0;

	// send new value to the scripts
	amb_user_action("control", E->opt_name, (E->val > 0.5) ? "true" : "false");

	redraw();
}


void UI_Screen::toggleWordSel(Screen_element_c *E)
{
	// increment or decrement?
	int mid_x = WINDOW_X(E->x + E->w * 0.5);

	int dir = (Fl::event_x() < mid_x) ? -1 : +1;

	E->val = E->val + dir;

	if (E->val < 0)
		E->val = E->maxval;
	else if (E->val > E->maxval)
		E->val = 0;

	// send new value to the scripts
	static char buffer[64];
	sprintf(buffer, "%1.0f", E->val);

	amb_user_action("control", E->opt_name, buffer);

	redraw();
}


int UI_Screen::handle_drag()
{
	if (drag_elem)
	{
		Screen_element_c *E = drag_elem;

		int sx = WINDOW_X(E->x);
		int sw = WINDOW_X(E->x + E->w) - sx;

		if (sw < 2) sw = 2;

		double along = (Fl::event_x() - sx) / (double)sw;

		E->val = CLAMP(0, along, 1) * E->maxval;

		// send new value to the scripts
		static char buffer[64];
		sprintf(buffer, "%1.5f", E->val);

		amb_user_action("control", E->opt_name, buffer);

		redraw();
	}

	return 1;
}


int UI_Screen::handle_push()
{
	// find control to affect
	for (unsigned int k = 0 ; k < elements.size() ; k++)
	{
		Screen_element_c *E = elements[k];

		if (E->kind < ELEM_FIRST_CONTROL)
			continue;

		if (! touchesElement(E, Fl::event_x(), Fl::event_y()))
			continue;

		switch (E->kind)
		{
			case ELEM_CheckButton:
				toggleCheckButton(E);
				return 1;

			case ELEM_Slider:
				drag_elem = E;
				return handle_drag();

			case ELEM_WordSel:
				toggleWordSel(E);
				return 1;

			default: break;
		}
	}

	// not found
	return 0;
}


int UI_Screen::handle(int event)
{
	// check if should stop dragging something
	switch (event)
	{
		case FL_RELEASE:
		case FL_LEAVE:
		case FL_UNFOCUS:
		case FL_HIDE:
			drag_elem = NULL;
			break;
	}

	switch (event)
	{
		case FL_ENTER:
			return 1;

		case FL_LEAVE:
			return 1;

		case FL_PUSH:
			return handle_push();

		case FL_DRAG:
			return handle_drag();

		case FL_MOVE:
			/* nothing needed right now */
			break;

		default: break;
	}

	return 0;  // event not handled
}


//------------------------------------------------------------------------


void UI_Screen::Begin(int W, int H, int ch_W, int ch_H)
{
	virt_W = W;
	virt_H = H;

	char_W = ch_W;
	char_H = ch_H;

	Clear();

	redraw();
}


int UI_Screen::CreateElement()
{
	Screen_element_c *E = new Screen_element_c();

	elements.push_back(E);

	return (int) elements.size();
}


Screen_element_c * UI_Screen::FindElement(int id)
{
	if (id < 1 || id > (int)elements.size())
		return NULL;

	return elements[id - 1];
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
