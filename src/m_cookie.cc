//----------------------------------------------------------------------
//  COOKIE : Save/Load user preferences
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "hdr_lua.h"

#include "aplib/argv.h"
#include "aplib/util.h"

#include "main.h"
#include "m_cookie.h"
#include "m_lua.h"


// preference vars

bool pref_panel_right = false;
bool pref_text_top    = false;


typedef enum
{
	CCTX_Load = 0,
	CCTX_Save,
	CCTX_Arguments
}
cookie_context_e;


static FILE *cookie_fp;

static cookie_context_e context;


// this is called by the scripts via gui.change_preference
// (generally from amb_set_preference)

void Cookie_ChangePreference(const char *name, const char *value)
{
//  fprintf(stderr, "Change_Pref : %s = '%s'\n", name, value);

	bool bool_val = false;

	if (strcmp(value, "true") == 0 || value[0] == '1')
		bool_val = true;

	if (StringCaseCmp(name, "panel_right") == 0)
	{
		pref_panel_right = bool_val;

		if (main_win)
			main_win->UpdateArrangement();
	}
	else if (StringCaseCmp(name, "text_top") == 0)
	{
		pref_text_top = bool_val;

		if (main_win)
			main_win->UpdateArrangement();
	}
	else
	{
		// ignore unknown (game-side) preferences
	}
}


static void Cookie_SetValue(const char *name, const char *value)
{
	if (context == CCTX_Load)
		DebugPrintf("CONFIG: Name: [%s] Value: [%s]\n", name, value);
	else if (context == CCTX_Arguments)
		DebugPrintf("ARGUMENT: Name: [%s] Value: [%s]\n", name, value);

	amb_set_preference(name, value);
}


static bool Cookie_ParseLine(char *buf)
{
	// remove whitespace
	while (isspace(*buf))
		buf++;

	int len = strlen(buf);

	while (len > 0 && isspace(buf[len-1]))
		buf[--len] = 0;

	// ignore blank lines and comments
	if (*buf == 0)
		return true;

	if (buf[0] == '-' && buf[1] == '-')
		return true;

	// curly brackets are just for aesthetics : ignore them
	if (*buf == '{' || *buf == '}')
		return true;

	if (! (isalpha(*buf) || *buf == '@'))
	{
		LogPrintf("Weird preferences line: [%s]\n", buf);
		return false;
	}

	// Righteo, line starts with an identifier.  It should be of the
	// form "name = value".  We'll terminate the identifier, and pass
	// the name/value strings to the matcher function.

	const char *name = buf;

	for (buf++ ; isalnum(*buf) || *buf == '_' || *buf == '.' ; buf++)
	{ /* nothing here */ }

	while (isspace(*buf))
		*buf++ = 0;

	if (*buf != '=')
	{
		LogPrintf("Prefs line missing '=': [%s]\n", buf);
		return false;
	}

	*buf++ = 0;

	while (isspace(*buf))
		buf++;

	if (*buf == 0)
	{
		LogPrintf("Prefs line missing value!\n");
		return false;
	}

	Cookie_SetValue(name, buf);
	return true;
}


//------------------------------------------------------------------------

bool Cookie_Load(const char *filename)
{
	context = CCTX_Load;

	cookie_fp = fopen(filename, "r");

	if (! cookie_fp)
	{
		LogPrintf("Missing prefs file, using defaults.\n\n");
		return false;
	}

	BigPrintf("Loading Preferences...\n");

	// simple line-by-line parser
	char buffer[MSG_BUF_LEN];

	int error_count = 0;

	while (fgets(buffer, MSG_BUF_LEN-2, cookie_fp))
	{
		if (! Cookie_ParseLine(buffer))
			error_count += 1;
	}

	if (error_count > 0)
		LogPrintf("DONE (found %d parse errors)\n\n", error_count);
	else
		LogPrintf("DONE.\n\n");

	fclose(cookie_fp);

	return true;
}


bool Cookie_Save(const char *filename)
{
	context = CCTX_Save;

	cookie_fp = fopen(filename, "w");

	if (! cookie_fp)
	{
		BigPrintf("Error: unable to create file: %s\n(%s)\n\n",
				filename, strerror(errno));
		return false;
	}

	LogPrintf("\n");
	BigPrintf("Saving Preferences...\n");

	// header...
	fprintf(cookie_fp, "-- PREFERENCES\n"); 
	fprintf(cookie_fp, "-- " AMBER_TITLE " " AMBER_VERSION " (C) 2016 Andrew Apted\n");
#if 0
	fprintf(cookie_fp, "-- http://amber.xxx.net/\n\n");
#endif
	fprintf(cookie_fp, "\n");


	std::vector<std::string> lines;

	amb_read_preferences(&lines);

	for (unsigned int i = 0 ; i < lines.size() ; i++)
	{
		fprintf(cookie_fp, "%s\n", lines[i].c_str());
	}

	LogPrintf("DONE.\n");
	BigPrintf("\n");

	fclose(cookie_fp);

	return true;
}


void Cookie_ParseArguments(void)
{
	context = CCTX_Arguments;

	for (int i = 0 ; i < arg_count ; i++)
	{
		const char *arg = arg_list[i];

		if (arg[0] == '-')
			continue;

		if (arg[0] == '{' || arg[0] == '}')
			continue;

		const char *eq_pos = strchr(arg, '=');

		if (! eq_pos)
			continue;

		// split argument into name/value pair
		int eq_offset = (eq_pos - arg);

		char *name = StringDup(arg);
		char *value = name + eq_offset + 1;

		name[eq_offset] = 0;

		if (name[0] == 0 || value[0] == 0)
			Main_FatalError("Bad setting on command line: '%s'\n", arg);

		Cookie_SetValue(name, value);

		StringFree(name);
	}
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
