//----------------------------------------------------------------------
//  ALL INCLUDES
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014 Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __ALL_HEADERS_H__
#define __ALL_HEADERS_H__

/* OS specifics */
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

/* C library */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <math.h>
#include <limits.h>
#include <errno.h>
#include <time.h>

/* STL goodies */

#include <string>
#include <vector>
#include <list>
#include <map>

/* Our own system defs */

#include "aplib/types.h"
#include "aplib/macros.h"
#include "aplib/assert.h"
#include "aplib/endian.h"

#include "m_debug.h"

class image_c;

/* FLTK and UI definitions */

#include "hdr_fltk.h"
#include "hdr_ui.h"

//
// Note: "hdr_lua.h" is not here -- it is only needed by a few code files
//

#define MSG_BUF_LEN  2000

#endif /* __ALL_HEADERS_H__ */

