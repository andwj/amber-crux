//----------------------------------------------------------------------
//  MAIN WINDOW
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __UI_WINDOW_H__
#define __UI_WINDOW_H__


#define WINDOW_BG  fl_gray_ramp(3)

#define BUILD_BG   fl_gray_ramp(4)


class RE_Sprite;


// certain actions the user may be doing and which take some time
// (e.g. requiring the mouse button held down while moving)
typedef enum
{
	ACT_NONE = 0,

	ACT_ScrollMap,
	ACT_ScrollText,
	ACT_ZoomMap,

	ACT_Highlight,
	ACT_DragObject

} user_action_e;


class UI_Window : public Fl_Double_Window
{
public:
	// main child widgets

	UI_Panel *panel;

	UI_TextArea * text_box;

	UI_Map   *map;

	UI_Screen *screen;

	bool text_active;
	bool screen_active;

	// current action by user, if any
	user_action_e  action;

	// mouse position when user_action was set
	int act_mouse_x;
	int act_mouse_y;

	// the object we are highlighting or dragging
	RE_Sprite * act_sprite;

	double act_pos;  // used for UI_ZoomMap

	// non-zero when we can drop a dragged object (a DRAGTO_xxx value)
	int act_target;

public:
	UI_Window(int W, int H, const char *title);
	virtual ~UI_Window();

	static void InitialSize(int *W, int *H);

	void ShowText(int enable);
	void ShowScreen(int enable);

	// reset all widgets to initial state
	void ResetEverything();

	// arrangement has changed (esp. preferences)
	void UpdateArrangement();

	// user operations
	void Action_Set(user_action_e act, RE_Sprite * spr = NULL);
	void Action_Clear();

	void Action_DropObject();

	/* FLTK methods */

	void draw();
	void resize(int X, int Y, int W, int H);
	int  handle(int event);

private:
	int CalcPanelWidth(int W, int H);
	int CalcTextHeight(int W, int H);

	void HideChildren();
	void PositionChildren();

	void drawDragObject();
};


extern int screen_w;
extern int screen_h;

extern UI_Window * main_win;

// determine largest font size for a rectangle of W x H pixels which
// will fit the given number of character columns and rows.  The
// intermediate result is multiplied by the 'scale' parameter.
int FontSizeForArea(int W, int H, int char_w, int char_h, double scale = 1.0);


#endif /* __UI_WINDOW_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
