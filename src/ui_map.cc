//----------------------------------------------------------------------
//  MAP AREA WIDGET
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"

#include "main.h"
#include "m_lua.h"
#include "m_cookie.h"

#include "aplib/util.h"

#include "r_render.h"
#include "r_sprite.h"


// size of the current world (# of squares)
int world_w = 1;
int world_h = 1;

// tile we zoom into when pressing HOME key (usually where the player is)
int home_tile_x = 1;
int home_tile_y = 1;


//----------------------------------------------------------------------

Tile_c::Tile_c(int TX, int TY) :
	tx(TX), ty(TY),
	sx(0), sy(0), sw(0), sh(0),
	visible(false)
{
	for (unsigned int k = 0 ; k < TILE_LAYERS ; k++)
		background[k] = NULL;

	memset(border, 0, sizeof(border));
}

Tile_c::~Tile_c()
{ }


void Tile_c::setBackground(int layer, const char *filename)
{
	SYS_ASSERT(layer >= 0);

	// silently ignore extra layers
	if (layer >= TILE_LAYERS)
		return;

	if (background[layer])
		R_UncacheImage(background[layer]);

	if (filename && filename[0])
		background[layer] = R_CacheImage(filename);
	else
		background[layer] = NULL;
}


void Tile_c::setBorder(int dir, Fl_Color color)
{
	if (dir < 2 || dir > 8)
		return;

	border[(dir - 1) / 2].line = color;
}


void Tile_c::drawArrow(int b)
{
	// FIXME
}


void Tile_c::drawBorder(int b)
{
	if (! border[b].line)
		return;

	int lx1, ly1, lx2, ly2;

	lx1 = sx;
	ly1 = sy;
	lx2 = sx + sw;
	ly2 = sy + sh;

	switch (b)
	{
		case 0: /* south */ ly1 -= 3; ly2 = ly1; break;
		case 1: /* west  */ lx1 -= 3; lx2 = lx1; break;
		case 2: /* east  */ lx2 += 3; lx1 = lx2; break;
		case 3: /* north */ ly2 += 3; ly1 = ly2; break;
	}

	fl_color(border[b].line);
	fl_rectf(lx1, ly1, lx2 - lx1 + 2, ly2 - ly1 + 2);
}


void Tile_c::draw()
{
	// This mapping draws the solid background underneath all overlays,
	// and for overlays it does bottom first, then middle, and top last.
	//
	// Generally corners will be mutually exclusive of the nearby sides
	// (e.g. if top-left corner is present, top and left will be absent).
	static const unsigned int layer_map[10] = { 0, 2,1,3, 5,4,6, 8,7,9 };

	for (unsigned int k = 0 ; k < TILE_LAYERS ; k++)
	{
		unsigned int k2 = layer_map[k];

		if (! background[k2])
			continue;

		int img_w = background[k2]->width;
		int img_h = background[k2]->height;

		int nx = sx, ny = sy, nw = sw, nh = sh;

		// handle alignment of overlays
		switch (k2)
		{
			case 2:
				nh = nw * img_h / img_w;
				ny = ny + (sh - nh);
				break;

			case 8:
				nh = nw * img_h / img_w;
				break;

			case 4:
				nw = nh * img_w / img_h;
				break;

			case 6:
				nw = nh * img_w / img_h;
				nx = nx + (sw - nw);
				break;

			default: break;
		}

		R_DrawImage(background[k2], nx, ny, nw, nh);
	}

	for (int b = 0 ; b < 4 ; b++)
	{
		drawBorder(b);
		drawArrow(b);
	}
}


//----------------------------------------------------------------------

UI_Map::UI_Map(int X, int Y, int W, int H) :
	Fl_Widget(X, Y, W, H, NULL),
	mid_x(1), mid_y(1), scale(32), pad(0),
	home_square(NULL)
{
	memset(tiles, 0, sizeof(tiles));

	// we draw the background ourselves
	box(FL_NO_BOX);

	ComputeLimits();
	ComputePad();
}


UI_Map::~UI_Map()
{
}


void UI_Map::ComputePad()
{
#if 0
	pad = 1 + scale / 18;

#if 0
	// more padding on very high resolution displays
	if (screen_w + screen_h > 2090)
		pad *= 2;
#endif
#endif
}


void UI_Map::ComputeLimits()
{
	min_scale = 32.0;
	max_scale = 65536.0;

	min_mid_x = 1;
	min_mid_y = 1;

	max_mid_x = world_w + 1;
	max_mid_y = world_h + 1;

	ApplyLimits();
}


void UI_Map::ApplyLimits()
{
	if (mid_x < min_mid_x) mid_x = min_mid_x;
	if (mid_x > max_mid_x) mid_x = max_mid_x;

	if (mid_y < min_mid_y) mid_y = min_mid_y;
	if (mid_y > max_mid_y) mid_y = max_mid_y;

	if (scale < min_scale) scale = min_scale;
	if (scale > max_scale) scale = max_scale;

	// I assume that this function is called after changing one of the
	// position values, and hence no need here to redraw() or similar.
}


void UI_Map::resize(int X, int Y, int W, int H)
{
	int old_h = h();

	Fl_Widget::resize(X, Y, W, H);

	scale = scale * (double)H / (double)old_h;

	RepositionTiles();
}


void UI_Map::draw()
{
	fl_push_clip(x(), y(), w(), h());

	// fill the background
	fl_color(FL_BLACK);
	fl_rectf(x(), y(), w(), h());

	if (tiles[0][0])
	{
///		DrawBorder();

		DrawTiles();

		R_RenderSprites();

		DrawHomeSquare();
	}

	// draw a separator between the MAP and TEXTAREA widgets
	int sep_h = 2 + h() / 340;

	fl_color(fl_rgb_color(96, 80, 64));
	fl_rectf(x(), y() + (pref_text_top ? 0 : h() - sep_h), w(), sep_h);

	fl_pop_clip();
}


int UI_Map::handle(int event)
{
	if (event == FL_ENTER || event == FL_LEAVE)
		return 1;

	return Fl_Widget::handle(event);	
}


void UI_Map::DrawBorder()
{
	int gap = 10;

	int x1 = WINDOW_X(1) - gap;
	int y1 = WINDOW_Y(1) - gap;

	int x2 = WINDOW_X(world_w + 1) + gap;
	int y2 = WINDOW_Y(world_h + 1) + gap;

	int w = x2 - x1;
	int h = y2 - y1;

	fl_color(fl_rgb_color(64, 64, 128));

	fl_rect(x1 - 0, y1 - 0, w + 0, h + 0);
	fl_rect(x1 - 1, y1 - 1, w + 2, h + 2);
}


void UI_Map::DrawTiles()
{
	for (int tx = 0 ; tx < world_w ; tx++)
	for (int ty = 0 ; ty < world_h ; ty++)
	{
		Tile_c *T = tiles[tx][ty];

		if (T->visible)
			T->draw();
	}
}


void UI_Map::DrawHomeSquare()
{
	if (! home_square)
		return;

	Tile_c *T = tiles[home_tile_x - 1][home_tile_y - 1];

	int sx = T->sx - T->sw / 8;
	int sy = T->sy - T->sh / 8;

	int sw = T->sw + T->sw / 4;
	int sh = T->sh + T->sh / 4;

	R_DrawImage(home_square, sx, sy, sw, sh);
}


void UI_Map::Populate()
{
	SYS_ASSERT(world_w > 0);
	SYS_ASSERT(world_h > 0);

	for (int tx = 0 ; tx < world_w ; tx++)
	for (int ty = 0 ; ty < world_h ; ty++)
	{
		tiles[tx][ty] = new Tile_c(1+tx, 1+ty);
	}

	ZoomOut();

	home_square = R_CacheImage("ui/home_square");
}


void UI_Map::Unpopulate()
{
	for (int tx = 0 ; tx < world_w ; tx++)
	for (int ty = 0 ; ty < world_h ; ty++)
	{
		if (! tiles[tx][ty])
			continue;

		// uncache background/overlay images
		for (int bg = 0 ; bg < TILE_LAYERS ; bg++)
			tiles[tx][ty]->setBackground(bg, NULL);

		delete tiles[tx][ty];
		tiles[tx][ty] = NULL;
	}

	SYS_ASSERT(tiles[0][0] == NULL);
}


void UI_Map::ResetEverything()
{
	Unpopulate();

	world_w = 1;
	world_h = 1;

	mid_x = 1;
	mid_y = 1;
	scale = 32;
	pad   = 0;

	ComputeLimits();
	ComputePad();

	redraw();
}


Tile_c * UI_Map::getTile(int tx, int ty)
{
	SYS_ASSERT(1 <= tx && tx <= world_w);
	SYS_ASSERT(1 <= ty && ty <= world_h);

	Tile_c * T = tiles[tx - 1][ty - 1];

	return T;
}


void UI_Map::RepositionTiles()
{
	ComputeLimits();
	ComputePad();

	if (! tiles[0][0])
		return;

	int iscale = I_ROUND(scale);

	for (int tx = 0 ; tx < world_w ; tx++)
	for (int ty = 0 ; ty < world_h ; ty++)
	{
		Tile_c *T = tiles[tx][ty];

		T->sx = WINDOW_X(tx + 1);
		T->sy = WINDOW_Y(ty + 1);

		T->sw = iscale;
		T->sh = iscale;
	}
}


// convert a tile position to a window (FLTK) coordinate
int UI_Map::WINDOW_X(double tile_x) const
{
	int factor = I_ROUND(scale) + pad;

	return x() + w() / 2 + (tile_x - mid_x) * factor + pad / 2;
}

int UI_Map::WINDOW_Y(double tile_y) const
{
	int factor = I_ROUND(scale) + pad;

	return y() + h() / 2 + (tile_y - mid_y) * factor + pad / 2;
}


double UI_Map::TILE_X(double sx) const
{
	int factor = I_ROUND(scale) + pad;

	return mid_x + (sx - x() - w() / 2) / (double)factor;
}

double UI_Map::TILE_Y(double sy) const
{
	int factor = I_ROUND(scale) + pad;

	return mid_y + (sy - y() - h() / 2) / (double)factor;
}


void UI_Map::Scroll(double dx, double dy)
{
	double tile_h = h() / (scale + pad);
	double tile_w = w() / (scale + pad);

	mid_x += tile_w * dx;
	mid_y += tile_h * dy;

	RepositionTiles();
	redraw();
}


void UI_Map::Drag(int sdx, int sdy, double speed)
{
	int factor = I_ROUND(scale) + pad;

	double dx = sdx / (double)factor;
	double dy = sdy / (double)factor;

	mid_x -= dx * speed;
	mid_y -= dy * speed;

	RepositionTiles();
	redraw();
}


void UI_Map::MoveTo(double new_x, double new_y, double new_scale)
{
	if (new_x > 0) mid_x = new_x;
	if (new_y > 0) mid_y = new_y;

	if (new_scale > 0) scale = new_scale;

	RepositionTiles();
	redraw();
}


void UI_Map::Zoom(double factor, int mouse_x, int mouse_y)
{
	if (mouse_x < 0) mouse_x = x() + w() / 2;
	if (mouse_y < 0) mouse_y = y() + h() / 2;

	double prev_scale = scale;

	double old_x = TILE_X(mouse_x);
	double old_y = TILE_Y(mouse_y);


	scale = scale * factor;

	ApplyLimits();


	double dist_factor = 1.0 - prev_scale / scale;

	mid_x += (old_x - mid_x) * dist_factor;
	mid_y += (old_y - mid_y) * dist_factor;

	RepositionTiles();
	redraw();
}


void UI_Map::ZoomWithMouse(int delta_y, double orig_scale, int mouse_x, int mouse_y)
{
	double prev_scale = scale;

	double old_x = TILE_X(mouse_x);
	double old_y = TILE_Y(mouse_y);


	double factor = pow(1.008, 400.0 * abs(delta_y) / (double)h());

	if (delta_y > 0) factor = 1.0 / factor;


	scale = orig_scale * factor;

	ApplyLimits();


	double dist_factor = 1.0 - prev_scale / scale;

	mid_x += (old_x - mid_x) * dist_factor;
	mid_y += (old_y - mid_y) * dist_factor;

	RepositionTiles();
	redraw();
}


void UI_Map::ZoomIn(int tile_x, int tile_y)
{
	tile_x = CLAMP(1, tile_x, world_w);
	tile_y = CLAMP(1, tile_y, world_h);

	mid_x = tile_x + 0.5;
	mid_y = tile_y + 0.5;

	int gap = h() / 8;

	double tx = TILE_X(x() + w() - gap) - TILE_X(x() + gap);
	double ty = TILE_Y(y() + h() - gap) - TILE_Y(y() + gap);

	scale = scale * MIN(tx, ty);

	RepositionTiles();
	redraw();
}


void UI_Map::ZoomOut()
{
	if (! tiles[0][0])
		return;

	int x1, y1, x2, y2;

	SeenTileRange(&x1, &y1, &x2, &y2);

	// when no tiles are visible, nothing to zoom out to
	if (x2 == 0)
		return;

	mid_x = (x1 + x2 + 1.0) / 2.0;
	mid_y = (y1 + y2 + 1.0) / 2.0;

	int gap = h() / 10;

	double tx = (w() - gap * 2) / (double)(x2 - x1 + 1);
	double ty = (h() - gap * 2) / (double)(y2 - y1 + 1);

	scale = MIN(tx, ty);

	RepositionTiles();
	redraw();
}


void UI_Map::GoHome()
{
	ZoomIn(home_tile_x, home_tile_y);
}


void UI_Map::ChangeHome(int new_home_x, int new_home_y)
{
// 	int old_home_x = home_tile_x;
// 	int old_home_y = home_tile_y;

	home_tile_x = new_home_x;
	home_tile_y = new_home_y;

	// convert map area bounds into tile coords
	int gap = h() / 8;

	double tx1 = TILE_X(x() + gap / 2);
	double ty1 = TILE_Y(y() + gap / 2);
	double tx2 = TILE_X(x() + w() - gap / 2);
	double ty2 = TILE_Y(y() + h() - gap / 2);

	// zoom out if map is zoomed in very far

	double tdx = tx2 - tx1;
	double tdy = ty2 - ty1;

	double max_scale = scale * MIN(tdx, tdy);

	if (scale >= max_scale * 0.9)
	{
		GoHome();
		return;
	}


	// move horizontally

	if (new_home_x < tx1)
	{
		mid_x -= (tx1 - new_home_x);
	}
	else if (new_home_x + 1 > tx2)
	{
		mid_x += (new_home_x + 1 - tx2);
	}


	// move vertically

	if (new_home_y < ty1)
	{
		mid_y -= (ty1 - new_home_y);
	}
	else if (new_home_y + 1 > ty2)
	{
		mid_y += (new_home_y + 1 - ty2);
	}


	RepositionTiles();
	redraw();
}


void UI_Map::SeenTileRange(int *x1, int *y1, int *x2, int *y2)
{
	*x1 = *y1 = 99;
	*x2 = *y2 = 0;

	for (int tx = 1 ; tx <= world_w ; tx++)
	for (int ty = 1 ; ty <= world_h ; ty++)
	{
		Tile_c * T = tiles[tx - 1][ty - 1];

		if (T->visible)
		{
			*x1 = MIN(*x1, tx);
			*y1 = MIN(*y1, ty);
			*x2 = MAX(*x2, tx);
			*y2 = MAX(*y2, ty);
		}
	}
	
	// none at all?
	if (*x1 > *x2)
	{
		*x1 = *y1 = *x2 = *y2 = 0;
	}
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
