//----------------------------------------------------------------------
//  THE PANEL (STATS / MENU / ETC)
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"

#include "main.h"
#include "m_lua.h"
#include "m_cookie.h"

#include "aplib/util.h"


class UI_MenuButton : public Fl_Button
{
public:
	const char *act_str;
	const char *par1_str;
	const char *par2_str;

public:
	UI_MenuButton(int X, int Y, int W, int H,
				  const char *act, const char *label,
				  const char *param1, const char *param2) :
		Fl_Button(X, Y, W, H)
	{
		 act_str = StringDup(act);
		par1_str = param1 ? StringDup(param1) : NULL;
		par2_str = param2 ? StringDup(param2) : NULL;

		box(FL_ROUND_UP_BOX);

		copy_label(label);

//		labelcolor(FL_WHITE);

		visible_focus(0);

		if (StringCaseCmp(label, "OK") == 0)
			shortcut(FL_Enter);
	}

	virtual ~UI_MenuButton()
	{
		StringFree( act_str);
		StringFree(par1_str);
		StringFree(par2_str);
	}

public:
	static void push_callback(Fl_Widget *w, void *data)
	{
		UI_MenuButton *but = (UI_MenuButton *)w;

		if (but->act_str[0] == '!')
			Main_SpecialAction(but->act_str, but->par1_str);
		else
			amb_user_action("menu", but->act_str, but->par1_str);
	}
};


//----------------------------------------------------------------------


// #define PANEL_BG  fl_rgb_color(160, 128, 96)
#define PANEL_BG  fl_rgb_color(54, 47, 45)


UI_Panel::UI_Panel(int X, int Y, int W, int H) :
    Fl_Group(X, Y, W, H, NULL)
{
	box(FL_FLAT_BOX);

	color(PANEL_BG, PANEL_BG);


	num_items = 0;
	memset(items, 0, sizeof(items));


	int cx = X + 40;
	int cy = Y + 30;

	title_box = new Fl_Box(cx, cy, X + 10, Y + 5, "");
	title_box->labelcolor(FL_WHITE);
	title_box->align(FL_ALIGN_INSIDE /*|FL_ALIGN_LEFT*/ | FL_ALIGN_TOP);


	stat_box = new UI_Stats(X, Y + H - H/2, W, H/2);


	end();

	clip_children(1);

	resizable(NULL);

	CalcFontHeight(W, H/2);
}


UI_Panel::~UI_Panel()
{
}


void UI_Panel::resize(int X, int Y, int W, int H)
{
	CalcFontHeight(W, H/2);

	remove(stat_box);

	Fl_Group::resize(X, Y, W, H);

	stat_box->resize(X, Y + H - H/2, W, H/2);

	add(stat_box);

	RepositionMenu();
}


void UI_Panel::draw()
{
	Fl_Group::draw();

	// draw a separator between the PANEL and MAP widgets
	int sep_w = 3 + w() / 240;
	int sep_h = 2 + h() / 340;

	fl_color(fl_rgb_color(96, 80, 64));

	fl_rectf(x() + (pref_panel_right ? 0 : w() - sep_w), y(), sep_w, h());

	// draw a separator between the MENU and STATS widgets
	fl_rectf(x(), y() + h() - h()/2 - sep_h, w(), sep_h);
}


void UI_Panel::ResetEverything()
{
	title_box->label("");

	ClearItems();

	stat_box->ResetEverything();

	redraw();
}


void UI_Panel::ClearItems()
{
	for (int i = 0 ; i < num_items ; i++)
	{
		remove(items[i]);

		delete items[i];
		items[i] = NULL;
	}

	num_items = 0;
}


void UI_Panel::CalcFontHeight(int W, int H)
{
	font_h = FontSizeForArea(W, H, 14, 14);

	title_font_h = FontSizeForArea(W, H, 14, 14, 1.4);

//	fprintf(stderr, "font_h for %d x %d --> %d\n", W, H, font_h);
}


void UI_Panel::BeginMenu(const char *title)
{
	title_box->copy_label(title);

	ClearItems();
}

void UI_Panel::AddMenu(const char *act, const char *label,
					   const char *param1, const char *param2)
{
	if (num_items >= MAX_MENU_ITEMS)
		return;

	// dummy coords, will be fixed by RepositionMenu()...

	UI_MenuButton * but = new UI_MenuButton(0, 0, 60, 20, act, label, param1, param2);
	but->callback(UI_MenuButton::push_callback, NULL);

	if (label[0] == 0)
		but->hide();

	items[num_items++] = but;

	add(but);
}

void UI_Panel::FinishMenu()
{
	RepositionMenu();

	redraw();
}


void UI_Panel::RepositionMenu()
{
	// do title
	int title_h = title_font_h * 2;

	remove(title_box);

	title_box->resize(x() + 2, y() + 2, w() - 4, title_h);
	title_box->labelsize(title_font_h);

	add(title_box);

	// do buttons

	for (int i = 0 ; i < num_items ; i++)
	{
		UI_MenuButton *but = items[i];

		remove(but);

		int cx = x() + w() / 8;
		int cw = w() * 6 / 8;
		int ch = font_h *  3 / 2;
		int cy = title_h + i * (font_h * 2);

		but->resize(cx, cy, cw, ch);
		but->labelsize(font_h);

		add(but);
	}
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
