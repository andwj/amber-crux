//----------------------------------------------------------------------
//  SPRITES (MONSTERS, ITEMS, DECOR etc)
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "hdr_lua.h"

#include <algorithm>   // std::sort

#include "aplib/util.h"

#include "main.h"

#include "r_render.h"
#include "r_sprite.h"


/* this would fit about 6 humanoids vertically in each world tile */
#define OBJ_SCALE_DIVISOR  (6.0 * 100.0)


std::vector< RE_Sprite * > all_sprites;

static std::vector< RE_Sprite * > draw_sprites;

static double map_scale;


RE_Sprite::RE_Sprite() :
	id(-1),
	visible(false),
	tile_x(0),
	tile_y(0),
	img(NULL),
	origin_x(0.5),
	origin_y(1.0),
	height(100),
	mirror(false),
	min_scale(64),
	name(NULL),
	parent_id(-1),
	child_height(25),
	drag_to(0),
	sw(0), sh(0)
{ }


RE_Sprite::~RE_Sprite()
{
	// Note: img is cached, no real need to uncache it

	if (name)
		StringFree(name);
}


void RE_Sprite::SetImage(const char *filename)
{
	if (img)
		R_UncacheImage(img);
	
	if (filename)
		img = R_CacheImage(filename);
	else
		img = NULL;
}


void RE_Sprite::SetName(const char *new_name)
{
	if (name)
		StringFree(name);
	
	if (new_name)
		name = StringDup(new_name);
	else
		name = NULL;
}


void RE_Sprite::Project()
{
	// calculate size of sprite

	double new_h = map_scale * height / OBJ_SCALE_DIVISOR;

	sw = img->width * new_h / (double)img->height;
	sh = new_h;

	if (sw < 3 || sh < 3)
	{
		sw = sh = 0;
		return;
	}

	// determine where on the screen the sprite will sit

	double ty = world_h + 2.0 - tile_y;

	sx = main_win->map->WINDOW_X(tile_x);
	sy = main_win->map->WINDOW_Y(ty);

	if (mirror)
		sx -= sw * (1.0 - origin_x);
	else
		sx -= sw * origin_x;

	sy -= sh * origin_y;
}


void RE_Sprite::ProjectChild(const RE_Sprite *parent)
{
	// calculate size of sprite
	// [ the height field is now percentage of the parent height ]

	double new_h = parent->sh * child_height / 100.0;

	sw = img->width * new_h / (double)img->height;
	sh = new_h;

	if (sw < 3 || sh < 3)
	{
		sw = sh = 0;
		return;
	}

	// determine location from parent
	// [ tile_x/y fields are now a position within the parent image ]

	sx = parent->sx + parent->sw * tile_x;
	sy = parent->sy + parent->sh * tile_y;

	if (mirror)
		sx -= sw * (1.0 - origin_x);
	else
		sx -= sw * origin_x;

	sy -= sh * origin_y;
}


void RE_Sprite::ProjectDrag(int mx, int my)
{
	// calculate size of sprite

	double new_h = height;  // FIXME : should be bigger/smaller based on UI_Stat icon size

	sw = img->width * new_h / (double)img->height;
	sh = new_h;

	if (sw < 3 || sh < 3)
	{
		sw = sh = 0;
		return;
	}

	// determine location  [ always centered for dragging ]

	sx = mx - 0.7 * sw;
	sy = my - 0.7 * sh;
}


void R_ClearSprites(void)
{
	for (unsigned int k = 0 ; k < all_sprites.size() ; k++)
	{
		all_sprites[k]->SetImage(NULL);

		delete all_sprites[k];
	}
	
	all_sprites.clear();
}


RE_Sprite * R_NewSprite(void)
{
	RE_Sprite * sp = new RE_Sprite();

	sp->id = 1 + (int)all_sprites.size();

	// add it to global list
	all_sprites.push_back(sp);

	return sp;
}


RE_Sprite * R_FindSprite(int id)
{
	if (id < 1 || id > (int)all_sprites.size())
		return NULL;
	
	return all_sprites[id - 1];
}


void R_TestSpriteCode(void)
{
	for (int i = 0 ; i < 4 ; i++)
	{
		RE_Sprite * sp = R_NewSprite();

		sp->tile_x = 1.4 + i * 0.02;
		sp->tile_y = 1.9 - i * 0.04;

		sp->SetImage("player/male_idle");
	}
}


struct sprite_TILEY_CMP
{
	inline bool operator() (const RE_Sprite * A, const RE_Sprite * B) const
	{
		// ensure parented sprites are drawn _AFTER_ non-parented ones
		int A_parent = (A->parent_id >= 0);
		int B_parent = (B->parent_id >= 0);

		if (A_parent != B_parent)
			return A_parent < B_parent;

		return A->tile_y > B->tile_y;
	}
};


void R_RenderSprites(void)
{
	// bounding box
	int bx1 = main_win->map->x();
	int by1 = main_win->map->y();

	int bx2 = bx1 + main_win->map->w();
	int by2 = by1 + main_win->map->h();

	// map scale
	map_scale = main_win->map->getScale();


	/* collect all visible sprites */

	unsigned int k;

	// perform two passes : normal sprites first, parented sprites second
	// [ because parented sprites need their parent to be projected ]

	for (int pass = 0 ; pass < 2 ; pass++)
	{
		for (k = 0 ; k < all_sprites.size() ; k++)
		{
			RE_Sprite *sp = all_sprites[k];

			if (pass == 0 && sp->parent_id >= 0) continue;
			if (pass == 1 && sp->parent_id  < 0) continue;

			sp->sw = sp->sh = 0;

			if (! sp->visible || ! sp->img)
				continue;

			// checks for parented sprites
			// [ we ignore min_scale on the child ]

			if (sp->parent_id > 0)
			{
				RE_Sprite *parent = R_FindSprite(sp->parent_id);

				SYS_ASSERT(parent);

				if (! parent->visible)
					continue;

				if (map_scale < parent->min_scale)
					continue;

				sp->ProjectChild(parent);
			}
			else
			{
				// would be too small?
				if (map_scale < sp->min_scale)
					continue;

				sp->Project();
			}

			// projection was too small?
			if (! sp->sw)
				continue;

			// skip sprites which are completely outside bbox
			if (sp->sx >= bx2 || sp->sx + sp->sw <= bx1 ||
				sp->sy >= by2 || sp->sy + sp->sh <= by1)
			{
				continue;
			}

			draw_sprites.push_back(sp);
		}
	}


	/* sort sprites into drawing order, then draw */

	std::sort(draw_sprites.begin(), draw_sprites.end(), sprite_TILEY_CMP());

	for (k = 0 ; k < draw_sprites.size() ; k++)
	{
		RE_Sprite *sp = draw_sprites[k];

		int flags = sp->mirror ? DIF_MIRROR : 0;

		if (main_win->action == ACT_Highlight && sp == main_win->act_sprite)
			flags |= DIF_BRIGHT;

		R_DrawImage(sp->img, sp->sx, sp->sy, sp->sw, sp->sh, flags);
	}

	draw_sprites.clear();
}



//------------------------------------------------------------------------

RE_Sprite * R_FindSpriteUnderCursor(int mx, int my)
{
	// NOTE : this uses the information from the last R_RenderSprites() call.
	//        FIXME : THIS WILL PROBABLY DO THE WRONG THING AT TIMES....

	for (unsigned int k = 0 ; k < all_sprites.size() ; k++)
	{
		RE_Sprite *sp = all_sprites[k];
		
		if (! sp->sw)  // it was not drawn
			continue;

		if (mx < sp->sx || mx >= sp->sx + sp->sw ||
			my < sp->sy || my >= sp->sy + sp->sh)
			continue;

//!!!! FIXME : provide a 'filter' flag
if (! sp->isDraggable()) continue;

		if (main_win->action == ACT_DragObject && sp == main_win->act_sprite)
			continue;

		return sp;
	}

	return NULL;  // not found
}


void R_RenderDragSprite(RE_Sprite *sp, int mx, int my)
{
	sp->ProjectDrag(mx, my);

	if (! sp->sw)
		return;  // too small

	int flags = sp->mirror ? DIF_MIRROR : 0;

	R_DrawImage(sp->img, sp->sx, sp->sy, sp->sw, sp->sh, flags);
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
