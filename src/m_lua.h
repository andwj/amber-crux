//----------------------------------------------------------------------
//  LUA interface
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __SCRIPTING_HEADER__
#define __SCRIPTING_HEADER__

typedef struct lua_State lua_State;

void Script_Open();
void Script_Close();

bool Script_RunString(const char *str, ...);

// Wrappers which call Lua functions

void amb_init(const char *mode, int seed, const char *char_info = "");
void amb_user_action(const char *source, const char *act, const char *param1 = NULL);

void amb_set_preference(const char *key, const char *value);
void amb_read_preferences(std::vector<std::string> * lines);

#endif /* __SCRIPTING_HEADER__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
