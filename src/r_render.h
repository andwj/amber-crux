//----------------------------------------------------------------------
//  RENDERING LOGIC
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __R_RENDERING_H__
#define __R_RENDERING_H__

#include "aplib/image.h"

// cache an image.  this is different than R_LoadImage() since repeated
// calls with the same name return the exact same image object.
// NOTE WELL: never use R_FreeImage() on result, use R_UncacheImage() instead.
image_c * R_CacheImage(const char *name, bool required = true);

void R_UncacheImage(image_c * img);


// draw flags
#define DIF_MIRROR		(1 << 0)
#define DIF_BRIGHT		(1 << 1)

// draw the given image, where 'sx' and 'sy' are the top-left corner
// and 'sw' and 'sh' specify the final size of the image, scaling
// and clipping the image as necessary.
void R_DrawImage(image_c *img, int sx, int sy, int sw, int sh, int flags = 0);


#endif /* __R_RENDERING_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
