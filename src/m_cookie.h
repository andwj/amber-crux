//----------------------------------------------------------------------
//  COOKIE : Save/Load user preferences
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __M_COOKIE_H__
#define __M_COOKIE_H__

bool Cookie_Load(const char *filename);
bool Cookie_Save(const char *filename);

void Cookie_ParseArguments(void);

// this handles engine-side preferences
void Cookie_ChangePreference(const char *name, const char *value);


// preference vars

extern bool pref_panel_right;
extern bool pref_text_top;


#endif /* __M_COOKIE_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
