//----------------------------------------------------------------------
//  PLAYER STATS
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "main.h"

#include "r_render.h"
#include "r_sprite.h"  // DRAGTO_ constants


UI_Stats::UI_Stats(int X, int Y, int W, int H) :
    Fl_Group(X, Y, W, H, NULL),
	health(-1), mana(-1), armor(-1), gold(-1),
	icon_bg(NULL), icon_bg_dest(NULL),
	weapon_icon(NULL), hold_icon(NULL), spell_icon(NULL)
{
	end();

	resizable(NULL);

	CalcFontHeight(W, H);
}


UI_Stats::~UI_Stats()
{ }


void UI_Stats::Prepare()
{
	icon_bg      = R_CacheImage("ui/icon_bg");
	icon_bg_dest = R_CacheImage("ui/icon_bg_dest");
}


void UI_Stats::resize(int X, int Y, int W, int H)
{
	CalcFontHeight(W, H);

	Fl_Group::resize(X, Y, W, H);
}


void UI_Stats::draw()
{
	Fl_Group::draw();

	fl_font(FL_COURIER, font_h);

	int cx = x() + 7;
	int cy = y() + 4 + font_h + font_h / 4;


	/* Player stats */

	DrawStat("Health: ", cx, cy, health, FL_RED);
	cy += font_h *  3 / 2;

	DrawStat("Mana: ", cx, cy, mana, FL_CYAN);
	cy += font_h *  3 / 2;

	DrawStat("Armor: ", cx, cy, armor, FL_GREEN, "+");
	cy += font_h *  3 / 2;

	DrawStat("Gold: ", cx, cy, gold, FL_YELLOW, "$");
	cy += font_h *  3 / 2;


	/* Active items */

	cy += font_h;

	fl_color(FL_WHITE);

	fl_draw("Weapon:", cx, cy);
	fl_draw("Hold:", x() + w()/2 + font_h, cy);
	cy += font_h / 2;

	// compute size of icons
	int icon_w = w() * 6 / 16;
	int icon_h = (y() + h() - 8 - cy) / 2;

	icon_w = MIN(icon_w, icon_h);

	int gap = (w() - icon_w*2) / 3;

	int pic_x1 = x() + gap;
	int pic_x2 = x() + w() - gap - icon_w;

	int dy = cy + icon_w + font_h / 2 + 2;

	fl_draw("Spell:", cx + font_h, dy + icon_w - font_h*3/2);


	/* Weapon slot */

	if (icon_bg_dest && main_win->action == ACT_DragObject && main_win->act_target == DRAGTO_Weapon)
	{
		R_DrawImage(icon_bg_dest, pic_x1, cy, icon_w, icon_w);
	}
	else
	{
		if (icon_bg)     R_DrawImage(icon_bg, pic_x1, cy, icon_w, icon_w);
		if (weapon_icon) R_DrawImage(weapon_icon, pic_x1, cy, icon_w, icon_w);
	}

	/* Hold slot */

	if (icon_bg_dest && main_win->action == ACT_DragObject && main_win->act_target == DRAGTO_Hold)
	{
		R_DrawImage(icon_bg_dest, pic_x2, cy, icon_w, icon_w);
	}
	else
	{
		if (icon_bg)   R_DrawImage(icon_bg, pic_x2, cy, icon_w, icon_w);
		if (hold_icon) R_DrawImage(hold_icon, pic_x2, cy, icon_w, icon_w);
	}

	/* Spell slot */

	if (icon_bg_dest && main_win->action == ACT_DragObject && main_win->act_target == DRAGTO_Spell)
	{
		R_DrawImage(icon_bg_dest, pic_x2, dy, icon_w, icon_w);
	}
	else
	{
		if (icon_bg)    R_DrawImage(icon_bg, pic_x2, dy, icon_w, icon_w);
		if (spell_icon) R_DrawImage(spell_icon, pic_x2, dy, icon_w, icon_w);
	}
}


void UI_Stats::ResetEverything()
{
	health = -1;
	mana   = -1;
	armor  = -1;
	gold   = -1;

	setWeaponIcon(NULL);
	setHoldIcon(NULL);
	setSpellIcon(NULL);
}


void UI_Stats::CalcFontHeight(int W, int H)
{
	font_h = FontSizeForArea(W, H, 12, 10);
}


void UI_Stats::DrawStat(const char *name, int cx, int cy, 
			            int value, Fl_Color color,
						const char *prefix)
{
	// draw the name

	int name_w = 0;
	int name_h = 0;

	fl_measure(name, name_w, name_h);

	fl_color(FL_WHITE);
	fl_draw(name, cx, cy);

	/// cx += name_w;
	cx = x() + w() / 2;


	// draw the value

	if (value < 0)
		return;

	char buffer[100];

	if (prefix)
		sprintf(buffer, "%s%d", prefix, value);
	else
		sprintf(buffer, "%d", value);

	fl_color(color);
	fl_draw(buffer, cx, cy);
}


void UI_Stats::Update()
{
	// the stats window is part of UI_Panel (not really a separate widget)

	main_win->panel->redraw();
}


void UI_Stats::setWeaponIcon(const char *name)
{
	if (weapon_icon)
		R_UncacheImage(weapon_icon);
	
	if (name)
		weapon_icon = R_CacheImage(name);
	else
		weapon_icon = NULL;
	
	Update();
}

void UI_Stats::setHoldIcon(const char *name)
{
	if (hold_icon)
		R_UncacheImage(hold_icon);
	
	if (name)
		hold_icon = R_CacheImage(name);
	else
		hold_icon = NULL;
	
	Update();
}

void UI_Stats::setSpellIcon(const char *name)
{
	if (spell_icon)
		R_UncacheImage(spell_icon);
	
	if (name)
		spell_icon = R_CacheImage(name);
	else
		spell_icon = NULL;
	
	Update();
}


int UI_Stats::MouseOverSlot(int mouse_x, int mouse_y)
{
	// this matches code in draw() method

	int cy = y() + 4 + font_h + font_h / 4;

	cy += font_h * 3 / 2;
	cy += font_h * 3 / 2;
	cy += font_h * 3 / 2;
	cy += font_h * 3 / 2;

	cy += font_h;
	cy += font_h / 2;

	int icon_w = w() * 6 / 16;
	int icon_h = (y() + h() - 8 - cy) / 2;

	icon_w = MIN(icon_w, icon_h);

	int gap = (w() - icon_w*2) / 3;

	int pic_x1 = x() + gap;
	int pic_x2 = x() + w() - gap - icon_w;

	int dy = cy + icon_w + font_h / 2 + 2;

	if (mouse_x >= pic_x1 && mouse_x <= pic_x1 + icon_w &&
		mouse_y >= cy     && mouse_y <= cy + icon_w)
		return DRAGTO_Weapon;

	if (mouse_x >= pic_x2 && mouse_x <= pic_x2 + icon_w &&
		mouse_y >= cy     && mouse_y <= cy + icon_w)
		return DRAGTO_Hold;	

	if (mouse_x >= pic_x2 && mouse_x <= pic_x2 + icon_w &&
		mouse_y >= dy     && mouse_y <= dy + icon_w)
		return DRAGTO_Spell;	
	
	return 0;
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
