//----------------------------------------------------------------------
//  SCREEN WIDGET (for Help, Options, etc)
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __UI_SCREEN_H__
#define __UI_SCREEN_H__


typedef enum
{
	ELEM_INVALID = 0,

	ELEM_Text,
	ELEM_Image,

	// the rest are controls (interact with the user)
	ELEM_CheckButton,
	ELEM_WordSel,
	ELEM_Slider

} screen_element_kind_e;

#define ELEM_FIRST_CONTROL  ELEM_CheckButton


class Screen_element_c
{
public:
	screen_element_kind_e  kind;

	double x, y;
	double w, h;
	double scale;

	const char * text;
	image_c    * image;

	Fl_Align align;
	Fl_Color color;

	bool visible;
	bool mirror;

	double val, maxval;

	char opt_name[64];

public:
	 Screen_element_c();
	~Screen_element_c();

	void clearText();
	void   setText(const char *new_text);

	void setOptName(const char *new_name);
	void setImage(const char *filename);

	int countKeys() const;

	const char * currentKeyText() const;
};


//----------------------------------------------------------------------

class UI_Screen : public Fl_Widget
{
public:
	// virtual size
	int virt_W, virt_H;

	// these are for determining font sizes
	int char_W, char_H;

	std::vector< Screen_element_c * > elements;

	Screen_element_c * drag_elem;

private:
	/* rendering state */
	int rx, ry, rw, rh;
	double r_scale;

	double WINDOW_X(double x) const
	{
		return rx + x * r_scale;
	}
	double WINDOW_Y(double y) const
	{
		return ry + y * r_scale;
	}

public:
	UI_Screen(int X, int Y, int W, int H);

	~UI_Screen();

	void ResetEverything();

	void Begin(int W, int H, int ch_W, int ch_H);

	int CreateElement();

	Screen_element_c * FindElement(int id);

	/* FLTK functions */

	void draw();
///	void resize(int X, int Y, int W, int H);
	int handle(int event);


private:
	void Clear();

	void CalcTransform(int sx, int sy, int sw, int sh);

	void drawText(Screen_element_c * E);
	void drawImage(Screen_element_c * E);
	void drawCheckButton(Screen_element_c * E);
	void drawSlider(Screen_element_c * E);
	void drawWordSel(Screen_element_c * E);

	void drawElement(Screen_element_c * E);

	bool touchesElement(Screen_element_c *E, int mx, int my);

	void toggleCheckButton(Screen_element_c *E);
	void toggleWordSel(Screen_element_c *E);

	int handle_drag();
	int handle_push();
};


#endif  /* __UI_SCREEN_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
