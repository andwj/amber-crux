//----------------------------------------------------------------------
//  MAP AREA WIDGET
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __UI_MAP_H__
#define __UI_MAP_H__

struct tile_border_t
{
	// color for a blocking line, 0 = none
	Fl_Color line;

	// color for an out-pointing arrow, 0 = none
	Fl_Color arrow;
};


#define TILE_LAYERS		10
#define TILE_LAYER_BG	0

class Tile_c
{
public:
	// tile coords (starting at 1, Lua convention)
	int tx, ty;

	// screen coordinates
	int sx, sy, sw, sh;

	// tile should be drawn
	bool visible;

	// layer 0 is the solid background (usually present)
	// layer 2/4/6/8 are edge overlays (optional)
	// layer 1/3/7/9 are corner overlays
	// layer 5 is an overlay for the middle
	image_c * background[TILE_LAYERS];

	// index by (dir - 2) / 2
	// i.e. 0 = south, 1 = west, 2 = east, 3 = north
	tile_border_t border[4];

public:
	 Tile_c(int TX, int TY);
	~Tile_c();

public:
	void setBackground(int layer, const char *filename);

	void setBorder(int dir, Fl_Color color);

	void draw();

private:
	void drawArrow(int b);
	void drawBorder(int b);
};


//----------------------------------------------------------------------

#define MAX_WORLD_W  32
#define MAX_WORLD_H  32

class UI_Map : public Fl_Widget
{
private:
	Tile_c * tiles[MAX_WORLD_W][MAX_WORLD_H];

	// position of map -- coordinate for centre of the canvas.
	// these coordinates are in tiles (starting at 1) and the
	// fractional part is within a tile.
	double mid_x, mid_y;

	// scale for drawing map -- pixel size of each tile (W x H).
	double scale;

	// current padding size (pixels between tiles)
	int pad;

	// limits on the positioning
	double min_mid_x, min_mid_y;
	double max_mid_x, max_mid_y;

	double min_scale, max_scale;

	image_c * home_square;

public:
	UI_Map(int X, int Y, int W, int H);
	virtual ~UI_Map();

	/* FLTK methods */
	void resize(int X, int Y, int W, int H);

	void draw();

	int handle(int event);

public:
	inline double getScale() const
	{
		return scale;
	}

	// populate this map with tiles, using 'world_w/h' globals for
	// the size (which will never change over the course of a game).
	void Populate();

	// remove all the tiles.
	void Unpopulate();

	void ResetEverything();

	// access a tile (coordinates start at 1, Lua convention)
	Tile_c * getTile(int tx, int ty);

	// scroll the map.  The delta values are a fraction of the
	// full width (or height) of the map widget.  Negative values
	// scroll right/down (i.e. move the focus left/up), and vice
	// versa for positive values.
	void Scroll(double dx, double dy);

	// like Scroll() but 'dx' and 'dy' values are in screen coordinates
	// and the effect is like the user is dragging the map around.
	void Drag(int sdx, int sdy, double speed = 1);

	// move directly to a particular tile position and a particular
	// scale.  If any value is negative, that component will not be
	// modified.
	void MoveTo(double new_x = -1, double new_y = -1, double new_scale = -1);

	// zoom the map in or out.  Values > 1.0 will zoom in (increase the
	// visible size of the tiles) whereas values < 1.0 zoom out.
	//
	// The mouse_x/y parameters are the on-screen point to zoom around,
	// but if negative then the zoom is centred on middle of map view.
	void Zoom(double factor, int mouse_x = -1, int mouse_y = -1);

	// similar to Zoom() but this BLAH FUCK FUCK
	void ZoomWithMouse(int delta_y, double orig_scale, int mouse_x, int mouse_y);

	// set the position and zoom so that the specified tile becomes
	// visible and fairly large.
	void ZoomIn(int tile_x, int tile_y);

	void GoHome();

	// centre the map and zoom out far enough that all tiles and the
	// border of the map are visible.
	void ZoomOut();

	// set the home tile and scroll the map (if necessary) to ensure
	// that the tile is visible.
	void ChangeHome(int new_home_x, int new_home_y);

	// convert a tile position to a window coordinate
	int WINDOW_X(double tile_x) const;
	int WINDOW_Y(double tile_y) const;

	// convert a window coordinate to a tile position
	// (the result is not guaranteed to be valid)
	double TILE_X(double sx) const;
	double TILE_Y(double sy) const;

private:
	void ComputePad();

	void ComputeLimits();
	void ApplyLimits();

	void RepositionTiles();

	void DrawTiles();
	void DrawBorder();
	void DrawHomeSquare();

	// values are all zero if no tiles are visible
	void SeenTileRange(int *x1, int *y1, int *x2, int *y2);
};


extern int world_w;
extern int world_h;

extern int home_tile_x;  // 1 .. world_w
extern int home_tile_y;  // 1 .. world_h


#endif  /* __UI_MAP_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
