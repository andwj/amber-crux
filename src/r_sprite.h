//----------------------------------------------------------------------
//  SPRITES (MONSTERS, ITEMS, DECOR etc)
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#ifndef __R_SPRITE_H__
#define __R_SPRITE_H__


#define DRAGTO_Weapon		(1 << 0)
#define DRAGTO_Hold			(1 << 1)
#define DRAGTO_Spell		(1 << 2)

#define DRAGTO_Get          (1 << 3)
#define DRAGTO_Drop         (1 << 4)
#define DRAGTO_Armor		(1 << 5)    // i.e. wearable


class RE_Sprite
{
public:
	// id number, for communicating with the Lua scripts
	int id;

	// whether sprite is visible or hidden
	bool visible;

	// map coordinates, in terms of tiles (starting at 1)
	// Note: tile_y is 1 at the *bottom* of the map
	double tile_x, tile_y;

	image_c * img;

	// point in image where image is anchored.
	// range is 0.0 - 1.0
	// defaults to (0.50 1.00).
	double origin_x, origin_y;

	// apparent size of the object, where 100 is the height of the
	// average humanoid (such as the player).
	double height;

	// flip the sprite horizontally?
	bool mirror;

	// only draw the sprite when the map scale is >= this
	double min_scale;

	// displayed name of the item / monsters / etc...  NULL for none.
	const char *name;

	// this is normally -1.
	// when >= 0 then this sprite is "parented" to the sprite with the given ID,
	// which means this sprite is only visible when the parent is visible, and
	// tile_x and tile_y are a position inside the parent rectangle.
	int parent_id;

	// height when sprite is parented to another
	// it is a percentage, 100 makes it same height as the parent
	double child_height;

	// places where a sprite can be dragged to (a bit mask)
	int drag_to;

	/* === RENDER INFO === */

	int sx, sy, sw, sh;

public:
	RE_Sprite();
	virtual ~RE_Sprite();

public:
	void SetImage(const char *filename);

	void SetName(const char *new_name);

	bool isDraggable() const { return drag_to != 0; }

	// compute on-screen coordinates
	void Project();
	void ProjectChild(const RE_Sprite *parent);

	void ProjectDrag(int mx, int my);

private:

};


// remove all existing sprites
void R_ClearSprites(void);

RE_Sprite * R_NewSprite(void);

RE_Sprite * R_FindSprite(int id);

// draw all the visible sprites
void R_RenderSprites(void);


// scan sprites to find one under the mouse cursor
RE_Sprite * R_FindSpriteUnderCursor(int mx, int my);

void R_RenderDragSprite(RE_Sprite *sp, int mx, int my);


#endif  /* __R_SPRITE_H__ */

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
