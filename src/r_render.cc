//----------------------------------------------------------------------
//  RENDERING LOGIC
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2014-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "aplib/image.h"
#include "aplib/util.h"

#include "main.h"
#include "r_render.h"


#define MAX_CACHED_IMAGES  2048

typedef struct
{
	image_c	* img;

	int users;

} cached_image_t;

static cached_image_t all_cached_images[MAX_CACHED_IMAGES];



image_c * R_LoadImage(const char *name, bool required)
{
	const char *full_name = StringPrintf("%s/data/%s.tga", install_dir, name);

	image_c *img = IMG_LoadTGA(full_name);

	if (! img)
	{
		if (required)
		{
			Main_FatalError("Failed to load image: %s\n%s\n",
							name, IMG_Error());
		}

		// print a warning ??

		return NULL;
	}

	return img;
}


void R_FreeImage(image_c *img)
{
	delete img;
}


image_c * R_CacheImage(const char *name, bool required)
{
	cached_image_t *free_spot = NULL;

	for (int i = 0 ; i < MAX_CACHED_IMAGES ; i++)
	{
		image_c *img = all_cached_images[i].img;

		if (! img && ! free_spot)
			free_spot = &all_cached_images[i];

		if (img && strcmp(name, img->name) == 0)
		{
			all_cached_images[i].users += 1;
			return img;
		}
	}

	image_c *img = R_LoadImage(name, required);

	if (! img)
		return NULL;

	if (! free_spot)
		Main_FatalError("No free image slots.\n");

	img->SetName(name);

	free_spot->img = img;
	free_spot->users = 1;

	return img;
}


void R_UncacheImage(image_c * img)
{
	for (int i = 0 ; i < MAX_CACHED_IMAGES ; i++)
	{
		if (img == all_cached_images[i].img)
		{
			if (all_cached_images[i].users <= 0)
				Main_FatalError("Uncached image with 0 users.\n");

			all_cached_images[i].users -= 1;
			return;
		}
	}

	Main_FatalError("Uncached an unknown image.\n");
}


//
// Note: this is never used (e.g. by Main_Restart) since various UI
//       widgets hold on to references to images, and there is not a
//       huge benefit in freeing all the images (most will be used
//       again in a fresh game).
//
void R_ResetImageCache()
{
	for (int i = 0 ; i < MAX_CACHED_IMAGES ; i++)
	{
		if (all_cached_images[i].img)
			R_FreeImage(all_cached_images[i].img);

		all_cached_images[i].img = NULL;
		all_cached_images[i].users = 0;
	}
}


//----------------------------------------------------------------------

static int draw_image_flags;


static inline void Apply_Brighten(byte *rgb, int count)
{
	count = count * 3;

	for ( ; count > 0 ; count--)
	{
		const int val = (*rgb) + 32;

		*rgb++ = MIN(255, val);
	}
}


static inline void ApplyEffect(byte *rgb, int count)
{
	if (draw_image_flags & DIF_BRIGHT)
		Apply_Brighten(rgb, count);
}


#define MAX_SPAN_W  4096

static void DrawImageSpan(const byte *pix_line, opacity_e opac,
                          int ix, int idx, int cx, int cy, int cw)
{
	int x;
	int cx2 = cx + cw;

	static byte rgba[MAX_SPAN_W * 4];
	byte * dest = rgba;

	SYS_ASSERT(cw <= MAX_SPAN_W);

	if (opac == OPAC_Solid)
	{
		for (x = cx ; x < cx2 ; x++, ix += idx)
		{
			const byte *pix = pix_line + ((ix >> 14) & ~3);

			*dest++ = *pix++;
			*dest++ = *pix++;
			*dest++ = *pix  ;
		}

		ApplyEffect(rgba, cw);

		fl_draw_image(rgba, cx, cy, cw, 1, 3);
	}

/* NOTE: The following is what the code _SHOULD_ have been.
 *       However FLTK is horribly broken w.r.t. alpha blending via
 *       the fl_draw_image() function, especially under X11.
 */
 #if 0
 	else
	{
		for (x = cx ; x < cx2 ; x++, ix += idx)
		{
			const byte *pix = pix_line + ((ix >> 14) & ~3);

			*dest++ = *pix++;
			*dest++ = *pix++;
			*dest++ = *pix++;
			*dest++ = *pix  ;
		}

		fl_draw_image(rgba, cx, cy, cw, 1, 4);
	}
 #endif

	/* This treats OPAC_Complex the same as OPAC_Masked, i.e. does not
	 * do any blending between source image and the framebuffer.
	 * Real alpha blending will require using fl_read_image(), or for
	 * the FLTK developers to fix their shit.
	 */
	else
	{
		int start_x;

		for (x = cx ; x < cx2 ; )
		{
			const byte *pix = pix_line + ((ix >> 14) & ~3);

			// skip transparent parts

			if (! (pix[3] & 128))
			{
				x++;
				ix += idx;
				continue;
			}

			// collect a span of solid pixels

			start_x = x;
			dest = rgba;

			*dest++ = *pix++;
			*dest++ = *pix++;
			*dest++ = *pix  ;

			x++;
			ix += idx;

			for ( ; x < cx2 ; x++, ix += idx)
			{
				pix = pix_line + ((ix >> 14) & ~3);

				if (! (pix[3] & 128))
					break;

				*dest++ = *pix++;
				*dest++ = *pix++;
				*dest++ = *pix  ;
			}

			ApplyEffect(rgba, x - start_x);

			fl_draw_image(rgba, start_x, cy, (x - start_x), 1, 3);
		}
	}
}


void R_DrawImage(image_c *img, int sx, int sy, int sw, int sh, int flags)
{
	if (sw < 2 || sh < 2)
		return;
	
	// pre-clip : workaround for 16-bit limitations of fl_clip_box()
	int tx = sx, ty = sy;
	int tw = sw, th = sh;

	if (tx < 0) { tw += tx; tx = 0; }
	if (ty < 0) { th += ty; ty = 0; }

	if (tx + tw > 32767) tw = 32767 - tx;
	if (ty + th > 32767) th = 32767 - ty;

	if (tw <= 0 || th <= 0)
		return;

	// clip destination
	int cx, cy, cw, ch;

	fl_clip_box(/* in: */ tx, ty, tw, th, /* out: */ cx, cy, cw, ch);

	// outside clip region?
	if (cw <= 0 || ch <= 0)
		return;
	
	int cx2 = cx + cw;
	int cy2 = cy + ch;


#if 0  // DEBUG
fprintf(stderr, "cx - sx:%d  width:%d  sw:%d\n", cx-sx,  img->width,  sw);
#endif

	// compute fractional coords, using 16.16 fixed point
	int ix1 = (double)(cx  - sx) * img->width * 65536.0 / (double)sw;
	int ix2 = (double)(cx2 - sx) * img->width * 65536.0 / (double)sw;

	int idx = (ix2 - ix1) / cw;
	if (idx < 0) idx = 0;

#if 0  // DEBUG
fprintf(stderr, "ix = %08x .. %08x  idx = %08x  last = %08x  < width %04x\n",
        ix1, ix2, idx, ix1 + (cw-1) * idx, img->width);
#endif

	SYS_ASSERT( ((ix1 + (cw-1) * idx) >> 16) < img->width);


	draw_image_flags = flags;

	if (flags & DIF_MIRROR)
	{
		ix1 = ((img->width << 16) - 1) - ix1;
		idx = -idx;
	}


	int y;

	for (y = cy ; y < cy2 ; y++)
	{
		int iy = (y - sy) * img->height / sh;

		const byte *pix_line = img->PixelAt(0, iy);

		DrawImageSpan(pix_line, img->opacity, ix1, idx, cx, y, cw);
	}
}

//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
