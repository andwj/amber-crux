//----------------------------------------------------------------------
//  MAIN PROGRAM
//----------------------------------------------------------------------
//
//  Amber Crux : a graphical adventure game
//
//  Copyright (C) 2010-2016  Andrew Apted
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this software.  If not, please visit the following
//  web page: http://www.gnu.org/licenses/gpl.html
//
//----------------------------------------------------------------------

#include "headers.h"
#include "hdr_lua.h"

#include "aplib/argv.h"
#include "aplib/file.h"
#include "aplib/util.h"

#include "main.h"
#include "m_cookie.h"
#include "m_lua.h"

#include "r_render.h"
#include "r_sprite.h"


#ifndef WIN32
#include <unistd.h>
#endif


#define TICKER_TIME  50 /* ms */


const char *home_dir    = NULL;
const char *install_dir = NULL;

const char *prefs_file  = NULL;


bool debug_messages = false;

int next_rand_seed;

const char *next_char_info;


game_state_type_e game_state;

quit_type_e want_quit;


// this is non-NULL while the lock file exists
static const char *lock_file;

#define LOCK_FILE_NAME  "is_running"



/* ----- user information ----------------------------- */

static void ShowTitle()
{
	BigPrintf("\n");
	BigPrintf("*************************\n");
	BigPrintf("**   " AMBER_TITLE " " AMBER_VERSION "   **\n");
	BigPrintf("*************************\n");
	BigPrintf("\n");
}


static void ShowInfo()
{
	ShowTitle();

	printf(
		"USAGE: ambercrux [options...]\n"
		"\n"
		"Available options:\n"
		"     --home     <dir>    Home directory\n"
		"     --install  <dir>    Installation directory\n"
		"  -v --verbose           Enable log messages on stdout\n"
		"  -d --debug             Enable debugging messages\n"
		"  -h --help              Show this help\n"
		"\n"
	);

	printf(
		"Amber Crux is Copyright (C) 2014-2016 Andrew Apted, et al\n"
		"\n"
		"This program is free software, under the terms of the GNU\n"
		"General Public License, and comes with ABSOLUTELY NO WARRANTY.\n"
		"See the documentation for more details: http://ambercrux.sf.net/\n"
		"\n"
	);
}


void Determine_HomeDir(const char *argv0)
{
	// Find the home directory : the place where the preferences (PREFS.txt) files
	// is stored, among other things.  Remember it in the 'home_dir' global var.

	int home_arg = ArgvFind(0, "home");

	if (home_arg >= 0)
	{
		if (home_arg+1 >= arg_count || ArgvIsOption(home_arg+1))
		{
			fprintf(stderr, "ARG ERROR: missing path for --home\n");
			exit(9);
		}

		home_dir = StringDup(arg_list[home_arg + 1]);
		return;
	}

#ifdef WIN32
	home_dir = GetExecutablePath(argv0);

#else
	char *path = StringNew(FL_PATH_MAX + 4);

	if (fl_filename_expand(path, "$HOME/.ambercrux") == 0)
		Main_FatalError("Unable to find $HOME directory!\n");

	home_dir = path;

	// try to create it (doesn't matter if it already exists)
	if (home_dir)
		FileMakeDir(home_dir);
#endif

	if (! home_dir)
		home_dir = StringDup(".");
}


static bool Verify_InstallDir(const char *path)
{
	const char *filename = StringPrintf("%s/game/amber.lua", path);

#if 0  // DEBUG
	fprintf(stderr, "Trying install dir: [%s]\n", path);
	fprintf(stderr, "  using file: [%s]\n\n", filename);
#endif

	bool exists = FileExists(filename);

	StringFree(filename);

	return exists;
}


void Determine_InstallDir(const char *argv0)
{
	// Find the "Install directory" where all the data files (scripts, graphics, etc)
	// are stored.  Remember it in the global variable 'install_dir'.

	int inst_arg = ArgvFind(0, "install");

	if (inst_arg >= 0)
	{
		if (inst_arg+1 >= arg_count || ArgvIsOption(inst_arg+1))
		{
			fprintf(stderr, "ARG ERROR: missing path for --install\n");
			exit(9);
		}

		install_dir = StringDup(arg_list[inst_arg + 1]);

		if (Verify_InstallDir(install_dir))
			return;

		Main_FatalError("Bad install directory specified!\n");
	}

	// if run from current directory, look there
	if (argv0[0] == '.' && Verify_InstallDir("."))
	{
		install_dir = StringDup(".");
		return;
	}

#ifdef WIN32
	install_dir = StringDup(home_dir);

#else
	static const char *prefixes[] =
	{
		"/usr/local", "/usr", "/opt",
		
		NULL  // end of list
	};

	for (int i = 0 ; prefixes[i] ; i++)
	{
		install_dir = StringPrintf("%s/share/ambercrux", prefixes[i]);

		if (Verify_InstallDir(install_dir))
			return;

		StringFree(install_dir);
		install_dir = NULL;
	}
#endif

	if (! install_dir)
		Main_FatalError("Unable to find the install directory!\n");
}


void Determine_PrefsFile(const char *argv0)
{
	int conf_arg = ArgvFind(0, "prefs");

	if (conf_arg >= 0)
	{
		if (conf_arg+1 >= arg_count || ArgvIsOption(conf_arg+1))
		{
			fprintf(stderr, "ARG ERROR: missing path for --prefs\n");
			exit(9);
		}

		prefs_file = StringDup(arg_list[conf_arg + 1]);
		return;
	}

	prefs_file = StringPrintf("%s/%s", home_dir, PREFS_FILENAME);
}


void Main_CalcRandSeed(bool check_args)
{
	if (check_args)
	{
		int seed_arg = ArgvFind(0, "seed");

		if (seed_arg >= 0)
		{
			if (seed_arg+1 >= arg_count || ArgvIsOption(seed_arg+1))
				Main_FatalError("missing value for --seed\n");

			next_rand_seed = atoi(arg_list[seed_arg + 1]);

			// prevent negative values
			if (next_rand_seed < 0)
				next_rand_seed = -next_rand_seed;

			return;
		}
	}

	next_rand_seed = (int)time(NULL);
}


bool Main_BackupFile(const char *filename, const char *ext)
{
	if (FileExists(filename))
	{
		char *backup_name = ReplaceExtension(filename, ext);

		LogPrintf("Backing up existing file to: %s\n", backup_name);

		FileDelete(backup_name);

		if (! FileRename(filename, backup_name))
		{
			LogPrintf("WARNING: unable to rename file!\n");
			StringFree(backup_name);
			return false;
		}

		StringFree(backup_name);
	}

	return true;
}


/* this is only to prevent ESCAPE key from quitting */
int Main_key_handler(int event)
{
	if (event != FL_SHORTCUT)
		return 0;

	if (Fl::event_key() == FL_Escape)
		return 1;  // eat it

	return 0;
}


void Main_InitFLTK()
{
	Fl::visual(FL_DOUBLE | FL_RGB);

	if (true)
	{
//??		Fl::background(236, 232, 228);
//??		Fl::background2(255, 255, 255);
//??		Fl::foreground(0, 0, 0);

		Fl::scheme("plastic");
	}

	screen_w = Fl::w();
	screen_h = Fl::h();

#if 0  // debug
	fprintf(stderr, "Screen dimensions = %dx%d\n", screen_w, screen_h);
#endif


	// default font size for widgets
	FL_NORMAL_SIZE = 14;

	fl_message_font(FL_HELVETICA /* _BOLD */, 18);
}


void Main_OpenWindow()
{
	BigPrintf("Opening main window....\n");
	LogPrintf("\n");

	// load icons for file chooser
#ifndef WIN32
	Fl_File_Icon::load_system_icons();
#endif

	Fl::add_handler(Main_key_handler);


	int main_w, main_h;

	UI_Window::InitialSize(&main_w, &main_h);

	main_win = new UI_Window(main_w, main_h, AMBER_TITLE " " AMBER_VERSION);


	// show window (pass some dummy arguments)
	{
		char *argv[2];
		argv[0] = strdup("AmberCrux.exe");
		argv[1] = NULL;

		main_win->show(1 /* argc */, argv);
	}

	// kill the bright background of the FLTK "plastic" scheme
	{
		delete Fl::scheme_bg_;
		Fl::scheme_bg_ = NULL;

		main_win->image(NULL);
	}

	main_win->panel->stat_box->Prepare();
}


void Main_Ticker()
{
	// This function is called very frequently.
	// To prevent a slow-down, we only call Fl::check()
	// after a certain time has elapsed.

	static u32_t last_millis = 0;

	u32_t cur_millis = TimeGetMillies();

	if ((cur_millis - last_millis) >= TICKER_TIME)
	{
		Fl::check();

		last_millis = cur_millis;
	}
}


void Main_Shutdown(bool error)
{
	static bool in_shutdown = false;

	if (! in_shutdown)
	{
		in_shutdown = true;

		Main_RemoveLockFile();

		if (main_win)
		{
			// on fatal error we cannot risk calling into the Lua runtime
			// (the state may be compromised by a script error).

			if (prefs_file && ! error)
				Cookie_Save(prefs_file);

			delete main_win;
			main_win = NULL;
		}

		Script_Close();
		LogClose();
		ArgvClose();
	}
}


void Main_FatalError(const char *msg, ...)
{
	static bool in_error = false;

	if (! in_error)
	{
		in_error = true;

		static char buffer[MSG_BUF_LEN];

		va_list arg_pt;

		va_start(arg_pt, msg);
		vsnprintf(buffer, MSG_BUF_LEN, msg, arg_pt);
		va_end(arg_pt);

		buffer[MSG_BUF_LEN-1] = 0;

		BigPrintf("\n%s\n\n", buffer);

		DLG_ShowError("%s", buffer);
	}

	Main_Shutdown(true /* error */);

	exit(9);
}


void Main_ConfirmQuit()
{
	if (game_state == GAME_Active)
	{
		int choice = DLG_Confirm("&Cancel|&Quit",
								 "Are you sure you want to discard the current game?");
		if (choice == 0)
		{
			want_quit = QUIT_NO;
			return;
		}
	}

	want_quit = QUIT_Immediately;
	BigPrintf("User quit.\n");
}


void Main_ConfirmDiscard()
{
	if (game_state == GAME_Active)
	{
		int choice = DLG_Confirm("&Cancel|&Restart|&Quit",
								 "Are you sure you want to discard the current game?");
		if (choice == 0)
		{
			want_quit = QUIT_NO;
			return;
		}

		if (choice == 2)
		{
			want_quit = QUIT_Immediately;
			BigPrintf("User discarded game.\n");
			return;
		}
	}

	want_quit = RESTART_Menu;
	BigPrintf("User discarded game.\n");
}


void Main_RemoveLockFile()
{
	if (lock_file)
	{
		FileDelete(lock_file);

		StringFree(lock_file);
		lock_file = NULL;
	}
}


void Main_CreateLockFile()
{
	lock_file = StringPrintf("%s/%s", home_dir, LOCK_FILE_NAME);

	FILE *fp = fl_fopen(lock_file, "r");
	if (fp)
	{
		fclose(fp);

		// make sure to NOT delete the existing file
		const char *old_lock_file = lock_file;
		lock_file = NULL;

		// FIXME : improve this message
		Main_FatalError("Amber Crux is already running!\n\n"
						"( if you are sure it is not running, then the previous "
						"game must have crashed.  You can fix this by deleting "
						"this file: %s )\n", old_lock_file);
		/* NOT REACHED */
	}

	fp = fl_fopen(lock_file, "w");
	if (! fp)
	{
		Main_FatalError("Unable to create lock file: %s\n", strerror(errno));
		/* NOT REACHED */
	}

#ifdef WIN32
	fprintf(fp, "locked\n");
#else
	fprintf(fp, "locked by process %ld\n", (long)getpid());
#endif

	fclose(fp);
}


void Main_Restart()
{
	bool new_game    = (want_quit == RESTART_Fresh);
	bool replay_game = (want_quit == RESTART_Replay);

	want_quit  = QUIT_NO;
	game_state = GAME_NotStarted;

	Cookie_Save(prefs_file);

	main_win->ResetEverything();

	R_ClearSprites();

	// Note: we DON'T clear the image cache
	// [ various widgets still have references ]

	// kill existing Lua VM and create a fresh one
	Script_Close();
	Script_Open();

	Cookie_Load(prefs_file);
	Cookie_ParseArguments();

	if (replay_game)
	{
		if (!next_char_info || !next_char_info[0])
			Main_FatalError("script problem, failed to set char_info.\n");

		amb_init("replay", next_rand_seed, next_char_info);
		return;
	}

	Main_CalcRandSeed(false /* check_args */);

	amb_init(new_game ? "newgame" : "start", next_rand_seed);
}


void Main_SpecialAction(const char *act, const char *param)
{
	if (strcmp(act, "!quit") == 0)
		want_quit = CONFIRM_Quit;
	else if (strcmp(act, "!discard") == 0)
		want_quit = CONFIRM_Discard;

	else if (strcmp(act, "!restart") == 0)
		want_quit = RESTART_Menu;
	else if (strcmp(act, "!fresh") == 0)
		want_quit = RESTART_Fresh;
	else if (strcmp(act, "!replay") == 0)
		want_quit = RESTART_Replay;
	else
		LogPrintf("WARNING: unknown action '%s'\n", act);
}


/* ----- Main Program ----------------------------- */


int main(int argc, char **argv)
{
	// initialise argument parser (skipping program name)
	ArgvInit(argc-1, (const char **)(argv+1));

	if (ArgvFind('?', NULL) >= 0 ||
	    ArgvFind('h', "help") >= 0 ||
	    ArgvFind(0, "version") >= 0)
	{
		ShowInfo();
		exit(1);
	}


	// prepare for dialogs...
	Main_InitFLTK();


	Determine_HomeDir(argv[0]);
	Determine_InstallDir(argv[0]);
	Determine_PrefsFile(argv[0]);


	// check that we are only running one instance of the game at a time
	Main_CreateLockFile();


	LogInit(LOG_FILENAME);

	if (ArgvFind('v', "verbose") >= 0)
		LogEnableTerminal(true);

	if (ArgvFind('d', "debug") >= 0)
		debug_messages = true;

	LogEnableDebug(debug_messages);


	ShowTitle();

	LogPrintf("Library versions: FLTK %d.%d.%d\n\n",
			  FL_MAJOR_VERSION, FL_MINOR_VERSION, FL_PATCH_VERSION);

	LogPrintf("   home_dir: %s\n",   home_dir);
	LogPrintf("install_dir: %s\n",   install_dir);
	LogPrintf(" prefs_file: %s\n\n", prefs_file);


	BigPrintf("Loading game scripts...\n");

	Script_Open();


	// read prefs after loading scripts but before opening main window

	Cookie_Load(prefs_file);
	Cookie_ParseArguments();

	Main_OpenWindow();
	Main_CalcRandSeed(true /* check_args */);


	amb_init("start", next_rand_seed);


	try
	{
		// run the GUI until the user quits
		while (want_quit != QUIT_Immediately)
		{
			switch (want_quit)
			{
				case QUIT_NO:
					break;

				case QUIT_Immediately:
					// should not get here
					break;

				case CONFIRM_Quit:
					Main_ConfirmQuit();
					break;

				case CONFIRM_Discard:
					Main_ConfirmDiscard();
					break;

				case QUIT_Save:
					want_quit = QUIT_NO;
					amb_user_action("system", "save");
					break;

				case RESTART_Menu:
				case RESTART_Fresh:
				case RESTART_Replay:
					Main_Restart();
					break;
			}

			Fl::wait();
		}
	}
	catch (...)
	{
		Main_FatalError("An unknown problem occurred (an exception was thrown).\n");
	}


	Main_Shutdown(false);

	return 0;
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
