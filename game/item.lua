------------------------------------------------------------------------
--  MAGICAL ITEMS
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------


--ITEM CLASS =
--[[
    item : keyword   -- name of the item

???  owned : ACTOR

???  quantity

    worn :  boolean   -- true if worn by an actor

    sprite : TABLE
--]]


ITEM_INFO =
{
  -- TODO
}



function item_lookup(name)
  -- TODO
end


function item_create(info)
  -- TODO
end

