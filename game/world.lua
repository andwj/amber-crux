------------------------------------------------------------------------
--  WORLD GENERATION
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------


--MAP OBJECT =
--[[
    width, height     -- size of world (in tiles)

    tiles : ARRAY_2D  -- all the tiles : [1][1] is bottom left tile

    areas : LIST      -- all the areas
    conns : LIST      -- all the connections

    home_x, home_y    -- tile where player begins


    world_name        -- name of the world

    player : ACTOR    -- our hero

    all_mons : LIST   -- all monsters in the world

    battle : BATTLE   -- current battle, or NIL if none
--]]



--TILE CLASS =
--[[
    tx, ty  --  tile coordinates (1..width, 1..height)

    kind    --  general environment here (desert, forest, town, etc)

    area : AREA

    visible    --  true if player has been here

    background       -- background image for this tile
    overlay[DIR]     -- overlay (side) images for this tile

    no_walk[DIR]    -- which directions the player CANNOT walk (holds a message)
    can_see[DIR]    -- which neighbors are visible

    player_x, player_y  -- offset (0-1) where player stands, can be NIL

    monsters : LIST  -- monsters free roaming in this tile

---###  south_mons : LIST  -- monsters bordering on the south of this tile
---###  east_mons  : LIST  -- monsters bordering on the east of this tile

    decor : LIST   -- decorative objects
    items : LIST   -- all pick-up-able items in this tile

    spots : LIST   -- places where stuff can be placed


    **** stuff for world creation only ****

    path_info : table  -- set if this tile on a path between landmarks

    has_conn : boolean
--]]



--AREA CLASS =
--[[
    kind    -- same values as TILE.kind

    tiles : LIST  -- all tiles belonging to this area

    tx1, ty1, tx2, ty2   -- bounding box of all tiles in area

    conns : LIST

    zone : ZONE

    solution : LOCK   -- if present, this area contains a solution (e.g. a key)
--]]



--CONNECTION CLASS =
--[[
    T1 : TILE   -- source
    T2 : TILE   -- destination

    A1 : AREA   -- connected areas
    A2 : AREA   --

    dir         -- direction

    lock : LOCK    -- if present, this connection is locked
--]]



--LOCK CLASS =
--[[
    kind : keyword   -- "key" : an item is required to open a door

    item : name      -- what kind of key is required

    conn : CONNECTION  -- the connection which is locked
--]]


------------------------------------------------------------------------


function tile_is_valid(tx, ty)
  if tx < 1 or tx > MAP.width  then return false end
  if ty < 1 or ty > MAP.height then return false end

  return true
end


function tile_is_outdoors(T)
  return T.kind == "forest"   or T.kind == "desert" or
         T.kind == "mountain" or T.kind == "town" or
         T.kind == "vulcano"  or T.kind == "snow" or
         T.kind == "water"
end


function tile_neighbor(T, dir)
  local nx, ny = geom.nudge(T.tx, T.ty, dir)

  if tile_is_valid(nx, ny) then
    return MAP.tiles[nx][ny]
  end

  return nil
end


function tile_is_corner(T)
  return (T.tx == 1 or T.tx == MAP.width) and
         (T.ty == 1 or T.ty == MAP.height)
end


function tile_touches(T, kind)
  assert(kind)

  each dir in geom.ALL_DIRS do
    local N = tile_neighbor(T, dir)

    if N and N.kind == kind then
      return true
    end
  end

  return false
end


------------------------------------------------------------------------

CASTLE_SHAPES =
{
  { "....", ".#..", "....", "...." }
  { "....", ".##.", ".##.", "...." }
  { "##..", "#...", "##..", "...." }
  { "###.", "##..", "###.", "...." }
  { ".#..", "###.", ".#..", "...." }
  { "###.", ".#..", "###.", "...." }
}

TOWN_SHAPES =
{
  { ".##.", "####", ".##.", "...." }
  { "###.", "###.", "###.", "...." }
  { "##..", "##..", "####", "...." }
  { "##..", "####", "..##", "...." }
  { "....", ".##.", ".##.", "...." }

---??  { "##..", "###.", ".###", "..##" }
---??  { ".##.", "####", "####", ".##." }
}

VULCANO_SHAPES =
{
  { "###.", "###.", "###.", "...." }
}

STARTING_SHAPES =
{
  { "....", ".#..", "....", "...." }
}


SHAPE_TABLES =
{
  castle  = CASTLE_SHAPES
  town    = TOWN_SHAPES
  vulcano = VULCANO_SHAPES
  start   = STARTING_SHAPES
}


function W_AllTiles()
  local list = {}

  for tx = 1, MAP.width  do
  for ty = 1, MAP.height do
    table.insert(list, MAP.tiles[tx][ty])
  end
  end

  return list
end


function W_NewArea(kind)
  local AREA =
  {
    kind  = kind
    tiles = {}
    conns = {}
  }

  return AREA
end


function W_NewConn(T1, dir, T2)
  local CONN =
  {
    T1 = T1
    T2 = T2

    A1 = T1.area
    A2 = T2.area

    dir = dir
  }

  table.insert(MAP.conns, CONN)

  table.insert(T1.area.conns, CONN)
  table.insert(T2.area.conns, CONN)

  T1.has_conn = true
  T2.has_conn = true

  return CONN
end



function W_CenterOfArea(area)
  local mid_x = math.i_mid(area.tx1, area.tx2)
  local mid_y = math.i_mid(area.ty1, area.ty2)

  if MAP.tiles[mid_x][mid_y].area == area then
    return { x=mid_x, y=mid_y }
  end

  for loop = 1,999 do
    local dx = rand.index_by_probs({ 1, 20, 40, 20, 1 }) - 3
    local dy = rand.index_by_probs({ 1, 20, 40, 20, 1 }) - 3

    local tx = mid_x + dx
    local ty = mid_y + dy

    if not tile_is_valid(tx, ty) then
      continue
    end

    if MAP.tiles[tx][ty].area == area then
      return { x=tx, y=ty }
    end
  end
  
  -- only get here if there's a bug in above code
  error("Unable to find center of landmark")
end


function W_CreateTiles()
  MAP.tiles = table.array_2D(MAP.width, MAP.height)

  for tx = 1, MAP.width  do
  for ty = 1, MAP.height do
    local TILE =
    {
      tx = tx
      ty = ty

      visible = false
      can_see = {}
      no_walk = {}

      overlay  = {}
      decor    = {}
      monsters = {}
    }

    MAP.tiles[tx][ty] = TILE
  end
  end
end


function W_SetTile(tx, ty, area)
  local T = MAP.tiles[tx][ty]

  assert(T.kind == nil)

  T.kind = area.kind
  T.area = area

  table.insert(area.tiles, T)

  -- update bounding box
  area.tx1 = math.min(area.tx1 or  999, tx)
  area.ty1 = math.min(area.ty1 or  999, ty)

  area.tx2 = math.max(area.tx2 or -999, tx)
  area.ty2 = math.max(area.ty2 or -999, ty)
end


function W_Transform(rot, dx, dy)
  if bit.band(rot, 1) ~= 0 then dx = 3 - dx end
  if bit.band(rot, 2) ~= 0 then dy = 3 - dy end

  if bit.band(rot, 4) ~= 0 then return dy, dx end

  return dx, dy
end


function W_PlaceShape(base_x, base_y, shape, rot, area, TEST_ONLY)
  for dx = 0, 3 do
  for dy = 0, 3 do

    local line = shape[1 + dy]
    assert(line)

    local ch = string.sub(line, dx+1, dx+1)
    assert(ch ~= "")

    if ch == '#' then
      local ndx, ndy = W_Transform(rot, dx, dy)

      local tx = base_x + ndx
      local ty = base_y + ndy

      if tx < 1 or tx > MAP.width  then return false end
      if ty < 1 or ty > MAP.height then return false end

      if TEST_ONLY then
        local T = MAP.tiles[tx][ty]

        -- occupied ?
        if T.kind then return false end

        -- touching same?
        if tile_touches(T, area.kind) then return false end
      else
        W_SetTile(tx, ty, area)
      end
    end

  end -- dx, dy
  end

  return true  -- OK!
end


function W_FindPlaceForShape(mid_x, mid_y, shape, area)
  local ROTS = { 0,1,2,3, 4,5,6,7 }

debugf("placing... %s\n", area.kind)
  for dist = 0, 9 do
    for dx = -dist, dist do
    for dy = -dist, dist do

      local tx = mid_x - 1 + dx
      local ty = mid_y - 1 + dy

      rand.shuffle(ROTS)

      each rot in ROTS do
        if W_PlaceShape(tx, ty, shape, rot, area, "TEST") then
           W_PlaceShape(tx, ty, shape, rot, area)
           return true -- done
        end
      end

    end -- dx, dy
    end
  end -- dist

  debugf("Unable to place: " .. area.kind)
  return false
end


function W_CreateLandmark(gx, gy, kind)
  -- gx, gy are grid coordinates (1..3)

  local mid_x = math.floor((gx - 0.25) * MAP.width  / 3)
  local mid_y = math.floor((gy - 0.25) * MAP.height / 3)

  if gx < 2 then mid_x = mid_x - 1
  elseif gx > 2 then mid_x = mid_x + 1
  end

  if gy < 2 then mid_y = mid_y - 1
  elseif gy > 2 then mid_y = mid_y + 1
  end

  mid_x = mid_x + rand.irange(-2, 2)
  mid_y = mid_y + rand.irange(-2, 2)

  local shape_list = SHAPE_TABLES[kind]
  assert(shape_list)

  local shape = rand.pick(shape_list)

  local area = W_NewArea(kind)

  if W_FindPlaceForShape(mid_x, mid_y, shape, area) then
    table.insert(MAP.areas, area)

    return area
  
  else
    -- failure to place something is rare, but possible
    return nil
  end
end


function W_AddLandmarks()
  --
  -- Decides where the major landmarks will go,
  -- in particular: towns, castles, and the vulcano.
  --
  -- We use a 3x3 grid for the coarse placement.
  --
  local cols = { 1,2,3, 1,2,3 }
  local rows = { 1,2,3, 1,2,3 }

  rand.shuffle(rows)
  rand.shuffle(cols)

  local grid = table.array_2D(3, 3)

  local vulcano_gx, start_gx
  local vulcano_gy, start_gy


  local function place_landmark(what, x, y)
    -- if already used, randomly pick somewhere else
    while not x or grid[x][y] do
      x = rand.irange(1, 3)
      y = rand.irange(1, 3)
    end

    grid[x][y] = W_CreateLandmark(x, y, what)
  end


  local function starting_spot()
    -- player starts in a spot far away from the vulcano.
    -- the spot must be either free or a town.

    local spot_tab = {}

    for x = 1, 3 do
    for y = 1, 3 do
      local spot = (x*10 + y)

      local area = grid[x][y]
      local what = area and area.kind

      if what == "vulcano" or what == "castle" then
        continue
      end

      local dx = math.abs(x - vulcano_gx)
      local dy = math.abs(y - vulcano_gy)

      local prob

      if dx >= 2 or dy >= 2 then
        prob = 100
      elseif not (dx == 0 or dy == 0) then
        prob = 10
      else
        prob = 1
      end

      if what == "town" then prob = prob * 3 end

      spot_tab[spot] = prob
    end
    end

    local result = rand.key_by_probs(spot_tab)

    start_gx = int(result / 10)
    start_gy = int(result % 10)

    debugf("Starting grid spot : %d/%d\n", start_gx, start_gy)

    local area = grid[start_gx][start_gy]

    if area then
      local center = W_CenterOfArea(area)

      MAP.home_x = center.x
      MAP.home_y = center.y

      area.has_start = true

    else
      local area = W_CreateLandmark(start_gx, start_gy, "start")
      assert(area)

      MAP.home_x = area.tx1
      MAP.home_y = area.ty1
    end
  end


  local function char_for_landmark(x, y)
    local area = grid[x][y]

    if x == start_gx and y == start_gy then
      if area and area.kind == "town"  then return 'T' end
      return 'S'
    end

    if not area then return '.' end

    return string.sub(area.kind, 1, 1)
  end


  local function dump_grid()
    debugf("Landmarks:\n")

    for y = 3, 1, -1 do
      local line = "   "
      for x = 1, 3 do
        line = line .. char_for_landmark(x, y)
      end
      debugf("%s\n", line)
    end

    debugf("\n")
  end


  -- the vulcano is always placed first

  vulcano_gx = cols[1]
  vulcano_gy = rows[1]

  place_landmark("vulcano", vulcano_gx, vulcano_gy)

  -- decide number of towns and castles

  local total = rand.irange(3, 5)

  local num_castles = rand.sel(50, 2, 1)
  local num_towns   = total - num_castles

  if num_towns < 1 then num_towns = 1 end
  if num_towns > 4 then num_towns = 4 end

  -- add the towns and castles

  if num_castles >= 1 then
    place_landmark("castle", cols[2], rows[2])
  end

  if num_towns >= 1 then
    place_landmark("town", cols[3], rows[3])
  end

  for i = 2, num_castles do
    place_landmark("castle")
  end

  for i = 2, num_towns do
    place_landmark("town")
  end

  starting_spot()

  dump_grid()
end



function W_JoinLandmarks()
  --
  -- link all the landmarks together with a path.
  -- this is mainly so we know where we can place normal terrain
  -- (forests, deserts) and where to place dungeons and caves.
  --
  local id_number = 1


  local function collect_landmarks()
    local list = {}

    each area in MAP.areas do
      if true then  --???  if area.kind ~= "vulcano" then
        table.insert(list, area)

        area.center = W_CenterOfArea(area)
      end
    end

    rand.shuffle(list)
    
    return list
  end


  local function initial_costs()
    -- we want to avoid certain tiles, e.g. the ones next to a town
    -- or castle, and we also want to avoid the vulcano.

    local vulc_coord = W_CenterOfArea(MAP.areas[1])

    for tx = 1, MAP.width  do
    for ty = 1, MAP.height do
      local T = MAP.tiles[tx][ty]

      local cost = gui.random() ^ 3

      if tile_touches(T, "town") then
        cost = cost + 0.3
      end

      if tile_touches(T, "castle") then
        cost = cost + 0.9
      end

      -- vulcano cost
      local dist = MAP.width / 2 - geom.dist(tx, ty, vulc_coord.x, vulc_coord.y)

      if dist > 0 then
        cost = cost + (1.5 ^ dist - 1)
      end

      T.lm_cost = cost

    end -- tx, ty
    end
  end


  local function install_path(path, info)
    each P in path do
      local T = MAP.tiles[P.x][P.y]
      assert(T)

      if not T.kind then
        T.path_info = info
      end

      -- future paths should avoid this one
      T.lm_cost = (T.lm_cost or 0) + 1.0
    end
  end


  local function connect_two_landmarks(A1, A2)
    --
    -- invoke the A* path-finding algorithm
    --
    local sx = A1.center.x
    local sy = A1.center.y

    local ex = A2.center.x
    local ey = A2.center.y

    local W = MAP.width
    local H = MAP.height

    local info =
    {
      id = id_number
      area1 = A1
      area2 = A2
    }

    id_number = id_number + 1


    local function LM_scorer(x, y, dir, data)
      local T = MAP.tiles[x][y]
      assert(T)

      local N = tile_neighbor(T, dir)
      assert(N)

      -- never pass through another landmark, except for the ones we
      -- are currently trying to connect.
      if N.kind then
        if N.area == data.area1 then return 0 end
        if N.area == data.area2 then return 0 end

        return -1
      end

      -- use cost assigned to neighbor tile (if any)
      return N.lm_cost or 0
    end
    

    local path = a_star.find_path(sx, sy, ex, ey, W, H, LM_scorer, info)

    -- no worries if a path cannot be found [very unlikely though]
    if not path then
      debugf("No path between %s <--> %s\n", A1.kind, A2.kind)
      return
    end

    install_path(path, info)
  end

  
  ---| W_JoinLandmarks |---

  initial_costs()

  local list = collect_landmarks()

  for i = 2, #list do
    -- connect each new landmark with a previously handled one
    local k = rand.irange(1, i - 1)

    connect_two_landmarks(list[i], list[k])
  end
end


------------------------------------------------------------------------

function W_AddTerrain()
  --
  -- This will create the dungeons, caves, forests and deserts.
  --
  -- Algorithm : 
  --   1. create a 'temp_area' object for every free tile
  --   2. randomly merge these, maintaining size limits
  --   3. assign environment kind to final areas
  --

  local all_tiles = W_AllTiles()

  local temp_areas = {}

  local MAX_SIZE = 6


  local TERRAIN_PROBS =
  {
    empty = 15
    water = 35

    mountain = 20
    desert   = 45
    cave     = 55

    forest   = 75
    dungeon  = 150
  }


  local function create_temp_areas()
    each T in all_tiles do
      if not T.area then
        local A = { tiles={T} }
        table.insert(temp_areas, A)

        T.temp_area = A
      end
    end
  end


  local function convert_start_landmark()
    -- The "start" landmark, when present, is only a single tile in size.
    -- Hence we want it to grow by participating in the merging logic here,
    -- so convert it to a "temp area".

    -- find it
    local ST = MAP.tiles[MAP.home_x][MAP.home_y]

    assert(ST)
    assert(ST.area)

    local start = ST.area

    if start.kind ~= "start" then return end

    -- remove from list of real areas  [will be added back later]
    table.kill_elem(MAP.areas, start)

    table.insert(temp_areas, start)

    -- fix tile info
    each T in all_tiles do
      if T.area == start then
        T.area = nil
        T.temp_area = start

        -- must clear tile kind too
        T.kind = nil
      end
    end
  end


  local function merge_two_areas(A, B)
    -- B is merged into A

    table.kill_elem(temp_areas, B)

    table.append(A.tiles, B.tiles)
    B.tiles = nil

    each T in all_tiles do
      if T.temp_area == B then
         T.temp_area = A
      end
    end
  end


  local function try_merge_two_areas(A, B)
    -- cannot merge with ourself!
    if A == B then return false end

    -- result would be too big?
    if #A.tiles + #B.tiles > MAX_SIZE then return false end

    -- OK --

    merge_two_areas(A, B)

    return true
  end


  local function try_merge_with_a_neighbor(A)
    assert(A.tiles)

    each dir in rand.dir_list() do
      each T in A.tiles do
        local N = tile_neighbor(T, dir)
        local B = N and N.temp_area

        if B and try_merge_two_areas(A, B) then
          return true
        end
      end
    end

    return false
  end


  local function attempt_a_merge()
    rand.shuffle(temp_areas)

    each A in temp_areas do
      if try_merge_with_a_neighbor(A) then
        return -- success
      end
    end

--stderrf("attempt_a_merge : none possible\n")
  end


  local function merge_areas()
    -- how many merges we will attempt
    local steps = int(#temp_areas * 0.75)

--stderrf("num tiles %d --> %d steps\n", #temp_areas, steps)

    for i = 1, steps do
      attempt_a_merge()
    end

--stderrf("Final # areas --> %d\n", #temp_areas)
  end


  local function analyse_area(A)
    A.touch_kinds = {}

    each T in A.tiles do
      if T.path_info then
        A.has_path = true
      end

      if T == MAP.tiles[MAP.home_x][MAP.home_y] then
        A.has_start = true
      end

      if tile_is_corner(T) then 
        A.at_corner = true
      end

      for dir = 2,8,2 do
        local N = tile_neighbor(T, dir)

        if not N then
          A.at_edge = true
          continue
        end

        if N.kind then
          A.touch_kinds[N.kind] = true
        end
      end
    end
  end


  local function pick_terrain_for_area(A)
    local tab = table.copy(TERRAIN_PROBS)

    -- position adjustments --

    if A.at_corner then
      tab.empty    = tab.empty * 12
      tab.mountain = tab.mountain * 4

    elseif A.at_edge then
      tab.empty    = tab.empty * 4
      tab.mountain = tab.mountain * 2
    end

    -- content adjustments --

    if A.has_start or A.has_path then
      tab.empty   = nil
      tab.water   = nil
      tab.dungeon = nil
    end

    if A.has_start then
      tab.cave = tab.cave / 5
    end

    -- touching adjustments --

    if A.touch_kinds["vulcano"] then
      tab.water  = nil
      tab.snow   = nil
      tab.forest = nil

      if tab.desert then tab.desert = tab.desert * 5 end
    end

    -- dungeons should come off a town / castle / vulcano
    if not A.touch_kinds["town"] and
       not A.touch_kinds["castle"] and
       not A.touch_kinds["vulcano"]
    then
      if tab.dungeon then tab.dungeon = tab.dungeon / 4 end
    end

    return rand.key_by_probs(tab)
  end


  local function assign_terrain()
    -- some maps have snow instead of desert
    -- [ DISABLED for now..... ]
    if rand.odds(0) then
      MAP.has_snow = true

      TERRAIN_PROBS.snow = 60
      TERRAIN_PROBS.desert = nil
    end

    each A in temp_areas do
      analyse_area(A)

      A.kind = pick_terrain_for_area(A)
    end
  end


  local function convert_area(A)
    A.conns = {}

    -- this will be reconstitued by W_SetTile
    A.tiles = {}

    table.insert(MAP.areas, A)
  end


  local function convert_to_real_areas()
    each A in temp_areas do
      convert_area(A)
    end

    each T in all_tiles do
      if T.temp_area then
        -- this creates/updates the bounding box of the area
        W_SetTile(T.tx, T.ty, T.temp_area)

        T.temp_area = nil
      end
    end
  end


  local function name_up_areas()
    -- give each area a 'db_name' -- for debugging

    each A in MAP.areas do
      A.db_name = A.kind .. "_" .. _index

      if A.has_start then
        debugf("Start area : %s\n", A.db_name)
      end
    end
  end


  local function debug_areas()
    -- use the GUI to show where the areas are
    each T in all_tiles do
      for dir = 2,8,2 do
        local N = tile_neighbor(T, dir)

        if not (N and N.area == T.area) then
          local field = "border" .. dir
          gui.set_tile(T.tx, T.ty, { [field] = "fff" })
        end
      end
    end
  end


  ---| W_AddTerrain |---

  create_temp_areas()
  convert_start_landmark()
  merge_areas()

  assign_terrain()
  convert_to_real_areas()

  name_up_areas()

--  debug_areas()
end


------------------------------------------------------------------------

function W_ConnectAreas()
  --
  -- This ensure that all the areas on the map are connected together
  -- (forming a undirected graph structure, with no loops or cycles).
  --

  local area_list

  local all_tiles = W_AllTiles()


  local function collect_visitable_areas()
    area_list = {}

    each A in MAP.areas do
      if A.kind == "empty" or A.kind == "water" then
        -- skip it
        continue
      end

      table.insert(area_list, A)

      -- assign a conn_group value
      A.conn_group = #area_list
    end
  end


  local function convert_matching_areas(old_kind)
    each A in MAP.areas do
      if A.kind == old_kind then
        local new_kind = rand.pick({ "forest", "mountain", "cave" })

        A.kind = new_kind

        -- fix all 'kind' values in tiles
        each T in all_tiles do
          if T.area == A then T.kind = new_kind end
        end

        table.insert(area_list, A)

        -- assign a conn_group value
        A.conn_group = #area_list
      end
    end
  end

  
  local function check_all_connected()
    for i = 2, #area_list do
      local A1 = area_list[i - 1]
      local A2 = area_list[i]

      if A1.conn_group ~= A2.conn_group then
        return false
      end
    end

    return true
  end


  local function eval_leafiness(A)
    -- the vulcano **MUST** be a leaf node
    if A.kind == "vulcano" and #A.conns > 0 then
      return -8888
    end

    -- dungeons should generally be leaves (except when connected to the vulcano)

    if A.kind == "dungeon" then
      if #A.conns > 1 then return 0 end
      
      if #A.conns == 1 then
        local C = A.conns[1]

        if C.A1.kind == "vulcano" or C.A2.kind == "vulcano" then
          -- OK
        else
          return 0
        end
      end
    end

    return 10
  end


  local function eval_kindness(A1, A2)
    -- dungeons should normally only connect to towns / castles / caves

    if A2.kind == "dungeon" then
      A1, A2 = A2, A1
    end

    if A1.kind == "dungeon" then
      if A2.kind == "town" or A2.kind == "castle" then
        return 50
      end

      if A2.kind == "dungeon"  then return 10 end
      if A2.kind == "vulcano"  then return 20 end
      if A2.kind == "cave"     then return 5 end
      if A2.kind == "mountain" then return 5 end

      return rand.irange(-40, 0)
    end

    -- mountains and caves like each other

    if A1.kind > A2.kind then
      A1, A2 = A2, A1
    end

    local k_str = A1.kind .. "_" .. A2.kind

    if k_str == "cave_mountain" or
       k_str == "cave_snow" or
       k_str == "mountain_vulcano" or
       k_str == "mountain_snow"
    then
      return 0.7
    end

    -- small preference for areas being a different kind

    if A1.kind ~= A2.kind then
      return 0.2
    end

    return 0
  end


  local function eval_connection(T, dir, N)
    local A1 = T.area
    local A2 = N.area

    -- ignore empty / water areas
    if not A1.conn_group then return -1 end
    if not A2.conn_group then return -1 end

    -- already part of same group?
    -- (this also checks for being the same area)
    if A1.conn_group == A2.conn_group then
      return -2
    end

    local score = 100

    score = score + eval_leafiness(A1)
    score = score + eval_leafiness(A2)

    score = score + eval_kindness(A1, A2)

    -- prefer not having multiple connections in a tile
    if T.has_conn or N.has_conn then
      score = score - 2
    end

    -- tie breaker
    return score + gui.random()
  end


  local function pick_a_connection()
    local best = { score=0 }

    for tx = 1, MAP.width do
    for ty = 1, MAP.height do
      local T = MAP.tiles[tx][ty]

      assert(T.area)

      for dir = 2,4,2 do
        local N = tile_neighbor(T, dir)

        if not N then continue end

        local score = eval_connection(T, dir, N)

        if score > best.score then
          best.T1 = T
          best.T2 = N
          best.dir = dir
          best.score = score
        end
      end
    end -- tx, ty
    end
    
    if best.score > 0 then
      return best
    end

    return nil  -- nothing was possible
  end


  local function merge_conn_groups(id1, id2)
    assert(id1 and id2)
    assert(id1 ~= id2)

    if id1 > id2 then id1,id2 = id2,id1 end

    each area in MAP.areas do
      if area.conn_group == id2 then
         area.conn_group = id1
      end
    end
  end


  local function make_a_connection(best)

    local C = W_NewConn(best.T1, best.dir, best.T2)

    -- sanity check
    assert(table.has_elem(area_list, C.A1))
    assert(table.has_elem(area_list, C.A2))

    merge_conn_groups(C.A1.conn_group, C.A2.conn_group)

    -- DEBUG
    --[[
    local field = "border" .. C.dir
    gui.set_tile(C.T1.tx, C.T1.ty, { [field] = "0f0" })
          field = "border" .. (10 - C.dir)
    gui.set_tile(C.T2.tx, C.T2.ty, { [field] = "0f0" })
    --]]
  end


  local function try_connect_areas()
    for loop = 1, 999 do
      local best = pick_a_connection()

      if not best then return end

      make_a_connection(best)

      -- when everything is connected, we are done
      if check_all_connected() then
        return
      end
    end
  end


  ---| W_ConnectAreas |---

  collect_visitable_areas()

  -- perform a few passes
  -- [ this is in case we cannot connect some areas due to water or empty
  --   areas being in the way -- need to convert them when this happens ]

  for pass = 1, 3 do
    try_connect_areas()

    if check_all_connected() then
      return  -- Success --
    end

    if pass == 1 then convert_matching_areas("water") end
    if pass == 2 then convert_matching_areas("empty") end
  end

  -- FAIL --

  error("Unable to connect all areas")
end



function W_DumpConnections()
  local seen = {}


  local function sub_tree_size(A, seen2)
    local total = 0

    seen2[A] = true

    each C in A.conns do
      local A2 = sel(C.A1 == A, C.A2, C.A1)

      if seen[A2] or seen2[A2] then continue end

      total = total + 1
      total = total + sub_tree_size(A2, seen2)
    end

    return total
  end


  local function dump_area(A, level)
    seen[A] = true

    local line = ""

    if level > 0 then
      line = string.rep(" ", (level - 1) * 4)
      line = line .. "+-- "
    end

    line = line .. A.db_name

    debugf("%s\n", line)

    local sort_conns = {}

    each C in A.conns do
      local A2 = sel(C.A1 == A, C.A2, C.A1)
    
      if not seen[A2] then
        local size = sub_tree_size(A2, {})
        size = size + _index / 1000 -- tie breaker
        table.insert(sort_conns, { A2=A2, size=size })
      end
    end

    table.sort(sort_conns, function(A, B) return A.size < B.size end)

    each sc in sort_conns do
      dump_area(sc.A2, level + 1)
    end
  end

  ---| W_DumpConnections |---

  dump_area(MAP.areas[1], 0)
end


------------------------------------------------------------------------

function W_GenerateWorld()
  --
  -- Generates a whole world (no easy feat!).
  --
  -- NOTE: this should never check the player's character, so that
  -- the player can replay this world with different characters.
  --

  W_CreateTiles()

  W_AddLandmarks()

  W_JoinLandmarks()

  W_AddTerrain()

  assert(MAP.home_x)

  W_ConnectAreas()
  W_DumpConnections()

  W_FleshOut()
end


NEIGHBOR_PREFIXES_1 =
{
  { 2 }
  { 4 }
  { 4, 2 }

  { 6 }
  { 6, 2 }
  { 6, 4 }
  { 6, 4, 2 }

  { 8 }
  { 8, 2 }
  { 8, 4 }
  { 8, 4, 2 }

  { 8, 6 }
  { 8, 6, 2 }
  { 8, 6, 4 }
  { 8, 6, 4, 2 }
}


NEIGHBOR_PREFIXES_2 =
{
  { 2 }
  { 4 }
  { 1 }

  { 6 }
  { 3 }
  { 6, 4 }
  { 3, 4 }

  { 8 }
  { 8, 2 }
  { 7 }
  { 8, 1 }

  { 9 }
  { 9, 2 }
  { 8, 1 }
  { 7, 3 }
}


function W_AnalyseNeighbors(T, nb_kind)
  -- returns an integer bitmask for which neighbors match the given kind

  local N
  local what = 0

  N = tile_neighbor(T, 2)
  if N and N.kind == nb_kind then what = what + 1 end

  N = tile_neighbor(T, 4)
  if N and N.kind == nb_kind then what = what + 2 end

  N = tile_neighbor(T, 6)
  if N and N.kind == nb_kind then what = what + 4 end

  N = tile_neighbor(T, 8)
  if N and N.kind == nb_kind then what = what + 8 end

  return what
end


function W_AutoSetupSides(T, file_prefix, nb_kind, prefix_tab)
  local sides = prefix_tab[W_AnalyseNeighbors(T, nb_kind)]

  if not sides then return end

  each side in sides do
    T.overlay[side] = string.format("%s%d_%s", file_prefix, side, nb_kind)
  end
end


------------------------------------------------------------------------

function forest_setup(T)
  T.background = "forest/0_forest11"
end


function desert_setup(T)
  T.background = "desert/0_oasis"
  
  T.player_x = 0.6
  T.player_y = 0.24

-- TEST : add some palm trees
  local palm1 =
  {
    spr_id = gui.create_sprite()
    image = "desert/palmtree"
    visible = true
    height = 120
    origin_x = 0.5
    origin_y = 1.0
    tile_x = T.tx + 0.2
    tile_y = T.ty + 0.5
  }

  local palm2 =
  {
    spr_id = gui.create_sprite()
    image = "desert/palmtree"
    visible = true
    height = 120
    origin_x = 0.5
    origin_y = 1.0
    tile_x = T.tx + 0.45
    tile_y = T.ty + 0.7
  }

  table.insert(T.decor, palm1)
  table.insert(T.decor, palm2)
end


function snow_setup(T)
  T.background = "world/snow_ph"
end


function mountain_setup(T)
  local dx = 1 + (T.tx % 2)
  local dy = 1 + (T.ty % 2)

  T.background = string.format("mountain/0_mount%d%d", dx, dy)
end


function cave_setup(T)
  T.background = "world/cave_ph"
end


function town_setup(T)
  T.background = "world/town_ph"
end


function castle_setup(T)
  T.background = "world/castle_ph"
end


function dungeon_setup(T)
  T.background = "world/dungeon_ph"
end


function water_setup(T)
  local dx = 1 + (T.tx % 2)
  local dy = 1 + (T.ty % 2)

  T.background = string.format("water/0_water%d%d", dx, dy)

  W_AutoSetupSides(T, "water/", "forest", NEIGHBOR_PREFIXES_2)
end


function vulcano_setup(T)
  local vulc_area = MAP.areas[1]

  local dx = T.tx - vulc_area.tx1 + 1
  local dy = T.ty - vulc_area.ty1 + 1

  assert(dx >= 1 and dx <= 3)
  assert(dy >= 1 and dy <= 3)

  T.background = string.format("vulcano/vulc_%d%d", dx, dy)
end


TILE_SETUP_FUNCS =
{
  forest   = forest_setup
  desert   = desert_setup
  snow     = snow_setup

  mountain = mountain_setup
  cave     = cave_setup
  water    = water_setup

  town     = town_setup
  castle   = castle_setup
  dungeon  = dungeon_setup
  vulcano  = vulcano_setup
}


function W_FleshOut()
  --
  -- select background image for each tile, and also side images
  -- plus decor images.
  --
  for tx = 1, MAP.width do
  for ty = 1, MAP.height do
    local T = MAP.tiles[tx][ty]

-- FIXME: TEST CRUD
-- if --[[ not T.kind and ]] T.path_info then
--  T.kind = "vulcano"
-- end

    assert(T.kind)

    if T.kind == "empty" then
      continue
    end

    local setup_func = TILE_SETUP_FUNCS[T.kind]
    if not setup_func then
      error("Bad tile kind: " .. tostring(T.kind))
    end

    setup_func(T)
  end
  end
end


------------------------------------------------------------------------


function W_MarkTileSeen(T)
  if T.visible then return end

  T.visible = true

  gui.set_tile(T.tx, T.ty, T)

  -- mark objects as visible (including monsters)

  each dec in T.decor do
    dec.visible = true
    gui.set_sprite(dec.spr_id, dec)
  end

  each mon in T.monsters do
    mon.visible = true
    gui.set_sprite(mon.spr_id, mon)
  end
end


function W_MarkWaterCorner(T, dir)
  local N = tile_neighbor(T, dir)

  if not (N and N.kind == "water") then return false end

  local NL = tile_neighbor(T, geom. LEFT_45[dir])
  local NR = tile_neighbor(T, geom.RIGHT_45[dir])

  if not (NL and NL.visible and NL.kind == "water") then return false end
  if not (NR and NR.visible and NR.kind == "water") then return false end

  W_MarkTileSeen(N)
end



function world_mark_seen(tx, ty)  -- NOT USED ATM
  local T = MAP.tiles[tx][ty]

  W_MarkTileSeen(T)

  -- show nearby water tiles if this tile is outdoorsy
  if tile_is_outdoors(T) then
    each dir in geom.SIDES do
      local N = tile_neighbor(T, dir)

      if N and N.kind == "water" then
        W_MarkTileSeen(N)
      end
    end

    each dir in geom.CORNERS do
      W_MarkWaterCorner(T, dir)
    end
  end
end


function world_mark_environ_seen(tx, ty)
  -- use to mark nearby tiles when you enter a new tile

  world_mark_seen(tx, ty)

  local T = MAP.tiles[tx][ty]

  each dir in geom.ALL_DIRS do
    if not T.can_see[dir] then continue end

    local N = tile_neighbor(T, dir)

    if N then
      W_MarkTileSeen(N)
    end
  end
end


function world_mark_all_seen()
  -- this is for debugging

  for nx = 1, MAP.width  do
  for ny = 1, MAP.height do
    local N = MAP.tiles[nx][ny]

    world_mark_seen(nx, ny)
  end
  end
end



function W_SyncAllTiles()
  for tx = 1, MAP.width do
  for ty = 1, MAP.height do
    local T = MAP.tiles[tx][ty]

    gui.set_tile(tx, ty, T)
  end
  end
end



function world_init()
  MAP = {}

  MAP.width  = 12
  MAP.height = 12

  MAP.areas = {}
  MAP.conns = {}
  MAP.all_mons = {}

  gui.rand_seed(RAND_SEED)

  gui.create_world(MAP.width, MAP.height)

  W_GenerateWorld()

  -- FIXME : debugging only!!
  -- world_mark_all_seen()

  W_SyncAllTiles()

  MAP.world_name = "FooBar"

  collectgarbage("collect")
end



function world_restore()
  -- this is called after loading a savegame

  assert(MAP.width)
  assert(MAP.height)

  gui.create_world(MAP.width, MAP.height)

  W_SyncAllTiles()

  monster_restore_all()
end

