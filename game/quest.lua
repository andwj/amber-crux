------------------------------------------------------------------------
--  QUEST STRUCTURE
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------


--ZONE CLASS
--[[
    ** this represents a group of areas **

    id : number

    areas : LIST   -- all the areas in this zone

    size   -- total number of tiles

    exits  -- total number of connections to a different zone

    kinds : TABLE   -- which area kinds are here ("town", "desert", etc)
--]]



function quest_create_zones()
  --
  -- We group the map areas into fewer, larger "ZONE" objects, so
  -- that when locking connections we only need a handful of keys
  -- (instead of 15-25 or more).
  --

  local MAX_SIZE  = 20
  local MAX_EXITS = 10

  local temp_zones = {}


  local function zone_from_area(A)
    local ZONE =
    {
      id = 1 + #temp_zones
      areas = { A }
      size  = # A.tiles
      exits = # A.conns
      kinds = {}
    }

    ZONE.kinds[A.kind] = true

    if A.has_start then
      ZONE.kinds["START"] = true
    end

    A.zone = ZONE

    table.insert(temp_zones, ZONE)
  end


  local function create_zones()
    -- create a zone for each traversible area

    each A in MAP.areas do
      -- ignore water/empty areas
      if A.conn_group then
        zone_from_area(A)
      end
    end
  end


  local function merge_two_zones(Z, Z2)
    -- Z2 is merged into Z

    debugf("merging ZONE_%d --> ZONE_%d\n", Z2.id, Z.id)

    table.kill_elem(temp_zones, Z2)

    table.append(Z.areas, Z2.areas)
    Z2.areas = nil

    Z.size = Z.size + Z2.size

    -- the '- 2' is because these two zones are connected, and hence each
    -- has an exit leading to the other one.
    Z.exits = Z.exits + Z2.exits - 2

    table.merge(Z.kinds, Z2.kinds)

    each A in MAP.areas do
      if A.zone == Z2 then
         A.zone = Z
      end
    end
  end


  local function evaluate_merger(Z1, Z2)
    -- cannot merge with ourself!
    if Z1 == Z2 then return -1 end

    -- never merge the vulcano
    if Z1.kinds["vulcano"] or Z2.kinds["vulcano"] then return -2 end

    -- result would be too big or too many exits?
    if Z1.size  + Z2.size      > MAX_SIZE  then return -3 end
    if Z1.exits + Z2.exits - 2 > MAX_EXITS then return -3 end

    -- too manu towns or castles?
    if Z1.kinds["town"]   and Z2.kinds["town"]   then return -4 end
    if Z1.kinds["castle"] and Z2.kinds["castle"] then return -4 end

    -- OK --

    -- want to merge the smallest zones
    local score = 900 - math.min(Z1.size, Z2.size) * 5

    score = score - (Z1.exits + Z2.exits)

    -- tie breaker
    return score + gui.random()
  end


  local function try_merge_zones()
    local best_Z1
    local best_Z2
    local best_score = 0

    each C in MAP.conns do
      local Z1 = C.A1.zone
      local Z2 = C.A2.zone

      assert(Z1 and Z2)

      local score = evaluate_merger(Z1, Z2)

      if score > best_score then
        best_score = score
        best_Z1 = Z1
        best_Z2 = Z2
      end
    end

    -- nothing was possible?
    if not best_Z1 then
      return false
    end

    merge_two_zones(best_Z1, best_Z2)

    return true  -- OK
  end


  local function debug_zones()
    -- use the GUI to show where the areas are
    each T in W_AllTiles() do
      for dir = 2,8,2 do
        local N = tile_neighbor(T, dir)
        if not (N and N.area and T.area and N.area.zone and N.area.zone == T.area.zone) then
          local field = "border" .. dir
          gui.set_tile(T.tx, T.ty, { [field] = "fff" })
        end
      end
    end
  end


  ---| quest_create_zones |---

  create_zones()

  debugf("Initial zones : %d\n", #temp_zones)

  while try_merge_zones() do end

  debugf("Final zones : %d\n", #temp_zones)

  MAP.zones = temp_zones

--debug_zones()
end



function quest_lock_connections()
  --
  -- ALGORITHM:
  --   1.  lock connection to current "target" zone (a leaf node)
  --   2.  remove TARGET from the working set
  --   3.  pick a leaf zone to contain the lock's solution
  --   4.  solution becomes the new TARGET zone
  --   5.  repeat until no more non-start leaves [or enough locks]
  --

  -- the working set
  local zones
  local target

  local MAX_LOCKS = 6


  local function prepare()
    zones = table.copy(MAP.zones)

    target = MAP.areas[1].zone
  end


  local function find_target_conn(T)
    each Z in zones do
      if Z == T then continue end

      each A in Z.areas do
      each C in A.conns do
        if C.A1.zone == T or C.A2.zone == T then
          assert(not C.lock)
          return C
        end
      end
      end
    end

    error("Cannot find connection to target!")
  end


  local function eval_for_solution(Z)
    -- check if zone can be used to contain a solution
    -- results < 0 are not usable at all

    if Z == target then return -1 end

    if Z.kinds["START"] then return -2 end

    local exits = 0
    local score = 50

    each A in Z.areas do
    each C in A.conns do
      local Z2 = sel(C.A1.zone == Z, C.A2.zone, C.A1.zone)

      if Z2 == Z then continue end

      if C.lock then continue end

      exits = exits + 1

      -- in emergency, can place solution in zone leading to target zone
      if C.A1.zone == target or C.A2.zone == target then
        score = score - 40
      end
    end
    end

    if exits == 0 then
      error("Quest failure : zone without any exits!")
    end

    if exits > 1 then
      return -3
    end

    -- TODO : better scoring?

    -- tie breaker
    return score + gui.random()
  end


  local function try_add_lock(index)
    -- check if we have any leafs to use for solutions
    local best_Z
    local best_score = 0

    each Z in zones do
      local score = eval_for_solution(Z)

      if score > best_score then
        best_Z = Z
        best_score = score
      end
    end

    if not best_Z then
--stderrf("No more locks possible.\n")
      return false
    end

    assert(not best_Z.solution)

    -- add the lock --

    local conn = find_target_conn(target)

    local LOCK =
    {
      kind = "key"

      -- TODO : decide this properly (possibly later on)
      item = "key" .. index

      conn = conn
    }

    conn.lock = LOCK

    best_Z.solution = LOCK

    -- make solution area the new target --

    table.kill_elem(zones, target)

    target = best_Z

    local prev = sel(conn.A1.zone == target, conn.A2.zone, conn.A1.zone)

--stderrf("  lock : ZONE_%s --> ZONE_%s  (sol: ZONE_%d)\n", prev.id, target.id, best_Z.id)

    return true
  end


  ---| quest_lock_connections |---

  prepare()

  for loop = 1, MAX_LOCKS do
    if not try_add_lock(loop) then break; end
  end

end



------------------------------------------------------------------------

function quest_traversibility()

  local function block_message(T, N)
    -- FIXME : proper messages

    return "You cannot travel in the desired direction dude."
  end


  local function visit_tile(T, dir, N)
    if not N or N.kind == "empty" then
      T.no_walk[dir] = block_message(T, N)
      return
    end

    -- FIXME : more stuff

    -- FIXME : proper visibility
    T.can_see[dir] = true
  end


  ---| quest_traversibility |---

  for tx = 1, MAP.width do
  for ty = 1, MAP.height do
    local T = MAP.tiles[tx][ty]

    each dir in geom.SIDES do
      local N = tile_neighbor(T, dir)

      visit_tile(T, dir, N)
    end
  end
  end
end


------------------------------------------------------------------------

function quest_add_weapons()
  -- TODO
end



function quest_add_monsters()
  -- TODO
end



function quest_add_health()
  -- TODO
end



------------------------------------------------------------------------

function quest_init()
  --
  -- Creates a quest structure for the generated world areas.
  --
  -- Primarily this means locking connections between areas and placing
  -- the solution (like a key) in an earlier, accessible area.
  --
  -- Secondly this will decide all the monsters and items which will
  -- appear in the world, ensuring that the player will always have
  -- enough weaponry / health / etc to win every battle.
  --

  quest_create_zones()

  quest_lock_connections()

  -- this does visibility too
  quest_traversibility()

  -- do weapons first, as it affects how strong the monsters will be
  quest_add_weapons()

  quest_add_monsters()

  -- now ensure we have enough health to survive
  quest_add_health()
end

