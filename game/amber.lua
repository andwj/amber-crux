------------------------------------------------------------------------
--  MAIN FILE / INTERFACE TO C++ code
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------

require "defs"
require "util"
require "save"

require "help"
require "option"
require "naming"
require "battle"

require "item"
require "monster"
require "player"
require "quest"
require "world"


------------------------------------------------------------------------
--   GAME  LOGIC
------------------------------------------------------------------------

-- ugh
IS_REPLAY = false


function new_game()
  if IS_REPLAY then
    printf("\nReplaying the previous game.\n")
  else
    printf("\nNew game started.\n")
  end

  gui.game_state("active")

   world_init()
   quest_init()
  player_init()

  screen_off()

  player_begin_message()

  menu_clear()
  menu_push(GAME_MENU)

  IS_REPLAY = false
end


function replay_game(char_info)
  IS_REPLAY = true

  PLAYER_LAST_RACE = assert(char_info)

  menu_push(CHARACTER_MENU)
end


------------------------------------------------------------------------
--   SCREEN LOGIC
------------------------------------------------------------------------

TITLE_SCREEN =
{
  width  = 800, char_w = 20
  height = 500, char_h = 12

  elements =
  {
    { image="ui/title", x=0, y=0, w=800, h=500 }
  }
}


function screen_show(screen)
  SCREEN = assert(screen)

  -- this clears (invalidates) any existing elements
  gui.screen_begin(screen)

  each E in screen.elements do
    -- for option screens, grab current value from PREFS
    if screen.is_options and E.opt_name then
      pref_to_element(E)
    end

    local id = gui.create_element()

    gui.set_element(id, E)
  end
end


function screen_off()
  SCREEN = nil

  gui.screen_off()
end


------------------------------------------------------------------------
--   MENU LOGIC
------------------------------------------------------------------------

MAIN_MENU =
{
  title = "Main Menu"

  items =
  {
    { label="&New Game",  act="new" }
    { label="&Help",      act="help" }
    { label="&Options",   act="options" }
    { label="&Quit",      act="!quit" }
  }

  enter_func = function()
    screen_show(TITLE_SCREEN)
  end

  back_func = function()
    screen_show(TITLE_SCREEN)
  end

  menu_func = function(act, param)
    if act == "new" then
      menu_push(CHARACTER_MENU)

    elseif act == "help" then
      menu_push(HELP_MENU)

    elseif act == "options" then
      menu_push(OPTION_MENU)
    end
  end

  key_func = function(act, param)
    if act == "help" or act == "options" then
      MENU.menu_func(act)
    end
  end
}


MISC_MENU =
{
  title = "Misc Stuff"

  items =
  {
    { label="&Help",          act="help" }
    { label="&Options",       act="options" }
    { label="&Discard Game",  act="!discard" }
    { label="&Save / Exit",   act="save" }

    { label="<----", act="back" }
  }

  back_func = function()
    -- we want to return to GAME_MENU from the Help or Option screens
    menu_pop()
  end

  menu_func = function(act, param)
    if act == "help" then
      menu_push(HELP_MENU)

    elseif act == "options" then
      menu_push(OPTION_MENU)
    end
  end

  key_func = function(act, param)
    if act == "help" or act == "options" then
      MENU.menu_func(act)
    end
  end
}


GAME_MENU =
{
  title = "Game Menu"

  items =
  {
    { label="&Go",        act="go" }
    { label="&Fight",     act="fight" }
    { label="&Inventory", act="inventory" }
    { label="&Misc...",   act="misc" }
  }

  back_func = function()
    -- ensure Help / Options / Battle screens are gone
    screen_off()
  end

  menu_func = function(act, param)
    if act == "go" then
      menu_push(WALK_MENU)

    elseif act == "fight" then
      -- FIXME
      battle_test()

    elseif act == "misc" then
      menu_push(MISC_MENU)
    end
  end

  key_func = function(act, param)
    if act == "help" or act == "options" then
      -- simulate user going into the MISC sub-menu
      menu_push(MISC_MENU)

      MENU.menu_func(act)
    end
  end
}


GAME_OVER_MENU =
{
  title = "Game Over"

  no_back = true

  items =
  {
    { label="&Replay Game",  act="!replay" }
    { label="&New Game",     act="!fresh" }
    { label="&Quit",         act="!quit" }
  }
}


function menu_clear()
  -- clear the menu stack
  -- a call to this should be immediately followed by a menu_push()
  MENU = nil
end


function menu_push(new_menu)
  assert(new_menu)
  assert(new_menu ~= MENU)

  new_menu.last_menu = MENU

  MENU = new_menu

  gui.set_menu(MENU.title, MENU.items)

  if MENU.enter_func then
     MENU.enter_func()
  end
end


function menu_pop()
  assert(MENU)

  if not MENU.last_menu then
    error("menu_pop : tried to pop the last menu")
  end

  MENU = MENU.last_menu

  gui.set_menu(MENU.title, MENU.items)

  if MENU.back_func then
     MENU.back_func()
  end
end


function menu_replace(new_menu)
  -- this is like pop() followed by push(), but without calling any hook functions
  assert(MENU)

  new_menu.last_menu = MENU.last_menu

  MENU = new_menu

  gui.set_menu(MENU.title, MENU.items)
end


function menu_go_back()
  assert(MENU)

  if MENU.no_back or not MENU.last_menu then
    -- beep ??
    return
  end

  menu_pop()
end


function menu_handle_key(act, param)
  assert(MENU)

  -- need a key handler in either current MENU or its parent

  if MENU.key_func then
     MENU.key_func(act, param)
     return
  end

  if not MENU.no_back and MENU.last_menu then
    if MENU.last_menu.key_func then
      -- simulate user pressing the back button
      menu_pop()

      -- last_menu is now in 'MENU' global
      MENU.key_func(act, param)
      return
    end
  end

  -- beep ??
end


function menu_handle_menu(act, param)
  assert(MENU)

  if not MENU.menu_func then
    error("Menu has no handler for: " .. tostring(act))
  end

  MENU.menu_func(act, param)
end


function menu_handle_control(name, value)
  assert(MENU)

  if MENU.control_func then
     MENU.control_func(name, value)
  end
end


--
-- Main routine for doing a user game action, e.g. from a menu or
-- a key binding.  The 'source' parameter can be one of the following:
--
--    "menu"    :  from user clicking a menu item
--    "key"     :  from user pressing a key on keyboard (or mouse button)
--    "control" :  user adjusts a control in an option screen
--    "system"  :  special cases (mainly "save" action if user tries to close
--                 the window via the window manager)
--
function amb_user_action(source, act, param)
  
--[[ DEBUG
  printf("amb_user_action.... source: '%s' act: '%s' param: '%s'\n",
         tostring(source), tostring(act), tostring(param))
--]]

  if act == "back" then
    menu_go_back()
    return
  end

  if act == "save" then
    save_game()
    return
  end


  if source == "menu" then
    menu_handle_menu(act, param)
    return
  end

  if source == "key" then
    menu_handle_key(act, param)
    return
  end

  if source == "control" then
    menu_handle_control(act, param)
    return
  end
end


------------------------------------------------------------------------

--
-- called by C++ code after loading the scripts
--
-- mode parameter can be "start", "newgame" or "replay"
-- seed parameter is required
-- char_info is only used for "replay" mode
--
function amb_init(mode, seed, char_info)
  printf("Initializing scripts...\n")

  RAND_SEED = assert(seed)

  debugf("RANDOM SEED = %d\n", seed)

  -- this only for the random choice of character, as the
  -- world generator will reseed the RNG to RAND_SEED.
  gui.rand_seed(seed + 1)

  help_set_version()

  -- existence of a savefile overrides the "mode" parameter
  -- [ it should occur exist when mode == "start" ]
  if gui.sv_exists(SAVEFILE) then
    load_game()

  elseif mode == "newgame" then
    menu_push(MAIN_MENU)
    menu_push(CHARACTER_MENU)

  elseif mode == "replay" then
    replay_game(char_info)

  else
    menu_push(MAIN_MENU)
  end
end


--
-- called by C++ code when a script error occurs.
-- this reads the stack-trace and prints it to the console / log file.
--
function amb_traceback(msg)

  -- guard against very early errors
  if not gui then
    return msg
  end

  gui.raw_print("\n")
  gui.raw_print("****** ERROR OCCURRED ******\n\n")
  gui.raw_print("Stack Trace:\n")

  local stack_limit = 40

  local function format_source(info)
    if not info.short_src or info.currentline <= 0 then
      return ""
    end

    local base_fn = string.match(info.short_src, "[^/]*$")
 
    return string.format("@ %s:%d", base_fn, info.currentline)
  end

  for i = 1,stack_limit do
    local info = debug.getinfo(i+1)
    if not info then break end

    if i == stack_limit then
      gui.raw_print("(remaining stack trace omitted)\n")
      break;
    end

    if info.what == "Lua" then
      local func_name = "???"

      if info.namewhat and info.namewhat ~= "" then
        func_name = info.name or "???"
      else
        -- perform our own search of the global namespace,
        -- since the standard LUA code (5.1.2) will not check it
        -- for the topmost function (the one called by C code)
        each k,v in _G do
          if v == info.func then
            func_name = k
            break;
          end
        end
      end

      gui.raw_print(string.format("  %2d: %s() %s\n", i, func_name, format_source(info)))

    elseif info.what == "main" then
      gui.raw_print(string.format("  %2d: main body %s\n", i, format_source(info)))

    elseif info.what == "tail" then
      gui.raw_print(string.format("  %2d: tail call\n", i))

    elseif info.what == "C" then
      if info.namewhat and info.namewhat ~= "" then
        gui.raw_print(string.format("  %2d: c-function %s()\n", i, info.name or "???"))
      end
    end
  end

  return msg
end


--
-- called by C++ code while reading the PREFS.txt file
-- also called when using the option screens
--
function amb_set_preference(name, value)
  assert(name)
  assert(value)

  -- only accept preferences which already exist
  -- (otherwise put it into MISC_OPTS)
  if PREFS[name] == nil then
    MISC_OPTS[name] = value
    return
  end

  -- handle numeric and boolean values
  if type(PREFS[name]) == "number" then
    PREFS[name] = tonumber(value) or 0
    
  elseif type(PREFS[name]) == "boolean" then
    PREFS[name] = (value == "true")

  else
    PREFS[name] = value
  end

  -- allow engine-side options to take effect
  gui.change_preference(name, tostring(PREFS[name]))
end


--
-- called by C++ code while writing the PREFS.txt file
--
function amb_read_preferences()
  local keys = table.keys(PREFS)

  table.sort(keys)

  each k in keys do
    local key_str = tostring(k)
    local val_str = tostring(PREFS[k])

    gui.config_line(key_str .. " = " .. val_str)
  end

  gui.config_line("")
  gui.config_line("-- END --")
end


