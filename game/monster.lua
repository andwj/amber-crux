------------------------------------------------------------------------
--  EVIL MONSTERS
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------


--MONSTER CLASS :: ACTOR
--[[
    TODO
--]]


MONSTER_INFO =
{
  bear =
  {
    stats =
    {
      health = 70
      armor  = 0
    }

    faces = "right"

    sprite =
    {
      image = "monster/bear"
      height = 80
      origin_x = 0.5
      origin_y = 1.0
    }
  }

  mummy =
  {
    stats =
    {
      health = 25
      armor  = 0
    }

    faces = "front"

    sprite =
    {
      image = "monster/mummy"
      height = 100
      origin_x = 0.5
      origin_y = 1.0
    }
  }

  rat =
  {
    stats =
    {
      health = 10
      armor  = 0
    }

    faces = "left"

    sprite =
    {
      image = "monster/rat"
      height = 50
      origin_x = 0.5
      origin_y = 1.0
    }
  }

  wolf =
  {
    stats =
    {
      health = 40
      armor  = 0
    }

    faces = "left"

    sprite =
    {
      image = "monster/wolf"
      height = 70
      origin_x = 0.5
      origin_y = 1.0
    }
  }

}



function M_CreateSprite(mon)
  local info = actor_lookup(mon)

  if not mon.spr_id then
    mon.sprite.spr_id = gui.create_sprite()
    mon.sprite.visible = true
  end

  gui.set_sprite(mon.sprite.spr_id, mon.sprite)
end



function monster_restore_all()
  each mon in MAP.all_mons do
    M_CreateSprite(mon)
  end
end

