------------------------------------------------------------------------
--  PLAYER STUFF
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------


--ACTOR CLASS   (base class for PLAYER and MONSTER)
--[[
    tx, ty    --  tile coordinates where actor is

    kind : keyword  -- "player", "monster" or "npc"

    race : keyword  -- for looking up in MONSTER_INFO or PLAYER_INFO

    stats : ACTOR_STATS   -- contains 'health', 'armor', etc...

    is_dead : boolean   -- true if has been killed

    inventory : LIST  -- currently held (or worn) items
    armor     : LIST
    spells    : LIST

    sprite : TABLE  -- sprite information (image, size, location)
--]]



--INVENTORY_ITEM CLASS
--[[
    item : ITEM      -- the actual item, NIL for empty slots

    usage : keyword  -- normally NIL, can be "worn", "held" or "spell"

    tile_x, tile_y   -- position within inventory box
--]]



--ACTOR_STATS CLASS
--[[
    health : integer
    mana   : integer
    armor  : integer   -- computed from all worn objects
    gold   : integer
--]]



--PLAYER OBJECT :: ACTOR
--[[
--]]


------------------------------------------------------------------------


PLAYER = {}


PLAYER_LAST_RACE = "random"


PLAYER_INFO =
{
  male =
  {
    stats =
    {
      health = 500
      mana   = 150
      armor  = 2
      gold   = 200
    }

    sprite =
    {
      image = "player/male_idle"
      height = 100
      origin_x = 0.5
      origin_y = 1.0
    }

    inventory =
    {
      {
        item  = "sword"
        usage = "held"
        spot  = 1
      }

      {
        item  = "leather_armor"
        usage = "worn"
        spot  = 2
      }

      {
        item  = "hiking_boots"
        usage = "worn"
        spot  = 3
      }
    }

    spells =
    {
    }
  }

  female =
  {
    stats =
    {
      health = 420
      mana   = 300
      armor  = 2
      gold   = 400
    }

    sprite =
    {
      image = "player/female_idle"
      height = 100
      origin_x = 0.5
      origin_y = 1.0
    }

    inventory =
    {
      {
        item  = "dagger"
        usage = "held"
        spot  = 1
      }

      {
        item  = "elven_armor"
        usage = "worn"
        spot  = 2
      }
    }

    spells =
    {
    }
  }

  ogre =
  {
    stats =
    {
      health = 560
      mana   = 0
      armor  = 4
      gold   = 100
    }

    sprite =
    {
      image = "player/ogre_idle"
      height = 100
      origin_x = 0.5
      origin_y = 1.0
    }

    inventory =
    {
      {
        item  = "sword"
        usage = "held"
        spot  = 1
      }

      {
        item  = "chainmail"
        usage = "worn"
        spot  = 2
      }

      {
        item  = "helmet"
        usage = "worn"
        spot  = 1
      }
    }

    spells = {}
  }
}


INVENTORY_BOX =
{
  sprite =
  {
    image = "ui/invent_box"
    height = 50
    min_scale = 1500.0

    -- this origin shows the inventory to the right of the player
    origin_x = -0.70
    origin_y = 1.2
  }

  armor_height = 20

  armor_spots =
  {
    { tile_x = 0.20, tile_y = 0.77 }
    { tile_x = 0.20, tile_y = 0.52 }
    { tile_x = 0.20, tile_y = 0.27 }
  }

  item_height = 13

  item_spots =
  {
    { tile_x = 0.40, tile_y = 0.82 }
    { tile_x = 0.55, tile_y = 0.82 }
    { tile_x = 0.69, tile_y = 0.82 }
    { tile_x = 0.84, tile_y = 0.82 }

    { tile_x = 0.40, tile_y = 0.67 }
    { tile_x = 0.55, tile_y = 0.67 }
    { tile_x = 0.69, tile_y = 0.67 }
    { tile_x = 0.84, tile_y = 0.67 }

    { tile_x = 0.40, tile_y = 0.52 }
    { tile_x = 0.55, tile_y = 0.52 }
    { tile_x = 0.69, tile_y = 0.52 }
    { tile_x = 0.84, tile_y = 0.52 }

    { tile_x = 0.40, tile_y = 0.37 }
    { tile_x = 0.55, tile_y = 0.37 }
    { tile_x = 0.69, tile_y = 0.37 }
    { tile_x = 0.84, tile_y = 0.37 }

    { tile_x = 0.40, tile_y = 0.22 }
    { tile_x = 0.55, tile_y = 0.22 }
    { tile_x = 0.69, tile_y = 0.22 }
    { tile_x = 0.84, tile_y = 0.22 }
  }
}


SPELL_BOX =
{
  sprite =
  {
    image = "ui/spell_box"
    height = 18
    min_scale = 1500.0

    -- this origin places the box to the right of the player
    origin_x = -0.95
    origin_y = 4.6
  }

  item_height = 38

  item_spots =
  {
    { tile_x = 0.16, tile_y = 0.62 }
    { tile_x = 0.33, tile_y = 0.62 }
    { tile_x = 0.50, tile_y = 0.62 }
    { tile_x = 0.67, tile_y = 0.62 }
    { tile_x = 0.84, tile_y = 0.62 }
  }
}


GRAVESTONE_SPRITES =
{
  grave2 =
  {
    image = "player/grave2"
    height = 60
    origin_x = 0.5
    origin_y = 1.0
  }
}


------------------------------------------------------------------------

CHARACTER_MENU =
{
  title = "Pick Race:"

  items =
  {
    { label="&Adventurer",  act="set_char",  param1="male" }
    { label="Elven &Mage",  act="set_char",  param1="female" }
    { label="The &Beast",   act="set_char",  param1="ogre" }
    { label="",             act=""     }
    { label="OK",           act="ok"   }

    -- we need to restart everything when this is a replay
    { label="Cancel",       act="!restart" }
  }

  enter_func = function()
    player_show_race(PLAYER_LAST_RACE)
  end

--[[  Not needed
  back_func = function()
    player_show_race(PLAYER_LAST_RACE)
  end
--]]

  menu_func = function(act, param)
    if act == "set_char" then
      player_show_race(param)

    elseif act == "ok" then
      new_game()
    end
  end
}


PLAYER_SCREEN =
{
  width  = 800, char_w = 20
  height = 500, char_h = 12

  elements =
  {
    {
      image="ui/broken_tower",
      x=300, y=520, h=560, align="B"
    }
  }
}


PLAYER_ELEMENTS =
{
  male =
  {
    {
      image="player/male_idle",
      x=280, y=370, h=360, align="B"
    }
  }

  female =
  {
    {
      image="player/female_idle",
      x=310, y=370, h=360, align="B"
    }
  }

  ogre =
  {
    {
      image="player/ogre_idle",
      x=315, y=370, h=360, align="B"
    }
  }
}


function player_show_race(race)
  if race == "random" then
    race = rand.pick(table.keys(PLAYER_INFO))
  end

  PLAYER_LAST_RACE = race

  screen_show(PLAYER_SCREEN)

  -- augment basic screen with race-specific stuff

  local elems = PLAYER_ELEMENTS[race]
  assert(elems)

  each E in elems do
    local id = gui.create_element()

    gui.set_element(id, E)
  end
end



function actor_lookup(mon)
  local info

  if mon.kind == "player" then
    info = PLAYER_INFO[mon.race]

  elseif mon.kind == "monster" then
    info = MONSTER_INFO[mon.race]

  else
    error("Unknown actor kind: " .. tostring(mon.kind))
  end

  if not info then
    error("Unknown actor race: " .. tostring(mon.race))
  end

  return info
end



function P_CreateSprite()
  if not PLAYER.spr_id then
    PLAYER.sprite.spr_id  = gui.create_sprite()
    PLAYER.sprite.visible = true

    PLAYER.invent_box.spr_id  = gui.create_sprite()
    PLAYER.invent_box.visible = true

    PLAYER. spell_box.spr_id  = gui.create_sprite()
    PLAYER. spell_box.visible = true
  end

  gui.set_sprite(PLAYER.sprite.spr_id, PLAYER.sprite)

  gui.set_sprite(PLAYER.invent_box.spr_id, PLAYER.invent_box)
  gui.set_sprite(PLAYER. spell_box.spr_id, PLAYER. spell_box)

-- TEST
  local tome_id = gui.create_sprite()

  gui.set_sprite(tome_id,
  {
    image = "raven/book_red"
    visible = true
    origin_x = 0.5
    origin_y = 0.5

    child_height = SPELL_BOX.item_height
    tile_x = 0.33
    tile_y = 0.62

    drag_to = "w"
  })

  gui.set_sprite(tome_id, { parent_id = PLAYER.spell_box.spr_id })
end


function P_SetLocation(home_mode)
  local T = MAP.tiles[PLAYER.tx][PLAYER.ty]

  world_mark_environ_seen(PLAYER.tx, PLAYER.ty)

  PLAYER.sprite.tile_x = PLAYER.tx + (T.player_x or 0.5)
  PLAYER.sprite.tile_y = PLAYER.ty + (T.player_y or 0.42)

  gui.set_sprite(PLAYER.sprite.spr_id, PLAYER.sprite)

  -- move the inventory / spell boxes too
  -- [ same coordinate : origin_x/y fields offset them to the right ]
  PLAYER.invent_box.tile_x = PLAYER.sprite.tile_x
  PLAYER.invent_box.tile_y = PLAYER.sprite.tile_y

  PLAYER.spell_box.tile_x = PLAYER.sprite.tile_x
  PLAYER.spell_box.tile_y = PLAYER.sprite.tile_y

  gui.set_sprite(PLAYER.invent_box.spr_id, PLAYER.invent_box)
  gui.set_sprite(PLAYER. spell_box.spr_id, PLAYER. spell_box)

  if home_mode == "normal" then
    gui.move_home(PLAYER.tx, PLAYER.ty)
  elseif home_mode == "zoom" then
    gui.move_home(PLAYER.tx, PLAYER.ty, "zoom")
  end
end



function P_CopySpotList(list)
  local new_list = {}

  each spot in list do
    table.insert(new_list, table.copy(spot))
  end

  return new_list
end


function P_SetupInventory()
  local info = actor_lookup(PLAYER)

  PLAYER.invent_box = table.copy(INVENTORY_BOX.sprite)
  PLAYER. spell_box = table.copy(    SPELL_BOX.sprite)

  PLAYER.inventory = P_CopySpotList(INVENTORY_BOX.item_spots)
  PLAYER.armor     = P_CopySpotList(INVENTORY_BOX.armor_spots)
  PLAYER.spells    = P_CopySpotList(SPELL_BOX.item_spots)

  -- objects --

  each inv in info.inventory do
    local spot = inv.spot

    if inv.usage == "worn" then
      if not spot or spot < 1 or spot > #PLAYER.armor then
        error("bad/missing spot for player starting armor item")
      end

      spot = PLAYER.armor[spot]

    else
      if not spot or spot < 1 or spot > #PLAYER.inventory then
        error("bad/missing spot for player starting item")
      end

      spot = PLAYER.inventory[spot]
    end

    local info = item_lookup(inv.name)

    spot.item  = item_create(info)
    spot.usage = inv.usage
  end

  -- spells --

  each inv in info.spells do
    local spot = inv.spot

    if not spot or spot < 1 or spot > #PLAYER.spells then
      error("bad/missing spot for player starting spell")
    end

    spot = PLAYER.spells[spot]

    -- TODO
  end
end


function actor_compute_armor(mon)
  local armor = 0

  each inv in mon.inventory do
    local item = item_lookup(inv.item)
    
    if inv.usage == "worn" then
      armor = armor + (item.armor or 0)
    end
  end

  mon.stats.armor = armor
end


function actor_adjust_damage(actor, damage)
  -- FIXME

  return damage
end



function player_init()
  -- create a fresh player object
  MAP.player = {}

  PLAYER = MAP.player

  PLAYER.kind = "player"
  PLAYER.race = assert(PLAYER_LAST_RACE)

  gui.set_char_info(PLAYER.race)

  local info = actor_lookup(PLAYER)

  P_SetupInventory()
 
  PLAYER.stats = table.copy(info.stats)

--!! FIXME  actor_compute_armor(PLAYER)

  gui.set_stats(PLAYER.stats)

  PLAYER.sprite = table.copy(info.sprite)

  P_CreateSprite()

  PLAYER.tx = MAP.home_x
  PLAYER.ty = MAP.home_y

  P_SetLocation("zoom")
end



function player_restore()
  -- this is called after loading a savegame

  PLAYER = MAP.player

  assert(PLAYER)

  local info = actor_lookup(PLAYER)

  -- FIXME uhhhh, more?

  gui.set_stats(PLAYER.stats)

  P_CreateSprite()
  P_SetLocation("zoom")
end


------------------------------------------------------------------------

function player_begin_message()
  messagef("You arrive in the land of %s.", MAP.world_name)
  messagef("The magic portal closes behind you.")
end



function player_die(mod, killer)
  --
  -- Aww poor baby gone bye bye...
  --
  -- mod is the Method Of Death, can be:
  --    "hurt"       -- fatally wounded by a monster (etc)
  --    "exhausted"  -- all tuckered out
  --    "destroyed"  -- body has gone (eaten, vaporized, drowned)
  --
  -- killer is the monster which did the deed (can be nil)
  --
  -- a messagef() describing the death should have already been done.
  --

  printf("\nPlayer died.\n")

  PLAYER.is_dead = true

  -- update game state
  gui.game_state("finished")

  -- turn off battle screen
  screen_off()

  -- turn sprite into a gravestone (unless body is gone)
  if mod == "destroyed" then
    PLAYER.sprite.visible = false
  else
    table.merge(PLAYER.sprite, GRAVESTONE_SPRITES.grave2)

    -- zoom into player
    gui.move_home(PLAYER.tx, PLAYER.ty, "close")
  end

  gui.set_sprite(PLAYER.sprite.spr_id, PLAYER.sprite)

  -- show the game-over menu
  menu_clear()
  menu_push(GAME_OVER_MENU)
end



VICTORY_SCREEN =
{
  width  = 800, char_w = 20
  height = 500, char_h = 12

  elements =
  {
    {
      image="ui/victory",
      x=-150, y=-50, w=1100, h=750
    }

    {
      text="You Have Won!",
      x=400, y=50, scale=2.5,
      color="f00", align="M"
    }
  }
}


VICTORY_PLAYERS =
{
  male =
  {
    image="player/male_idle",
    x=360, y=340, h=300, align="M"
  }

  female =
  {
    image="player/female_idle",
    x=390, y=330, h=300, align="M"
  }

  ogre =
  {
    image="player/ogre_idle",
    x=400, y=330, h=300, align="M"
  }
}


function player_victory()
  --
  -- The player has won the game!  Woohoo!!
  --

  -- update game state
  gui.game_state("finished")

  -- show the victory screen
  screen_show(VICTORY_SCREEN)

  -- add the player image
  local p_elem = VICTORY_PLAYERS[PLAYER.race]
  local p_id = gui.create_element()
  gui.set_element(p_id, p_elem)

  -- show the game-over menu
  menu_clear()
  menu_push(GAME_OVER_MENU)
end


------------------------------------------------------------------------

WALK_MENU =
{
  title = "Go Where?"

  items =
  {
    { label="&North",  act="walk",  param1="north" }
    { label="&East",   act="walk",  param1="east"  }
    { label="&South",  act="walk",  param1="south" }
    { label="&West",   act="walk",  param1="west"  }

    { label="<----",   act="back" }
  }

  menu_func = function(act, param)
    if act == "walk" then
      menu_pop()

      player_try_walk(param)
    end
  end
}


function can_walk(T, dir)
end



function player_try_walk(dir_name)
  -- convert direction from a name to a number

  assert(DIR_NAMES[dir_name])

  local dir = DIR_NAMES[dir_name]

  -- see if walk is possible
  local T = MAP.tiles[PLAYER.tx][PLAYER.ty]
  local N = tile_neighbor(T, dir)

  if N and N.kind == "water" then
    messagef("You cannot swim!")
    return
  end

  if T.no_walk[dir] then
    messagef(T.no_walk[dir])
    return
  end

  -- sanity check
  if not N then
    messagef("You cannot go that way.");
    return
  end

  -- perform the move --

  PLAYER.tx = N.tx
  PLAYER.ty = N.ty

  P_SetLocation("zoom")

  -- FIXME : compute 'effort', subtract from health

  messagef("You go %s.", dir_name);
end

