------------------------------------------------------------------------
--  BASIC DEFINITIONS
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------

--
-- the whole game-world is contained in the 'MAP' object
--
MAP = {}

--
-- user interface stuff
--
MENU = nil
SCREEN = nil

--
-- default game preferences
--
PREFS  =
{
  -- screen arrangement
  panel_right = false
  text_top    = false

  -- TEST CRUD
  blah = 0
  debug_messages = false
  slidey = 32.5
}

MISC_OPTS =
{ }

-- map direction names to numbers
DIR_NAMES =
{
  north = 8
  south = 2
  west  = 4
  east  = 6
}

