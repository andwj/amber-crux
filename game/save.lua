------------------------------------------------------------------------
--  SAVE / RESTORE GAME STATE
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------

--
--  NOTES
--  =====
--
--  This is fairly simplistic code for serialization of Lua tables
--  and de-serialization to get them back again.  The actual way
--  that "ATOMS" are written and read from the savegame file is
--  handled in the C++ code (see m_lua.cc).
--
--  Does _not_ handle:
--
--    +  metatables
--    +  functions
--    +  threads
--    +  userdata (light or heavy)
--    +  strings with embedded zeros
--

SAVEFILE = "save.dat"


function save_error(msg)
  -- We delete the savefile, otherwise the game will not start the next
  -- time the user goes to play (error caused by corrupt savefile).
  --
  -- Used for load errors too.

  printf("Fatal problem with savegame, deleting savefile...\n")

  gui.sv_close()
  gui.sv_delete(SAVEFILE)

  error(msg)
end


------------------------------------------------------------------------
--   SAVING  LOGIC
------------------------------------------------------------------------

-- this collects all table which have been processed, and maps them
-- into an index value.
SEEN_TAB   = {}
SEEN_INDEX = 0


function save_value(val)
  local kind = type(val)

  -- handle simple data types
  if kind == "nil" then
    gui.sv_write_atom("nil")
    return

  elseif kind == "boolean" or kind == "number" or kind == "string" then
    gui.sv_write_atom(kind, val)
    return
  end

  -- handle tables
  if kind == "table" then
    save_table(val)
    return
  end

  -- nothing else is supported
  save_error("Save game: found unsaveable value of type: " .. tostring(kind))
end



function save_table(T)
  if T == _G then
    save_error("Save game: hit reference to global space")
  end

  if SEEN_TAB[T] ~= nil then
    gui.sv_write_atom("reference", SEEN_TAB[T])
    return
  end

  SEEN_INDEX  = SEEN_INDEX + 1
  SEEN_TAB[T] = SEEN_INDEX

  gui.sv_write_atom("table")

  each key,val in T do
    save_value(key)
    save_value(val)
  end

  gui.sv_write_atom("end_table")
end



function save_header_string()
  save_value(gui.version())
end



function save_game()
  assert(gui.game_state() == "active")

  printf("\nSaving the game...\n")

  gui.sv_open(SAVEFILE, "w")

  save_header_string()

  SEEN_TAB   = {}
  SEEN_INDEX = 0

  -- everything is contained in the 'MAP' object
  save_table(MAP)

  gui.sv_close()

  debugf("DONE.\n\n")

  SEEN_TAB = {}

  gui.force_quit()
end


------------------------------------------------------------------------
--   LOADING  LOGIC
------------------------------------------------------------------------

function load_table()
  local T = {}

  SEEN_INDEX  = SEEN_INDEX + 1
  SEEN_TAB[SEEN_INDEX] = T

  while true do
    local kind, val = gui.sv_read_atom()

    if kind == "end_table" then break; end

    local key = load_value(kind, val)

    if key == nil then
      save_error("Corrupt savegame (key is nil)")
    end

    T[key] = load_value()
  end

  -- automatically recreate sprite refs
  -- [ the saved ID values would be invalid anyway ]
  if T.spr_id then
    T.spr_id = gui.create_sprite()

    if T.visible then
      gui.set_sprite(T.spr_id, T)
    end
  end

  return T
end



function load_value(kind, val)
  if kind == nil then
    kind, val = gui.sv_read_atom()
  end

  -- hit end of the file, or a read error?
  if kind == "ERROR" then
    save_error("Corrupt savegame (end of file / read error)")
  end

  -- handle simple data types
  if kind == "nil" then
    return nil

  elseif kind == "boolean" or kind == "number" or kind == "string" then
    return val
  end

  -- handle tables
  if kind == "table" then
    return load_table()

  elseif kind == "end_table" then
    save_error("Corrupt savegame (unexpected end_table)")

  elseif kind == "reference" then
    assert(val)
    if SEEN_TAB[val] == nil then
      save_error("Corrupt savegame (illegal reference #" .. tostring(val) .. ")")
    end
    return SEEN_TAB[val]
  end

  -- got a weird atom?
  save_error("Internal error: unknown save atom: " .. tostring(kind))
end



function expect_table()
  local T = load_value()

  if type(T) ~= "table" then
    save_error("Corrupt savegame (missing table)")
  end

  return T
end



function load_check_header()
  local kind, val = gui.sv_read_atom()

  if kind ~= "string" then
    save_error("Corrupt savegame (missing header string)")
  end

  if val ~= gui.version() then
    save_error("Savegame is from a different version!")
  end
end



function load_game()
  printf("Restoring a saved game...\n")

  gui.sv_open(SAVEFILE, "r")

  load_check_header()

  SEEN_TAB   = {}
  SEEN_INDEX = 0

  -- everything is contained in the 'MAP' pbject
  MAP = expect_table()

  gui.sv_close()

  -- delete the file now
  gui.sv_delete(SAVEFILE)

  SEEN_TAB = {}

  collectgarbage("collect")

  debugf("DONE.\n\n")

  -- update everything in the user interface

  gui.game_state("active")

  screen_off()

  menu_clear()
  menu_push(GAME_MENU)

   world_restore()
  player_restore()
  battle_restore()
end

