------------------------------------------------------------------------
--  BATTLE ARENA
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------


BATTLE =
{
--[[
    state : keyword      -- "active" or NIL (not started)

    player_elem : TABLE  -- screen element for the player

    active_mons : LIST   -- currently active monsters (MON_STATE tables)

    cur_attack : TABLE   -- remembers info for current attack (kind, dir)

    log_lines : LIST     -- log area (list of screen elements)
    log_pos   : number   --
--]]
}


--MON_STATE CLASS
--[[
    mon : MONSTER

    dir : DIR      -- position from player

    slot : number  -- distance from player, 1 is close, 2 is far

    elem : TABLE   -- screen element
--]]


FIGHT_MENU =
{
  title = "Fight Menu"

  no_back = true

  items =
  {
    { label="&Attack",      act="attack" }
--  { label="&Throw",       act="throw" }
    { label="&Zap Wand",    act="zap" }
    { label="&Cast Spell",  act="cast" }
    { label="&Misc...",     act="misc" }
  }

  back_func = function()
    BATTLE.cur_attack = nil
  end

  key_func = function(act, param)
    -- this is here to swallow the "help" and "option" key actions
    -- (their screen would destroy our current battle screen)
  end

  menu_func = function(act, param)
    if act == "misc" then
      menu_push(FIGHT_MISC_MENU)
      return
    end

    BATTLE.cur_attack = { kind=act }

    FIGHT_DIR_MENU.title = string.upperFirst(act) .. " dir?"

    menu_push(FIGHT_DIR_MENU)
  end
}


FIGHT_DIR_MENU =
{
  title = ""  -- set before entering

  items =
  {
    { label="&North",  act="dir", param1="north" }
    { label="&East",   act="dir", param1="east"  }
    { label="&South",  act="dir", param1="south" }
    { label="&West",   act="dir", param1="west"  }

    { label="<----",   act="back" }
  }

  menu_func = function(act, param)
    assert(BATTLE.cur_attack)

    BATTLE.cur_attack.dir = assert(DIR_NAMES[param])

    battle_perform_attack()
  end
}


FIGHT_MISC_MENU =
{
  title = "Misc Stuff"

  items =
  {
    { label="&Help",          act="help" }
    { label="&Options",       act="options" }
    { label="&Discard Game",  act="!discard" }
    { label="&Save / Exit",   act="save" }

    { label="<----",  act="back" }
  }

  back_func = function()
    -- restore battle arena after viewing help / options
    battle_recreate_screen()
  end

  menu_func = function(act, param)
    -- Note that discard and save are handled elsewhere

    if act == "help" then
      menu_push(HELP_MENU)

    elseif act == "options" then
      menu_push(OPTION_MENU)
    end
  end

}


FIGHT_OVER_MENU =
{
  title = "Fight Over"

  items =
  {
    { label="<----",  act="back" }
  }
}


BATTLE_SCREEN =
{
  width  = 930, char_w = 50
  height = 500, char_h = 20

  elements =
  {
    -- red circle background
    { image="ui/battle_arena", x=280, y=260, w=600, align="M" }

    -- player spot
    { image="ui/monster_square", x=280, y=265, w=160, align="M" }

    -- monster spots : north and south
    { image="ui/monster_square", x=280, y=160, w=100, align="M" }
    { image="ui/monster_square", x=280, y= 70, w=100, align="M" }

    { image="ui/monster_square", x=280, y=365, w=100, align="M" }
    { image="ui/monster_square", x=280, y=455, w=100, align="M" }

    -- monster spots : east and west
    { image="ui/monster_square", x=170, y=265, w=100, align="M" }
    { image="ui/monster_square", x= 75, y=265, w=100, align="M" }

    { image="ui/monster_square", x=390, y=265, w=100, align="M" }
    { image="ui/monster_square", x=485, y=265, w=100, align="M" }

    -- log area
    { text="Battle Log", x=590, y=10, scale=1.5, color="fc9" }
  }
}


-- the LOG area is N rows of text which display things that happen
-- during a fight move, such as "the rat bites (-5)"

function battle_create_log()
  BATTLE.log_lines = {}
  BATTLE.log_pos   = 0

  for i = 1, 18 do
    local elem = { text="", x=592, y=44 + (i - 1) * 25 } 

    elem.id = gui.create_element()

    gui.set_element(elem.id, elem)

    table.insert(BATTLE.log_lines, elem)
  end
end


function battle_scroll_log()
  for i = 2, #BATTLE.log_lines do
    local elem = BATTLE.log_lines[i]
    local prev = BATTLE.log_lines[i - 1]

    prev.text  = elem.text
    prev.color = elem.color
  end

  BATTLE.log_lines[#BATTLE.log_lines].text = ""

  each elem in BATTLE.log_lines do
    gui.set_element(elem.id, elem)
  end
end


function battle_dim_log()
  each elem in BATTLE.log_lines do
    elem.color = "999"

    gui.set_element(elem.id, elem)
  end
end


function battle_print(msg, ...)
  -- don't add empty line at very beginning
  if BATTLE.log_pos == 0 and msg == "" then
    return
  end

  -- scroll up when full
  if BATTLE.log_pos >= #BATTLE.log_lines then
    battle_scroll_log()
  else
    BATTLE.log_pos = BATTLE.log_pos + 1
  end

  local elem = BATTLE.log_lines[BATTLE.log_pos]
  assert(elem)

  local new_text = string.format(msg, ...)

  elem.text  = new_text
  elem.color = "eee"

  gui.set_element(elem.id, elem)

  -- send to the Text Area too, even though it is not currently visible,
  -- it will allow player to review the battle (especially after dying).
  if msg == "" then
    messagef("\n")
  else
    messagef("%s", new_text)
  end
end



function battle_clear()
  BATTLE.state = nil
  BATTLE.log_lines = nil
  BATTLE.active_mons = nil

  MAP.battle = nil
end



function battle_begin(monsters, travel_dir)
  --
  -- Begin a fresh battle, using the monsters in the given list.
  -- 'travel_dir' is the direction the player wanted to go, can be NIL
  --

  local SCALE = 1.15


  local function display_player()
    local info = actor_lookup(PLAYER)

    local x = BATTLE_SCREEN.elements[2].x
    local y = BATTLE_SCREEN.elements[2].y

    local elem =
    {
      image = info.sprite.image
      x = x
      y = y + 35
      h = info.sprite.height * SCALE
      align = "B"
    }

    BATTLE.player_elem = elem

    elem.id = gui.create_element()

    gui.set_element(elem.id, elem)
  end


  local function get_element_for_slot(dir, slot)
    -- find screen element which corresponds to this dir / slot combo
    local elem_idx

    if dir == 8 then elem_idx = 3 end
    if dir == 2 then elem_idx = 5 end
    if dir == 4 then elem_idx = 7 end
    if dir == 6 then elem_idx = 9 end

    return elem_idx + (slot - 1)
  end


  local function display_monster(state)
    local elem_idx = get_element_for_slot(state.dir, state.slot)

    local x = BATTLE_SCREEN.elements[elem_idx].x
    local y = BATTLE_SCREEN.elements[elem_idx].y

    local info = actor_lookup(state.mon)

    local elem =
    {
      image = info.sprite.image
      x = x
      y = y + 22
      h = info.sprite.height * SCALE
      align = "B"
    }

    -- mirror the monster if it faces left or right and is in certain slots
    if info.faces ~= "front" then
      local n1 = (info.faces == "left")
      local n2 = (state.dir == 4 or state.dir == 8)
      elem.mirror = (n1 == n2)
    end

    elem.id = gui.create_element()

    state.elem = elem

    gui.set_element(elem.id, elem)
  end


  local function place_monsters_in_slots()
    local dir1 = travel_dir or 6
    local dir2 = geom.LEFT [dir1]
    local dir3 = geom.RIGHT[dir1]
    local dir4 = 10 - dir1

    if rand.odds(50) then
      dir2, dir3 = dir3, dir2
    end

    -- TODO : pair up monsters in the world, honor those pairings here

    local STATES =
    {
      { mon = monsters[1], dir = dir1, slot = 1 }
      { mon = monsters[2], dir = dir2, slot = 1 }
      { mon = monsters[3], dir = dir1, slot = 2 }
      { mon = monsters[4], dir = dir2, slot = 2 }

      { mon = monsters[5], dir = dir3, slot = 1 }
      { mon = monsters[6], dir = dir3, slot = 2 }
      { mon = monsters[7], dir = dir4, slot = 1 }
      { mon = monsters[8], dir = dir4, slot = 2 }
    }

    each state in STATES do
      if state.mon then
        table.insert(BATTLE.active_mons, state)

        display_monster(state)
      end
    end
  end


  ---| battle_begin |---

  assert(BATTLE.state != "active")
  assert(#monsters > 0)

  if #monsters > 8 then
    error("battle_begin: too many monsters!")
  end

  BATTLE =
  {
    state = "active"

    active_mons = {}

    log_lines = {}
    log_pos   = 0
  }

  MAP.battle = BATTLE

  screen_show(BATTLE_SCREEN)

  display_player()

  place_monsters_in_slots()

  battle_create_log()

  menu_push(FIGHT_MENU)
end



function battle_recreate_screen()

  local function restore_player()
    local elem = BATTLE.player_elem

    elem.id = gui.create_element()

    gui.set_element(elem.id, elem)
  end


  local function restore_monsters()
    each M in BATTLE.active_mons do
      M.elem.id = gui.create_element()

      gui.set_element(M.elem.id, M.elem)
    end
  end


  local function restore_log()
    each elem in BATTLE.log_lines do
      elem.id = gui.create_element()

      gui.set_element(elem.id, elem)
    end
  end


  ---| battle_recreate_screen |---

  screen_show(BATTLE_SCREEN)

  restore_player()
  restore_monsters()
  restore_log()
end



function battle_restore()
  -- this is called after loading a savegame

  if MAP.battle then
    BATTLE = MAP.battle

    battle_recreate_screen()

    menu_push(FIGHT_MENU)

  else
    -- no battle in progress
    BATTLE = {}
  end
end



function battle_hurt_player(M, damage)
  -- 'M' is an entry in active_mons

  damage = actor_adjust_damage(PLAYER, damage) * 11

  battle_print("The %s %s! (-%1.1f)", M.mon.race, "bites", damage)

  PLAYER.stats.health = PLAYER.stats.health - damage

  gui.set_stats(PLAYER.stats)

  if PLAYER.stats.health > 0 then
    return
  end

  PLAYER.stats.health = 0
  gui.set_stats(PLAYER.stats)

  -- Oh no!!

  messagef("\nYou die.");

  BATTLE.state = nil

  player_die("hurt", M.mon)
end



function battle_run_monsters()

  local function can_hit_player(M)
    if M.slot == 1 then
      return true
    end

    return false
  end


  each M in BATTLE.active_mons do
    if can_hit_player(M) then
      battle_hurt_player(M, 2)  -- FIXME damage
    end

    if PLAYER.is_dead then return end
  end
end



function battle_remove_monster(M)
  table.kill_elem(BATTLE.active_mons, M)

  -- move up any monster behind this one
  each AM in BATTLE.active_mons do
    if AM.dir == M.dir and AM.slot == 2 then
      M.slot, AM.slot = AM.slot, M.slot
      M.elem.x, AM.elem.x = AM.elem.x, M.elem.x

      gui.set_element(AM.elem.id, AM.elem)
    end
  end

  -- make it invisible
  M.elem.visible = false

  gui.set_element(M.elem.id, M.elem)
end



function battle_hurt_monster(M, damage)
  -- 'M' is an entry in active_mons

  local mon = M.mon

  damage = actor_adjust_damage(mon, damage)

  battle_print("You hit the %s (-%1.1f)", mon.race, damage)

  mon.stats.health = mon.stats.health - damage

  if mon.stats.health > 0 then
    return
  end

  -- killed it!!
  battle_print("The %s is killed!", mon.race)

  mon.is_dead = true

  -- remove from battle
  battle_remove_monster(M)
end



function battle_success()
  battle_print("")
  battle_print("You won the battle.")

  -- replace normal fight menu with a simple menu (containing just the back
  -- button, which returns to GAME_MENU).  This allos player to read last
  -- part of the battle log.
  while MENU ~= FIGHT_MENU do
    menu_pop()
  end

  -- cannot pop() the FIGHT_MENU, as the GAME_MENU turns off our screen
  menu_replace(FIGHT_OVER_MENU)

  -- clear the BATTLE state
  battle_clear()
end



function battle_perform_attack()

  local function can_hit_monster(M)
    if M.dir == BATTLE.cur_attack.dir and M.slot == 1 then
      return true
    end

    return false
  end


  battle_dim_log()


  -- find each affected monster, apply damage

  local count = 0

  each M in BATTLE.active_mons do
    if can_hit_monster(M) then
      count = count + 1

      if count == 1 then battle_print("") end

      battle_hurt_monster(M, 5)  -- FIXME : damage!!
    end
  end
 
  -- is the battle over now?

  if #BATTLE.active_mons == 0 then
    battle_success()
    return
  end

  if count == 0 then
    battle_print("")
    battle_print("You swipe at thin air.")
  end

  battle_print("")

  battle_run_monsters()
end



function battle_test()
  -- FIXME TEST STUFF
  local MON1 = { kind="monster", race="bear",  stats={health=50} }
  local MON2 = { kind="monster", race="mummy", stats={health=30} }
  local MON3 = { kind="monster", race="wolf",  stats={health=20} }

  battle_begin({ MON1, MON2, MON3 })
end

