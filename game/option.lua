------------------------------------------------------------------------
--  OPTION SCREENS
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------


OPTION_LAST_PAGE = "ui"


OPTION_MENU =
{
  title = "Option Menu"

  items =
  {
    { label="&UI",  act="opt_screen", param1="ui" }

    { label="<----",  act="back" }
  }

  enter_func = function()
    option_show_screen(OPTION_LAST_PAGE)
  end

  menu_func = function(act, param)
    if act == "opt_screen" then
      option_show_screen(param)
    end
  end

  control_func = function(name, value)
    if PREFS[name] == nil then
      error("Unknown pref in options: " .. tostring(name))
    end

    -- this takes care of type conversion ('value' parameter is always a string)
    amb_set_preference(name, value)
  end
}


OPTION_SCREENS =
{
  ui =
  {
    is_options = true

    width  = 800, char_w = 20
    height = 500, char_h = 12

    elements =
    {
      {
        text="UI Options",
        x=400, y=10, align="T"
        color="DDD", scale=3
      }

      {
        control="check_button",
        x=300, y=180, scale=1.0,
        opt_name="panel_right", val=1
      }
      {
        text="Panel on Right",
        x=340, y=180, align="L", color="AAA", scale=1
      }

      {
        control="check_button",
        x=300, y=240, scale=1.0,
        opt_name="text_top", val=0
      }
      {
        text="Text on Top",
        x=340, y=240, align="L", color="AAA", scale=1
      }

      {
        control="slider",
        x=200, y=350, w=400, scale=1.0,
        opt_name="slidey", val=10, maxval=40
      }

      {
        control="word_sel",
        x=200, y=450, w=400, scale=1.0,
        opt_name="blah", val=0
        words="blah_blah|voodoo dolls|black cats|mocha|viva l'australie n'est pas?"
      }
    }
  }
}


function pref_to_element(E)
  local value = PREFS[E.opt_name]

  if value == nil then
    error("Unknown pref in options: " .. tostring(E.opt_name))
  end

  if E.control == "check_button" then
    E.val = sel(value, 1, 0)
  else
    E.val = value
  end
end


function option_show_screen(page)
  local screen = OPTION_SCREENS[page]

  if not screen then
    error("No such option screen: " .. tostring(page))
  end

  OPTION_LAST_PAGE = page

  screen_show(screen)
end

