------------------------------------------------------------------------
--  HELP and OPTION SCREENS
------------------------------------------------------------------------
--
--  Amber Crux : a graphical adventure game
--
--  Copyright (C) 2014-2016  Andrew Apted
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version 3
--  of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this software.  If not, please visit the following
--  web page: http://www.gnu.org/licenses/gpl.html
--
------------------------------------------------------------------------


HELP_LAST_PAGE = "about"


HELP_MENU =
{
  title = "Help Topics"

  items =
  {
    { label="&About",    act="help_screen", param1="about" }
    { label="&General",  act="help_screen", param1="help1" }
    { label="&Keys",     act="help_screen", param1="keys" }

    { label="<----",    act="back" }
  }

  enter_func = function()
    help_show_screen(HELP_LAST_PAGE)
  end

  menu_func = function(act, param)
    if act == "help_screen" then
      help_show_screen(param)
    end
  end
}



HELP_SCREENS =
{
  about =
  {
    width  = 800, char_w = 20
    height = 500, char_h = 12

    elements =
    {
      {
        text="Amber Crux",
        x=310, y=82, align="M"
        color="FA5", scale=3
      }

      {
        text="v???"  -- real version set in amb_init()
        x=650, y=92, align="M"
        color="FA5", scale=1.8
      }

      {
        text="A graphical adventure game which is set\n" ..
             "in a randomly generated fantasy world."
        x = 400, y=190, align="M" 
        color="FFF"
      }

      { text="Copyright (C) 2014  Andrew Apted, et al"
        x = 400, y=280, align="M"
        color="CCC", scale = 0.92
      }

      { text="This program is free software, under the terms of the\n" ..
             "GNU General Public License (v3), and comes with\n" ..
             "ABSOLUTELY NO WARRANTY.  USE AT OWN RISK."
        x = 400, y=370, align="M"
        color="69C", scale = 0.8
      }

      { text="http://ambercrux.sourceforge.net/"
        x = 400, y=465, align="M"
        color="9d9", scale = 0.8
      }

    }
  }


  keys =
  {
    width  = 800, char_w = 20
    height = 500, char_h = 12

    elements =
    {
      {
        text="Special Keys\n(TODO)",
        x=400, y=10, align="T"
        color="DDD", scale=3
      }

      -- backspace for menus

      -- +/- to zoom map (also mousewheel)

      -- arrow keys to scroll map

      -- HOME zooms into player's location

      -- END zooms out to show whole map
    }
  }


  help1 =
  {
    width  = 800, char_w = 20
    height = 500, char_h = 12

    elements =
    {
      {
        text="Help Screen\n(TODO)",
        x=400, y=250, align="M"
        color="DDD", scale=3
      }
    }
  }


  help2 =
  {
    width  = 800, char_w = 20
    height = 500, char_h = 12

    elements =
    {
    }
  }
}


function help_set_version()
  HELP_SCREENS.about.elements[2].text = "v" .. gui.version()  
end


function help_show_screen(page)
  local screen = HELP_SCREENS[page]

  if not screen then
    error("No such help screen: " .. tostring(page))
  end

  HELP_LAST_PAGE = page

  screen_show(screen)
end

